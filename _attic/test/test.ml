open Crowbar
open Test_utils

module Gen = struct
  let nb_nodes_max = 1000

  let node = range nb_nodes_max

  let insertions : (Graph.Node.T.t * Graph.Node.T.t) list gen =
    list (map [node; node] (fun v w -> (v, w)))
end

module Detect_cycles_incremental =
  Incremental_cycles.Simple_sparse.Make (Graph.Node)

let test insertions =
  let g = Graph.make Gen.nb_nodes_max in
  let g' = Detect_cycles_incremental.create_graph () in

  let rec run = function
    | [] -> ()
    | (v, w) :: edges ->
      Printf.printf "Inserting edge (%d, %d)...\n%!" v w;
      begin match Detect_cycles_incremental.add_edge g' v w with
        | Ok ->
          Printf.printf "Incremental detection said Ok\n%!";
          Graph.add_edge g v w;
          check (Detect_cycles.detect_cycles g = `Ok);
          run edges
        | Cycle ->
          Printf.printf "Incremental detection said Cycle\n%!";
          Graph.add_edge g v w;
          check (Detect_cycles.detect_cycles g = `Cycle)
          (* stop there *)
      end
  in
  run insertions

let crowbar () =
  add_test ~name:"test" [Gen.insertions] test

let () =
  crowbar ()

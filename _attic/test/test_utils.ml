module Graph = struct
  module Node = struct
    module T = struct
      type t = int
      let equal = (=)
      let hash = Hashtbl.hash
    end

    module Tbl = struct
      include Hashtbl.Make (T)
      let create () = create 35
      let find h x = try Some (find h x) with Not_found -> None
    end
  end

  type t = Node.T.t list array

  let make (nb_nodes: int): t =
    Array.make nb_nodes []

  let add_edge (g: t) (n1: Node.T.t) (n2: Node.T.t) =
    g.(n1) <- n2 :: g.(n1)

  let nb_nodes (g: t): int = Array.length g

  let outgoing (g: t) (n: Node.T.t): Node.T.t list =
    g.(n)
end

(* Non-incremental cycles detection (a DFS) *)
module Detect_cycles = struct
  exception Cycle

  type mark = NoMark | Visiting | Visited

  let detect_cycles (g: Graph.t): [`Cycle | `Ok] =
    let mark = Array.make (Graph.nb_nodes g) NoMark in

    let rec visit (v: Graph.Node.T.t) =
      if mark.(v) = Visited then ()
      else if mark.(v) = Visiting then raise Cycle
      else begin
        mark.(v) <- Visiting;
        List.iter visit (Graph.outgoing g v);
        mark.(v) <- Visited
      end
    in

    try
      for i = 0 to Array.length mark - 1 do
        if mark.(i) = NoMark then visit i
      done;
      `Ok
    with Cycle -> `Cycle
end

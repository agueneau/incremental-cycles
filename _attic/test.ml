open Crowbar
open Test_utils

module Gen = struct
  let nb_nodes_max = 1000

  let node = range nb_nodes_max

  let insertions : (Graph.Vertex.t * Graph.Vertex.t) list gen =
    list (map [node; node] (fun v w -> (v, w)))
end

module Detect_cycles_incremental0 = Incremental_cycles.Cycles.Make0 (Graph)
module Detect_cycles_incremental1 = Incremental_cycles.Cycles.Make1 (Graph)
module Detect_cycles_incremental2 = Incremental_cycles.Cycles.Make2 (Graph)

let test incremental_add_edge insertions =
  let g = Graph.make Gen.nb_nodes_max in

  let rec run = function
    | [] -> ()
    | (v, w) :: edges ->
      begin match incremental_add_edge g v w with
        | `Ok ->
          Printf.printf "Incremental detection said Ok\n%!";
          check (Detect_cycles.detect_cycles g = `Ok);
          run edges
        | `Cycle ->
          Printf.printf "Incremental detection said Cycle\n%!";
          Graph.add_edge g v w;
          check (Detect_cycles.detect_cycles g = `Cycle)
          (* stop there *)
      end
  in
  run insertions

let crowbar () =
  let add_graph_test incremental_add_edge =
    add_test ~name:"algo0" [Gen.insertions] (test incremental_add_edge)
  in

  (* add_graph_test Detect_cycles_incremental0.add_edge; *)
  add_graph_test Detect_cycles_incremental1.add_edge;
  (* add_graph_test Detect_cycles_incremental2.add_edge; *)
  ()


let () =
  crowbar ()

module type Graph = sig
  type vertex

  type stop_reason

  (* Iterates on the successors of a vertex. *)
  val iter_successors :
    (vertex -> [`Complete | `Stop of 'reason]) ->
    vertex ->
    [`Complete | `Stop of 'reason]

  (* "Visits" a vertex:
     - if [visit] has already been called on the same vertex,
       return [`Already_visited]
     - if it is the first time, return [`Visited]
     - to interrupt the traversal with reason [reason], return [`Stop reason]
  *)
  val visit : vertex -> [`Already_visited | `Visited | `Stop of stop_reason]
end

module Make (G : Graph) : sig
  val visit_successors : G.vertex -> [`Complete | `Stop of G.stop_reason]
end
=
struct
  let rec visit_successors (v : G.vertex) =
    match G.visit v with
    | `Already_visited -> `Complete
    | `Stop reason -> `Stop reason
    | `Visited -> G.iter_successors visit_successors v
end

(* ************************************************************************** *)

type ('vertex, 'reason) dfs_config = {
  iter_successors:
    ('vertex -> [`Complete | `Stop of 'reason]) ->
    'vertex ->
    [`Complete | `Stop of 'reason];

  visit:
    'vertex ->
    [`Already_visited | `Visited | `Stop of 'reason];
}

let rec visit_successors
    (c: ('vertex, 'reason) dfs_config)
    (v: 'vertex):
  [`Complete | `Stop of 'reason]
  =
  match c.visit v with
  | `Already_visited -> `Complete
  | `Stop reason -> `Stop reason
  | `Visited -> c.iter_successors (visit_successors c) v

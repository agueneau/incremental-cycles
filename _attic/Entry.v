Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Load the CF definitions. *)
Require Import Simple_sparse_ml.
Require Import TLC.LibMap.
Require Import TLCBuffer.

(******************************************************************************)
(* The [entry] type *)

Definition entry := Simple_sparse_ml.Entry_ml.t_.

(* TODO: generalize to an equivalence relation on entries *)
Hypothesis Entry_equal_spec : forall (x y: entry),
  app Entry_ml.equal [x y]
    PRE \[]
    POST \[= isTrue (x = y)].

Hint Extern 1 (RegisterSpec Entry_ml.equal) => Provide Entry_equal_spec.

Global Opaque Entry_ml.t_.

(******************************************************************************)
(* Axiomatized specification for ETbl.

   Possession of a table _does entail_ possession of its elements.
*)

Section ETblSpecs.

(* A table that _does not_ possess its elements. *)
Axiom ETblRaw : forall {A: Type},
  map entry A -> ETbl_ml.t_ A -> hprop.

(* A table that _does_ possess its elements. *)
Definition ETbl {A a: Type}:
  (A -> a -> hprop) -> map entry A -> ETbl_ml.t_ a -> hprop :=
  fun R M t =>
    Hexists (M1: map entry a) (M2: map a A),
    @ETblRaw a M1 t \* Group R M2 \*
    \[ M = map_compose M2 M1
       /\ codom M1 = dom M2
    ].

Lemma Group_Id : forall A M,
  @Group A A Id M = \[ map_is_id M ].
Admitted.

Lemma hprop_extens : forall (H1 H2: hprop),
  (H1 ==> H2) /\ (H2 ==> H1) <-> H1 = H2.
Proof.
  intros. unfold pred_incl.
  repeat intuition try extens; subst; auto.
Qed.

Hint Resolve map_is_id_on.
Ltac auto_tilde ::= rew_map; jauto.

Lemma ETbl_pure : forall A M t, @ETbl A A Id M t = @ETblRaw A M t.
Proof.
  intros. unfold ETbl. apply hprop_extens. split.
  { hpull; intros M1 M2 (-> & Hdom). rewrite Group_Id. hpull; intros HM2.
    forwards~ ->: map_compose_id_l M2 M1.
    { rewrite Hdom. apply incl_refl. (* TODO: reflexivity instance *) } }
  { hsimpl M (id_on (codom M)).
    { rewrite Group_Id. hsimpl~. }
    { rewrite dom_id_on. split~.
      forwards~ ->: map_compose_id_l. apply incl_refl. } }
Qed.

Axiom ETbl_create_spec : forall A a R,
  app ETbl_ml.create [tt]
    PRE \[]
    POST (fun tbl => @ETbl A a R empty tbl).

Axiom ETbl_set_spec : forall A a R t (k: entry) (v: a) (V: A) M,
  app ETbl_ml.set [t k v]
    PRE (ETbl R M t \* v ~> R V)
    POST (fun (_:unit) => ETbl R M[k:=V] t).

Axiom ETblRaw_get_spec : forall A t (k:entry) M `{Inhab A},
  k \indom M ->
  app ETbl_ml.get [t k]
    INV (@ETblRaw A M t)
    POST \[= M[k]].

(* Spec for [ETbl.get] using a magic wand. *)
Lemma ETbl_get_spec : forall A a R t (k:entry) M `{Inhab A} `{Inhab a},
  k \indom M ->
  app ETbl_ml.get [t k]
    PRE (@ETbl A a R M t)
    POST (fun (v:a) => Hexists (REST: hprop),
      v ~> R M[k] \* REST \*
        \[ REST \* v ~> R M[k] ==> ETbl R M t ]).
Proof.
  introv ? Hk.
  unfold ETbl. xpull; intros M1 M2 (-> & Hdom).
  forwards S: (@ETblRaw_get_spec a t k M1). { admit. }
  xapply S.
  { hsimpl. }
  { hpull. intros ->. hsimpl.
    { erewrite Group_rem with (x := M1[k]).
      (* FIXME *)
      { (* assert (M2[M1[k]] = (((map_compose M2 M1) : map entry A)[k] : A)) as -> by admit. *)
        assert (M1[k] ~> R M2[M1[k]] = \[]) as -> by admit.
        assert (M1[k] ~> R (@read entry A (map entry A) (@read_inst entry A H)
                                  (@map_compose entry a A M2 M1) k) = \[]) as -> by admit.
        assert (\GC = \[]) as -> by admit.  (* XX *)
        hsimpl. }
      { admit. (* ok *) } }
    { hsimpl M1 M2. admit. auto. } }
Qed.

Lemma ETbl_get_spec_pure : forall A t k M `{Inhab A},
  k \indom M ->
  app ETbl_ml.get [t k]
    INV (@ETbl A A Id M t)
    POST \[= M[k]].
Proof. introv Hk. rewrite ETbl_pure. xapply~ ETblRaw_get_spec. Qed.

(* More precise spec for [ETbl.get]. If relevant, should be a lemma *)
(* In practice here we do not need the extra precision. *)
(*
Axiom ETbl_get_spec_precise : forall A a R t (k:entry) M `{Inhab A} `{Inhab a},
  k \indom M ->
  app ETbl_ml.get [t k]
    PRE (@ETbl A a R M t)
    POST (fun (V:A) => Hexists (v: a) (M1: map entry a) (M2: map a A),
      @ETbl a a Id M1 t \*
      v ~> R V \*
      Group R (M2 \-- v) \*
      \[ M = map_compose M2 M1
         /\ codom M1 = dom M2
         /\ binds M2 v V
         /\ binds M1 k v
      ]).
*)

Axiom ETbl_mem_spec : forall A a R t k M,
  app ETbl_ml.mem [t k]
    PRE (@ETbl A a R M t)
    POST (fun (b:bool) => @ETbl A a R M t \* \[ b = isTrue (k \indom M) ]).

Axiom ETbl_keys_spec : forall A a R t M,
  app ETbl_ml.keys [t]
    INV (@ETbl A a R M t)
    POST (fun (xs: list entry) => \[ noduplicates xs /\ to_set xs = dom M ]).

End ETblSpecs.

Global Opaque ETbl.
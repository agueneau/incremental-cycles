module type Graph = sig
  type t

  module Vertex : sig
    type t
    val equal : t -> t -> bool
  end

  module VSet : sig
    type t
    val fold : (Vertex.t -> 'a -> 'a) -> t -> 'a -> 'a
    (* val cascade : t -> Vertex.t Cascade.cascade *)
    val to_list : t -> Vertex.t list
  end

  module VSetMut : sig
    type t
    val fold : (Vertex.t -> 'a -> 'a) -> t -> 'a -> 'a
    (* val cascade : t -> Vertex.t Cascade.cascade *)
    val to_list : t -> Vertex.t list
    val set_empty : t -> unit
    val add : t -> Vertex.t -> unit
  end


  (***** Description of the graph *****)

  val nb_vertices : t -> int
  val nb_edges : t -> int

  val outgoing : t -> Vertex.t -> VSet.t


  (***** Operations on the graph *****)

  val add_edge : t -> Vertex.t -> Vertex.t -> unit


  (***** Information maintained by the algorithm *****)

  val get_level : t -> Vertex.t -> int
  val set_level : t -> Vertex.t -> int -> unit

  val incoming : t -> Vertex.t -> VSetMut.t

  type mark
  val fresh_mark : unit -> mark
  val mark_equal : mark -> mark -> bool

  val get_mark : t -> Vertex.t -> mark
  val set_mark : t -> Vertex.t -> mark -> unit
end

module Make0 (G : Graph) = struct
  open G

  let get_delta (g: G.t): int =
    let n = G.nb_vertices g in
    let m = G.nb_edges g in
    int_of_float
      (min
         (float_of_int m ** 0.5)
         (float_of_int n ** (2. /. 3.)))

  exception Cycle
  exception Timeout
  exception Done

  let backward_traversal
      (g: G.t) (v: Vertex.t) (w: Vertex.t):
    (Vertex.t -> bool)
    =
    let mark = fresh_mark () in

    let rec traverse (v: Vertex.t) (count: int): int =
      if count < 0 then raise Timeout
      else if Vertex.equal v w then raise Cycle
      else if mark_equal (get_mark g v) mark then count
      else begin
        set_mark g v mark;
        VSetMut.fold traverse (incoming g v) (count - 1)
      end
    in
    match traverse v (get_delta g) with
    | exception Timeout ->
      set_level g w (get_level g v + 1);
      VSetMut.set_empty (incoming g w);
      (Vertex.equal v)
    | _ when get_level g w = get_level g v ->
      raise Done
    | _ when get_level g w < get_level g v ->
      set_level g w (get_level g v);
      VSetMut.set_empty (incoming g w);
      (fun v -> mark_equal (get_mark g v) mark)
    | _ ->
      assert false

  let outgoing_edges
      (g: G.t) (v: Vertex.t):
    (Vertex.t * Vertex.t) list
    =
    List.map
      (fun v' -> (v, v'))
      (VSet.to_list (outgoing g v))

  let forward_traversal
      (g: G.t) (w: Vertex.t) (visited_backward: Vertex.t -> bool):
    unit
    =
    let rec traverse (stack: (Vertex.t * Vertex.t) list) =
      match stack with
      | [] -> ()
      | (x, y) :: stack' ->
        if visited_backward y then
          raise Cycle
        else if get_level g x = get_level g y then begin
          VSetMut.add (incoming g y) x;
          traverse stack'
        end else if get_level g x > get_level g y then begin
          set_level g y (get_level g x);
          VSetMut.set_empty (incoming g y);
          VSetMut.add (incoming g y) x;
          traverse ((outgoing_edges g y) @ stack')
        end else begin
          (* Can this happen? *)
          (* For the moment, simply drop the edge and continue *)
          traverse stack'
        end
    in
    traverse (outgoing_edges g w)

  let insert_arc (g: G.t) (v: Vertex.t) (w: Vertex.t) =
    add_edge g v w;
    if get_level g v = get_level g w then
      VSetMut.add (incoming g w) v

  let add_edge (g: G.t) (v: G.Vertex.t) (w: G.Vertex.t):
    [`Cycle | `Ok]
    =
    match
      try if get_level g v < get_level g w then ()
        else forward_traversal g w (backward_traversal g v w)
      with Done -> ()
    with
    | exception Cycle -> `Cycle
    | () -> insert_arc g v w; `Ok
end

module Make1 (G : Graph) = struct
  open G

  let get_delta (g: G.t): int =
    let n = G.nb_vertices g in
    let m = G.nb_edges g in
    int_of_float
      (min
         (float_of_int m ** 0.5)
         (float_of_int n ** (2. /. 3.)))

  let backward_traversal
      (g: G.t) (v: Vertex.t) (w: Vertex.t):
    [`Forward of (Vertex.t -> bool) | `Cycle | `Done]
    =
    let mark = fresh_mark () in

    let rec traverse (count: int) (stack: Vertex.t list) =
      if count < 0 then `Timeout
      else begin
        match stack with
        | [] -> `Complete
        | v :: stack' ->
          if Vertex.equal v w then `Cycle
          else if mark_equal (get_mark g v) mark then
            traverse count stack'
          else begin
            set_mark g v mark;
            traverse
              (count - 1)
              ((VSetMut.to_list (incoming g v)) @ stack')
          end
      end
    in
    match traverse (get_delta g) [v] with
    | `Cycle ->
      `Cycle
    | `Timeout ->
      set_level g w (get_level g v + 1);
      VSetMut.set_empty (incoming g w);
      `Forward (Vertex.equal v)
    | `Complete when get_level g w = get_level g v ->
      `Done
    | `Complete when get_level g w < get_level g v ->
      set_level g w (get_level g v);
      VSetMut.set_empty (incoming g w);
      `Forward (fun v -> mark_equal (get_mark g v) mark)
    | `Complete ->
      assert false

  let outgoing_edges
      (g: G.t) (v: Vertex.t):
    (Vertex.t * Vertex.t) list
    =
    List.map
      (fun v' -> (v, v'))
      (VSet.to_list (outgoing g v))

  let forward_traversal
      (g: G.t) (w: Vertex.t) (visited_backward: Vertex.t -> bool):
    [`Cycle | `Done]
    =
    let rec traverse (stack: (Vertex.t * Vertex.t) list) =
      match stack with
      | [] -> `Done
      | (x, y) :: stack' ->
        if visited_backward y then
          `Cycle
        else if get_level g x = get_level g y then begin
          VSetMut.add (incoming g y) x;
          traverse stack'
        end else if get_level g x > get_level g y then begin
          set_level g y (get_level g x);
          VSetMut.set_empty (incoming g y);
          VSetMut.add (incoming g y) x;
          traverse ((outgoing_edges g y) @ stack')
        end else begin
          (* Can this happen? *)
          (* For the moment, simply drop the edge and continue *)
          traverse stack'
        end
    in
    traverse (outgoing_edges g w)

  let insert_arc
      (g: G.t) (v: Vertex.t) (w: Vertex.t)
    =
    add_edge g v w;
    if get_level g v = get_level g w then
      VSetMut.add (incoming g w) v

  let add_edge (g: G.t) (v: G.Vertex.t) (w: G.Vertex.t) =
    if get_level g v < get_level g w then begin
      insert_arc g v w;
      `Ok
    end else begin
      match backward_traversal g v w with
      | `Cycle -> `Cycle
      | `Done ->
        insert_arc g v w;
        `Ok
      | `Forward visited_backward ->
        match forward_traversal g w visited_backward with
        | `Cycle -> `Cycle
        | `Done ->
          insert_arc g v w;
          `Ok
    end
end

module Make2 (G : Graph) = struct
  open G

  let get_delta (g: G.t): int =
    let n = G.nb_vertices g in
    let m = G.nb_edges g in
    int_of_float
      (min
         (float_of_int m ** 0.5)
         (float_of_int n ** (2. /. 3.)))

  let backward_traversal (g: G.t) (v: Vertex.t) (w: Vertex.t)
      (k       : (Vertex.t -> bool) -> 'ret)
      (k_cycle : unit -> 'ret)
      (k_done  : unit -> 'ret)
    : 'ret
    =
    let mark = fresh_mark () in

    let rec traverse (count: int) (stack: Vertex.t list)
        (k         : unit -> 'ret)
        (k_cycle   : unit -> 'ret)
        (k_timeout : unit -> 'ret)
      : 'ret
      =
      if count < 0 then k_timeout ()
      else begin
        match stack with
        | [] -> k ()
        | v :: stack' ->
          if Vertex.equal v w then k_cycle ()
          else if mark_equal (get_mark g v) mark then
            traverse count stack' k k_cycle k_timeout
          else begin
            set_mark g v mark;
            traverse (count - 1) (VSetMut.to_list (incoming g v) @ stack')
              k k_cycle k_timeout
          end
      end
    in
    traverse (get_delta g) [v]
      (fun () ->
         let l_v = get_level g v in
         let l_w = get_level g w in
         if l_w = l_v then
           k_done ()
         else if l_w < l_v then (
           set_level g w l_v;
           VSetMut.set_empty (incoming g w);
           k (fun v -> mark_equal (get_mark g v) mark)
         ) else
           assert false)
      k_cycle
      (fun () ->
         set_level g w (get_level g v + 1);
         VSetMut.set_empty (incoming g w);
         k (Vertex.equal v))

  let outgoing_edges
      (g: G.t) (v: Vertex.t):
    (Vertex.t * Vertex.t) list
    =
    List.map
      (fun v' -> (v, v'))
      (VSet.to_list (outgoing g v))

  let forward_traversal
      (g: G.t) (w: Vertex.t) (visited_backward: Vertex.t -> bool)
      (k       : unit -> 'ret)
      (k_cycle : unit -> 'ret)
    : 'ret
    =
    let rec traverse (stack: (Vertex.t * Vertex.t) list)
        (k       : unit -> 'ret)
        (k_cycle : unit -> 'ret)
      =
      match stack with
      | [] -> k ()
      | (x, y) :: stack' ->
        let l_x = get_level g x in
        let l_y = get_level g y in
        let incoming_y = incoming g y in
        if visited_backward y then
          k_cycle ()
        else if l_x = l_y then begin
          VSetMut.add incoming_y x;
          traverse stack' k k_cycle
        end else if l_x > l_y then begin
          set_level g y l_x;
          VSetMut.set_empty incoming_y;
          VSetMut.add incoming_y x;
          traverse ((outgoing_edges g y) @ stack') k k_cycle
        end else begin
          (* Can this happen? *)
          (* For the moment, simply drop the edge and continue *)
          traverse stack' k k_cycle
        end
    in
    traverse (outgoing_edges g w) k k_cycle

  let insert_arc
      (g: G.t) (v: Vertex.t) (w: Vertex.t)
    =
    add_edge g v w;
    if get_level g v = get_level g w then
      VSetMut.add (incoming g w) v

  let add_edge (g: G.t) (v: G.Vertex.t) (w: G.Vertex.t):
    [`Cycle | `Ok ]
    =
    let k () = insert_arc g v w; `Ok in
    let k_cycle () = `Cycle in
    if get_level g v < get_level g w then k ()
    else
      backward_traversal g v w
        (fun visited -> forward_traversal g w visited k k_cycle)
        k_cycle
        k
end

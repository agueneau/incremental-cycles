(* Representation predicate for sets of entries, defined as a map from entries
   to unit *)

Definition ESet : set entry -> loc -> hprop :=
  fun S l => ETbl Id (const_on S tt) l.

(* Representation predicate for sets of entries, defined as lists (without
   duplicates). *)

Definition ESetL : set entry -> list entry -> hprop :=
  fun S l => \[ S = to_set l /\ noduplicates l ].

(******************************************************************************)

(* Representation predicate for a map associating data to nodes. Its model is a
   total function, which is required to be equal to [default] outside of the
   domain (the nodes of the graph).
*)

Definition NodeData {A a} (R: A -> a -> hprop) (G: graph node) (default: A):
  (entry -> A) -> ETbl_ml.t_ a -> hprop :=
  fun f tbl =>
    tbl ~> ETbl R (map_of f (nodes G)) \*
    \[ forall x, x \notin (nodes G) -> f x = default ].

(******************************************************************************)

Definition GraphImpl G L M I E (r: loc): hprop :=
  Hexists Lr Mr Or Ir fresh_mark,
  r ~> `{
      level' := Lr;
      mark' := Mr;
      outgoing' := Or;
      incoming' := Ir;
      fresh_mark' := fresh_mark
  } \*
  Lr ~> NodeData Id G 1 L \*
  Mr ~> NodeData Id G (-1) M \*
  Ir ~> NodeData ESetL G \{} I \*
  Or ~> NodeData ESet G \{} (out_edges G) \*
  \[
     forall n, n \in nodes G -> M n < fresh_mark /\
     0 <= fresh_mark
  ].

(***********)
Global Opaque entry node.

(* [Entry.t] is the type that is associated with nodes in the graph.
   [ETbl.t] is a map with [Entry.t] as keys.
*)

module Entry = struct
  type t = int
  let equal = (=)
end

(* Entry-indexed map *)
(* The actual implementation of this would be something like Hashtbl. *)
module ETbl = struct
  type 'a t = (int * 'a) list ref

  let create () : 'a t = ref []

  let set (r: 'a t) (k: Entry.t) (v: 'a) =
    (* this leaks memory *)
    r := (k, v) :: !r

  let get (r: 'a t) (k: Entry.t): 'a =
    let rec loop = function
      | (k', v') :: xs ->
        if Entry.equal k k' then v'
        else loop xs
      | [] -> assert false
    in
    loop !r

  let mem (r: 'a t) (k: Entry.t): bool =
    let rec loop = function
      | (k', _) :: xs ->
        if Entry.equal k k' then true
        else loop xs
      | [] -> false
    in
    loop !r

  (* let fold (f: Entry.t -> 'a -> 'acc -> 'acc) (r: 'a t) (acc: 'acc): 'acc = *)
  (*   List.fold_left (fun acc (k, v) -> f k v acc) acc !r *)

  let sort_uniq _ = failwith "dummy"

  let keys (r: 'a t): Entry.t list =
    sort_uniq (List.map fst !r)
end

(******************************************************************************)
(* The graph structure.

   This could also be abstracted from the code of the algorithm itself. *)

(* The type for marking nodes while traversing the graph.

   When doing a graph traversal, one usually only needs two or three different
   values for [mark]. However, since this is an incremental algorithm,
   multiple traversals are going to happen, and we want to make sure the marks
   of a traversal do not interfere with later traversals.

   One solution would be to clean-up after a traversal by un-marking the
   nodes.

   An other approach, that we adopt here, is to have a source of "fresh marks"
   (here, an integer that gets incremented). Each traversal allocates a fresh
   mark. If a node has a different mark than the current one, then it has not
   been marked by the current traversal.

   One can wonder if the global counter of marks could overflow. In general,
   this is possible (and one could solve this problem by using GC-allocated
   pointers as marks instead of integers). However, this is not possible for
   the code below: a single fresh mark is allocated for each edge insertion --
   therefore, the graph structure will consume all memory long before the mark
   counter overflows. *)
type mark = int

let mark_eq : int -> int -> bool = (=)

(* A node of the graph is an [Entry.t], with the additional invariant that it
   must be in the domain of the tables of the [graph] type below (level, mark,
   ...). *)
type node = Entry.t

let node_eq = Entry.equal

(* A hashset of nodess, encoded using a hashtable. *)
type node_set = unit ETbl.t

type graph = {
  level : int ETbl.t;
  mark : mark ETbl.t;
  outgoing : node_set ETbl.t;
  (* Instead of a [node list ETbl.t], this could be a [node_set ETbl.t] as
     well. However in practice we do not need constant time lookup, only to add
     an incoming edge, and iterate on the set of incoming edges.

     So for the time being we use a list. *)
  incoming : node list ETbl.t;

  (* Global counter used to allocate fresh marks. Its current value is the
     next fresh mark. *)
  mutable fresh_mark : mark;
}

let create_graph (): graph = {
  level = ETbl.create ();
  mark = ETbl.create ();
  outgoing = ETbl.create ();
  incoming = ETbl.create ();
  fresh_mark = 0;
}

let get_fresh_mark (g: graph): mark =
  let fresh_mark = g.fresh_mark in
  g.fresh_mark <- g.fresh_mark + 1;
  fresh_mark

(* Registers an entry as a node of the graph. *)
let add_node (g: graph) (e: Entry.t): node =
  ETbl.set g.level e 1;
  (* We have the invariant that marks of nodes in the graph are less than
     [g.fresh_mark]. Since [fresh_mark] is initially 0 and only increases,
     it is safe to use [-1] as a mark for new nodes. *)
  ETbl.set g.mark e (-1);
  ETbl.set g.outgoing e (ETbl.create ());
  ETbl.set g.incoming e [];
  e

(* Returns the node corresponding to an entry.

   With this particular implementation, a node is simply an entry that is "in
   the domain" of the graph. So we only need (if needed) to register the entry
   in the graph, i.e. make sure it is in the domain of the various maps. *)
let node_of_entry (g: graph) (e: Entry.t): node =
  if ETbl.mem g.level e then e
  else add_node g e

(* Some boilerplate, to implement a common Graph signature *)
let get_mark (g: graph) (n: node): mark = ETbl.get g.mark n
let set_mark (g: graph) (n: node) (m: mark) = ETbl.set g.mark n m
let get_level (g: graph) (n: node): int = ETbl.get g.level n
let set_level (g: graph) (n: node) (l: int) = ETbl.set g.level n l
let incoming (g: graph) (n: node): node list = ETbl.get g.incoming n
let clear_incoming (g: graph) (n: node) = ETbl.set g.incoming n []
let add_incoming (g: graph) (n: node) (x: node) =
  ETbl.set g.incoming n (x :: (incoming g n))
let set_incoming_singleton (g: graph) (n: node) (x: node) =
  ETbl.set g.incoming n [x]

(* Helper that returns the list of outgoing edges [(n1, n2)] for an entry [n1]. *)
(* let outgoing_edges (g: graph) (n1: node): (node * node) list = *)
(*   let outgoing = ETbl.get g.outgoing n1 in *)
(*   ETbl.fold (fun (n2: Entry.t) () acc -> *)
(*     (n1, n2) :: acc *)
(*   ) outgoing [] *)

(* let fold_outgoing (g: graph) (x: node) (acc: 'a) (f: node -> 'a -> 'a): 'a =
 *   ETbl.fold (fun (y: node) () acc -> f y acc) (ETbl.get g.outgoing x) acc *)

(* "Unconventional" order of arguments, to match the generic Fold
   specification defined in CFIteration *)
(* let fold_outgoing (f: node -> 'a -> 'a) ((g, x): graph * node) (acc: 'a) : 'a = *)
(*   ETbl.fold (fun (y: node) () acc -> f y acc) (ETbl.get g.outgoing x) acc *)

let outgoing (g: graph) (x: node): node list =
  ETbl.keys (ETbl.get g.outgoing x)

(* This is "unsafe" since it does not check for cycles: this is the whole point
   of the whole algorithm, which implements [add_edge]. *)
let unsafe_add_edge (g: graph) (n1: node) (n2: node) =
  let outgoing = ETbl.get g.outgoing n1 in
  ETbl.set outgoing n2 ();
  if get_level g n1 = get_level g n2 then
    add_incoming g n2 n1

(* Check if an edge is present in the graph. *)
let check_edge (g: graph) (n1: Entry.t) (n2: Entry.t) =
  let outgoing = ETbl.get g.outgoing n1 in
  ETbl.mem outgoing n2

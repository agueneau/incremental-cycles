Set Implicit Arguments.
From TLC Require Import LibTactics LibEqual LibLogic LibFun.
From TLC Require LibSet LibMap LibList LibListZ LibListSub.
From TLC Require LibIntTactics.

Section Map.

Import TLC.LibSet TLC.LibMap.

Open Scope set_scope.

(* Obtain a map from a total function and a domain. *)
Definition map_of {A B} (f: A -> B) (D: set A): map A B :=
  fun a => If a \in D then Some (f a) else None.

Definition codom {A B} (M: map A B) :=
  set_st (fun y => exists x, binds M x y).

Definition map_compose {A B C} (g: map B C) (f: map A B) :=
  fun x =>
    match f x with
    | Some y => g y
    | None => None
    end.

Definition map_is_id {A} (M: map A A) :=
  forall x, x \indom M -> binds M x x.

Lemma map_compose_id_l : forall A B (M2: map B B) (M1: map A B),
  map_is_id M2 ->
  codom M1 \c dom M2 ->
  map_compose M2 M1 = M1.
Proof.
  introv Id C.
  extens. intro a. unfold map_compose.
  case_eq (M1 a).
  { intros b Hb. apply Id.
    assert (Hb': b \in codom M1).
    { (* TODO: add hint *) unfold codom. rew_set. exists~ a. }
    eapply incl_inv; eassumption. }
  { auto. }
Qed.

Definition id_on {A} (D: set A): map A A :=
  ((fun x => Some x) : map A A) \| D.

(*
Lemma dom_id_on : forall A (D: set A),
  dom (id_on D) = D.
Admitted.

Lemma map_is_id_on : forall A (D: set A),
  map_is_id (id_on D).
Proof.
  intros. unfold map_is_id, id_on.
  intros x Hx. erewrite binds_eq_indom_read. split~.
  admit.
Admitted.

Definition const_on {A B} (D: set A) (c: B): map A B :=
  ((fun _ => Some c) : map A B) \| D.

Lemma dom_const_on : forall A B (D: set A) (c: B),
  dom (const_on D c) = D.
Admitted.

Lemma read_const_on : forall A B `{Inhab B} (D: set A) (c: B) (x: A),
  x \in D ->
  (const_on D c)[x] = c.
Admitted.
*)

Definition injective {A B} (M: map A B) :=
  forall a1 a2 b,
    binds M a1 b ->
    binds M a2 b ->
    a1 = a2.

End Map.

(*
Hint Rewrite dom_id_on : rew_map.
Hint Rewrite dom_const_on : rew_map.
*)

(******************************************************************************)

Hint Rewrite <-Logic.neg_false : rew_logic.

Lemma not_prop_eq_False : forall (P:Prop), P -> (~ P) = False.
Proof. intros. extens; tauto. Qed.

Ltac rew_logic_preprocess :=
  (* using non-linear pattern matching here raises an anomaly *)
  repeat
    match goal with
    | H : ?P |- _ =>
      match goal with
        |- context [ P ] => rewrite (prop_eq_True H)
      | |- context [ ~ P ] => rewrite (not_prop_eq_False H)
      end
    | H : ~ ?P |- _ =>
      match goal with
        |- context [ P ] => rewrite (prop_eq_False H)
      end
    end.

Ltac rew_logic' :=
  rew_logic_preprocess; rew_logic.

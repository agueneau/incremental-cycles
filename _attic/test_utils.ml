module Graph = struct
  module Vertex = struct
    type t = int
    let equal = (=)
  end

  module VSet = struct
    type t = int list ref

    let fold f vset acc = List.fold_right f !vset acc
    let to_list = (!)
    let set_empty vset = vset := []
    let add vset x = vset := x :: !vset
  end

  module VSetMut = VSet

  type mark = unit ref
  let fresh_mark () = ref ()
  let mark_equal = (==)

  type vertex_data = {
    incoming : int list ref;
    outgoing : int list ref;
    mutable level : int;
    mutable status : mark;
  }

  let mk_vertex_data () = {
    incoming = ref [];
    outgoing = ref [];
    level = 1;
    status = fresh_mark ();
  }

  type t = {
    vertices : vertex_data array;
    mutable nb_edges : int;
  }

  let make nb_vertices = {
    vertices = Array.init nb_vertices (fun _ -> mk_vertex_data ());
    nb_edges = 0;
  }

  let nb_vertices g = Array.length g.vertices
  let nb_edges g = g.nb_edges
  let outgoing g v = g.vertices.(v).outgoing

  let add_edge (g: t) v w =
    VSet.add  g.vertices.(v).outgoing w;
    g.nb_edges <- g.nb_edges + 1

  let get_level g v = g.vertices.(v).level
  let set_level g v l = g.vertices.(v).level <- l

  let incoming g v = g.vertices.(v).incoming

  let get_mark g v = g.vertices.(v).status
  let set_mark g v s = g.vertices.(v).status <- s
end

(* Non-incremental cycles detection (a DFS) *)
module Detect_cycles = struct
  exception Cycle

  type mark = NoMark | Visiting | Visited

  let detect_cycles (g: Graph.t):
    [`Cycle | `Ok]
    =
    let mark = Array.make (Graph.nb_vertices g) NoMark in

    let rec visit (v: Graph.Vertex.t) =
      if mark.(v) = Visited then ()
      else if mark.(v) = Visiting then raise Cycle
      else begin
        mark.(v) <- Visiting;
        List.iter visit (Graph.outgoing g v |> Graph.VSet.to_list);
        mark.(v) <- Visited
      end
    in

    try
      for i = 0 to Array.length mark - 1 do
        if mark.(i) = NoMark then visit i
      done;
      `Ok
    with Cycle -> `Cycle
end

let do_insertions g add_edge insertions =
  List.iter (fun (v, w) ->
    add_edge g v w |>
    (function `Ok -> "Ok" | `Cycle -> "Cycle")
    |> print_endline
  ) insertions

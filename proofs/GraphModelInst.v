From TLC Require Import LibSet LibList LibRelation LibTactics LibLogic.
Require Import TLCBuffer LibTacticsExtra LibRelationExtra.

Require Import GraphModel.

Module MyGraph.

Record graph (N : Type) := MkGraph {
  vertices : set N;
  edges : set (N * N);

  edges_in_vertices x y :
    (x, y) \in edges -> x \in vertices /\ y \in vertices;

  finite_graph :
    finite vertices;

  (* This could be deduced as a lemma from the two properties above,
     but it seems simpler to maintain it as an invariant for now. *)
  finite_graph_edges :
    finite edges;
}.

Arguments vertices {N} g.
Arguments edges {N} g.

Obligation Tactic := idtac.

Program Definition graph_empty {N} : graph N :=
  MkGraph N \{} \{} _ _ _.
Next Obligation. intros. rew_set in *. tauto. Qed.
Next Obligation. intros. apply finite_empty. Qed.
Next Obligation. intros. apply finite_empty. Qed.

Program Definition graph_add_vertex {N} (G: graph N) (x: N) : graph N :=
  MkGraph N (vertices G \u \{x}) (edges G) _ _ _.
Next Obligation.
  intros N G x x' y' ?. rew_set. lets: edges_in_vertices G x' y'. tauto.
Qed.
Next Obligation.
  intros. apply finite_union. apply finite_graph. apply finite_single.
Qed.
Next Obligation. intros. apply finite_graph_edges. Qed.

Program Definition graph_add_edge {N} (G: graph N) (e : N * N) : graph N :=
  let '(x, y) := e in
  MkGraph N (vertices G)
    (If x \in vertices G /\ y \in vertices G then
       edges G \u \{(x, y)}
     else
       edges G)
    _ _ _.
Next Obligation.
  introv <-. intros x' y'. case_if.
  { unpack. rew_set. intros [?|HP]. now applys~ edges_in_vertices.
    inversion HP. substs~. }
  { intros ?. applys~ edges_in_vertices. }
Qed.
Next Obligation. intros. applys finite_graph. Qed.
Next Obligation.
  intros. case_if; [apply finite_union|]; try apply finite_graph_edges.
  apply finite_single.
Qed.

End MyGraph.

Instance MyGraphModel (N: Type): GraphModel N.
Proof.
  apply (@MkGraphModel
           N (MyGraph.graph N)
           MyGraph.graph_empty MyGraph.graph_add_vertex MyGraph.graph_add_edge
           MyGraph.vertices MyGraph.edges); auto.
  - apply MyGraph.edges_in_vertices.
  - apply MyGraph.finite_graph.
  - apply MyGraph.finite_graph_edges.
  - intros. unfold graph_add_edge. simpl. case_if~.
Qed.

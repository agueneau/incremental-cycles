Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the CF definitions. *)
Require Import Simple_sparse_ml.

Require Import ssreflect.
Require Import BigO.CFMLBigO.
From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation.

Require Import TLCBuffer LibTacticsExtra.
Require Import Psatz.

(******************************************************************************)

Lemma interruptible_fold_remaining_spec :
  spec1 [cost] (
    forall A B C Inv (l: list A) (f:func),
    (forall x xs (acc:B),
       suffix (x :: xs) l ->
       app f [x acc]
         PRE (Inv (x :: xs) (Continue acc))
         POST (fun (ret: interruptible_fold_step_ B C) =>
           Inv xs ret \* \$ cost tt)) ->
    forall acc,
    app interruptible_fold [f l acc]
      PRE (Inv l (Continue acc) \* \$ cost tt)
      POST (fun ret => Hexists xs,
        Inv xs ret \*
        \[ match ret with
           | Continue _ => xs = nil
           | Break _ => suffix xs l
           end ])
  ).
Proof.
  xspecO_refine recursive. intros costf M D ?. intros.
  pose (l' := l).
  enough (
    PRE (Inv l' (Continue acc) \* \$ costf tt)
    POST fun ret => Hexists xs, Inv xs ret \*
      \[ match ret with
         | Break _ => suffix xs l
         | Continue _ => xs = nil
         end ]
     CODE (app interruptible_fold [f l' acc])) by auto.
  pose (xs := @nil A).
  asserts~ HH: (xs ++ l' = l). gen acc. revert HH.
  generalize l' xs. subst xs l'.
  induction l'.
  { intros. rew_listx in HH. subst xs. xcf.
    weaken. xpay. xmatch. xrets~. rew_cost. defer. }
  { introv E. xcf.
    weaken. xpay. xmatch;[]. xlet as ret. xapps~.
    { rewrite <-E. unfold suffix. eexists. eauto. }
    xmatch.
    { (* Continue *) xapp (xs & a). rewrite <-E. rew_listx~. hsimpl~. }
    { (* Break *) xrets~. rewrite le_zarith. defer.
      unfold suffix. rewrite <-E. eexists. rewrite app_cons_r. eauto. }
    { rew_cost. defer. } }
  close_cost.

  exists (fun (_:unit) => 1). repeat split; try math.
  cleanup_cost. monotonic. dominated. math.
Qed.

Lemma interruptible_fold_remaining_spec_alt :
  spec1 [cost] (
    forall A B C Inv (l: list A) (f:func),
    (forall x xs (acc:B),
       suffix (x :: xs) l ->
       app f [x acc]
         PRE (Inv (x :: xs) (Continue acc))
         POST (fun (ret: interruptible_fold_step_ B C) => Inv xs ret)) ->
    forall acc,
    app interruptible_fold [f l acc]
      PRE (Inv l (Continue acc) \* \$ (length l * cost tt) \* \$ cost tt)
      POST (fun ret => Hexists xs,
        Inv xs ret \* \$ (length xs * cost tt) \*
        \[ match ret with
           | Continue _ => xs = nil
           | Break _ => suffix xs l
           end ])
  ).
Proof.
  xspecO (cost interruptible_fold_remaining_spec). introv Sf. intros.
  xapp_spec (spec interruptible_fold_remaining_spec) (>>
    (fun xs ret =>
       Inv xs ret \*
       \$ (length xs * cost interruptible_fold_remaining_spec tt))
  ).
  { intros. xapply~ (>>Sf xs). rew_list. hsimpl_credits.
    rewrite le_zarith. pols. apply Zle_minus_le_0, Z.le_refl. }
  now hsimpl~. auto with zarith. monotonic. dominated.
Qed.

Lemma interruptible_fold_remaining_spec_alt_debt :
  spec1 [cost] (
    forall A B C Inv (l: list A) (f:func),
    (forall x xs (acc:B),
       suffix (x :: xs) l ->
       app f [x acc]
         PRE (Inv (x :: xs) (Continue acc))
         POST (fun (ret: interruptible_fold_step_ B C) => Inv xs ret)) ->
    forall acc,
    app interruptible_fold [f l acc]
      PRE (Inv l (Continue acc))
      POST (fun ret => Hexists xs,
        Inv xs ret \* \$ ((length xs - length l - 1) * cost tt) \*
        \[ match ret with
           | Continue _ => xs = nil
           | Break _ => suffix xs l
           end ])
  ).
Proof.
  xspecO (cost interruptible_fold_remaining_spec). introv Sf. intros.
  xapp_spec (spec interruptible_fold_remaining_spec) (>>
    (fun xs ret =>
       Inv xs ret \*
       \$ (length xs * cost interruptible_fold_remaining_spec tt))
  ).
  { intros. xapply~ (>>Sf xs). rew_list. hsimpl_credits.
    rewrite le_zarith. pols. apply Zle_minus_le_0, Z.le_refl. }
  now hsimpl_credits. hsimpl~. hsimpl_credits.
  now set X := cost interruptible_fold_remaining_spec tt; math_lia.
  auto with zarith. monotonic. dominated.
Qed.

(******************************************************************************)

Ltac xbreak :=
  (* FIXME *) apply xret_lemma; hsimpl_autorefine_credits.
Tactic Notation "xbreak" "~" :=
  xbreak; auto_tilde.
Ltac xcontinue :=
  (* FIXME *) apply xret_lemma; hsimpl_autorefine_credits.
Tactic Notation "xcontinue" "~" :=
  xcontinue; auto_tilde.

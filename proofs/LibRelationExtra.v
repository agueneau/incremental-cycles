Require Import TLC.LibRelation TLC.LibTactics TLC.LibEqual.
Require Import List TLC.LibListZ.

Local Ltac auto_tilde ::= eauto.

Definition reverse {A} (R: binary A): binary A :=
  fun x y => R y x.

(* More in DFS/MyRelations.v, Section Reverse *)

Lemma use_path_reverse: forall A (R: binary A),
  forall v w,
  rtclosure (reverse R) w v ->
  rtclosure R v w.
Proof.
  induction 1 using rtclosure_ind_l.
  - apply rtclosure_refl.
  - apply rtclosure_r with y; auto.
Qed.

Lemma prove_path_reverse: forall A (R: binary A),
  forall v w,
  rtclosure R v w ->
  rtclosure (reverse R) w v.
Proof.
  induction 1 using rtclosure_ind_l.
  - apply rtclosure_refl.
  - apply rtclosure_r with y; auto.
Qed.

Lemma rtclosure_reverse: forall A (R: binary A),
  reverse (rtclosure R) = rtclosure (reverse R).
Proof.
  intros. extens. intros v w; split; intros.
  eauto using prove_path_reverse.
  unfold reverse. eauto using use_path_reverse.
Qed.

Lemma use_tclosure_reverse: forall A (R: binary A),
  forall v w,
  tclosure (reverse R) w v ->
  tclosure R v w.
Proof.
  induction 1 using tclosure_ind_l.
  - apply~ tclosure_once.
  - apply tclosure_r with y; auto.
Qed.

Lemma prove_tclosure_reverse: forall A (R: binary A),
  forall v w,
  tclosure R v w ->
  tclosure (reverse R) w v.
Proof.
  induction 1 using tclosure_ind_l.
  - apply~ tclosure_once.
  - apply tclosure_r with y; auto.
Qed.

Lemma tclosure_reverse: forall A (R: binary A),
  reverse (tclosure R) = tclosure (reverse R).
Proof.
  intros. extens. intros v w; split; intros.
  eauto using prove_tclosure_reverse.
  unfold reverse. eauto using use_tclosure_reverse.
Qed.

Lemma tclosure_inv_l' : forall A (R: binary A) (x y: A),
  tclosure R x y ->
  exists z, R x z /\ rtclosure R z y.
Proof.
  introv H. forwards [?|[z [? ?]]]: tclosure_inv_l H.
  { exists y; splits~. applys rtclosure_refl. }
  { exists z. splits~. applys~ rtclosure_of_tclosure. }
Qed.

(*****)

Inductive is_path {A} R : list (A * A) -> binary A :=
  | is_path_nil : forall x,
    is_path R nil x x
  | is_path_cons : forall x y z l,
    R x y ->
    is_path R l y z ->
    is_path R ((x, y) :: l) x z.

Lemma rtclosure_is_path A (R: binary A) : forall (x y: A),
  rtclosure R x y ->
  exists l, is_path R l x y.
Proof.
  induction 1 using rtclosure_ind_l.
  { exists (@nil (A*A)). auto using is_path_nil. }
  { destruct IHrtclosure as [l Hl]. exists ((x,y)::l).
    eauto using is_path_cons. }
Qed.

Lemma is_path_rtclosure A (R: binary A) : forall (x y: A) l,
  is_path R l x y ->
  rtclosure R x y.
Proof.
  induction 1; try now constructor~. eauto using rtclosure_l.
Qed.

Lemma is_path_nil_inv A (R: binary A): forall (x y:A),
  is_path R nil x y ->
  x = y.
Proof. introv H; inversion H; eauto. Qed.

Lemma is_path_cons_inv A (R: binary A): forall (x y u v:A) l,
  is_path R ((u, v) :: l) x y ->
  u = x /\ is_path R l v y /\ R x v.
Proof. introv H. inversion H; substs~. Qed.

Lemma is_path_last_inv A (R: binary A): forall (x y u v:A) l,
  is_path R (l & (u, v)) x y ->
  v = y /\ is_path R l x u /\ R u y.
Proof.
  introv. revert x y u v. induction l.
  { introv H. rew_listx in H. inversion H; subst.
    forwards~: is_path_nil_inv. subst. splits~.
    constructor~. }
  { introv H. rew_listx in H. inversion H; subst.
    forwards~ [? [? ?]]: IHl. subst. splits~. constructors~. }
Qed.

Lemma is_path_last A (R: binary A): forall (x y z: A) l,
  is_path R l x y ->
  R y z ->
  is_path R (l & (y, z)) x z.
Proof.
  introv. revert x y z.
  induction l; introv Hp ?; inversion Hp; subst; repeat constructors~.
Qed.

Lemma covariant_is_path A (R1 R2: binary A) (x y: A) l :
  is_path R1 l x y ->
  rel_incl R1 R2 ->
  is_path R2 l x y.
Proof.
  introv HP HI. induction HP; unfold rel_incl in *; constructors~.
Qed.

Lemma is_path_reverse A (R: binary A) (x y: A) l :
  is_path R l x y ->
  is_path (reverse R) (rev (map (fun '(x, y) => (y, x)) l)) y x.
Proof.
  revert x y. induction l using list_ind_last; intros.
  { intros. forwards~: is_path_nil_inv. subst. constructors~. }
  { destruct a. rew_listx. forwards~ [? [? ?]]: is_path_last_inv. subst.
    constructors~. }
Qed.

Lemma is_path_mapfst_mapsnd A (R: binary A) (x y: A) l :
  is_path R l x y ->
  LibList.map fst l & y = x :: LibList.map snd l.
Proof.
  introv. revert x y. induction l as [| [u v] ].
  { intros. rew_listx. forwards~: is_path_nil_inv. substs~. }
  { introv H. forwards~ [-> [? ?]]: is_path_cons_inv H. rew_listx~. simpl.
    erewrite~ IHl. }
Qed.

Lemma is_path_concat A (R: binary A) (x y z: A) l1 l2 :
  is_path R l1 x y ->
  is_path R l2 y z ->
  is_path R (l1 ++ l2) x z.
Proof.
  introv. revert x y z l2. induction l1 as [| [u v]].
  { intros. rew_list. forwards~: is_path_nil_inv. substs~. }
  { introv H1 H2. forwards~ [-> [? ?]]: is_path_cons_inv H1. rew_list.
    constructor~. }
Qed.

(***)

Create HintDb rel_crush.

Ltac rel_crush :=
  repeat autounfold with rel_crush;
  tauto.

Hint Unfold rel_incl reverse : rel_crush.

Ltac rel_covariant :=
  now (eapply covariant_rtclosure; [ | now autos~ ]; rel_crush).
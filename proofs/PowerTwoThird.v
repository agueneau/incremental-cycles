Set Implicit Arguments.

From Coq Require Import Reals.
From Coq.micromega Require Import Lra Lia.
From Coq Require Import ssreflect.

Require Import TLCBuffer.

Close Scope boolean_if_scope.
Open Scope general_if_scope.
Open Scope Z_scope.

(******************************************************************************)

(* Coq's Rpower function is undefined when its first parameter is 0. *)
Definition power_two_third (x : R) : R :=
  if Rle_dec x 0 then 0%R else Rpower x (2/3).

Lemma power_two_third_nonneg r :
  (0 <= power_two_third r)%R.
Proof.
  unfold power_two_third. destruct (Rle_dec r 0); [lra|].
  apply Rlt_le, exp_pos.
Qed.

Lemma power_two_third_mono r1 r2 :
  (r1 <= r2)%R ->
  (power_two_third r1 <= power_two_third r2)%R.
Proof.
  intros * Hr. rewrite {1}/power_two_third. destruct (Rle_dec r1 0).
  now apply power_two_third_nonneg.
  rewrite /power_two_third. destruct (Rle_dec r2 0).
  now exfalso; lra.
  apply Rle_Rpower_l; lra.
Qed.
From TLC Require Import LibFun LibTactics LibIntTactics.
Require Import ZArith.

Ltac invp :=
  match goal with H: (_,_) = (_,_) |- _ => inversion H end.

(* sig/sigT -> exists, using the [indefinite_description] axiom. *)

Require Import TLC.LibAxioms.

Lemma classify_sigT_sig : forall A B P,
  { a : A | exists (b : B),  P a b } ->
  sigT (fun a : A => { b : B | P a b }).
Proof.
  intros * H. destruct H as [a Ha]. exists a.
  apply indefinite_description. auto.
Qed.

Lemma weaken_sigT : forall A (P P' : A -> Type),
  (forall a, P a -> P' a) ->
  sigT P ->
  sigT P'.
Proof. intros * H X. destruct X. eauto. Qed.

Ltac classify_sigT_sig :=
  lazymatch goal with
  | |- sigT _ =>
    first [
      apply classify_sigT_sig
    | let x := fresh "x" in
      let X := fresh "X" in
      eapply weaken_sigT;
      [intros x X;
       classify_sigT_sig;
       apply X |];
      classify_sigT_sig
    ]
  end.

Ltac classifysig :=
  try classify_sigT_sig; apply indefinite_description.

Goal (exists a b, a + b = 0) ->
     sigT (fun a => sig (fun b => a +b = 0)).
  intro. classifysig. auto.
Qed.

Goal
  (exists a b c d e, a + b = c /\ d + e = 0) ->
  sigT (fun a => sigT (fun b => sigT (fun c => sigT (fun d => sig (fun e => a + b = c /\ d + e = 0))))).
  intro. classifysig. auto.
Qed.

Set Implicit Arguments.

From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation LibTactics.
Require Import TLCBuffer LibRelationExtra LibTacticsExtra.

Require Import GraphModel RawGraph.

Notation reachable := rtclosure.

Section Paths.

Context `{GM: GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)

Local Ltac auto_tilde ::= eauto with graph maths.

(******************************************************************************)

Definition incoming_level G L : binary vertex :=
  fun x y => has_edge G y x /\ L y = L x.

Hint Unfold incoming_level : rel_crush.

Definition outgoing_level G L : binary vertex :=
  fun x y => has_edge G x y /\ L y = L x.

Hint Unfold outgoing_level : rel_crush.

Lemma incoming_level_as_sets : forall G L,
  incoming_level G L =
  fun x => set_st (fun y => has_edge G y x /\ L y = L x).
Proof. reflexivity. Qed.

(******************************************************************************)

Definition src_marked M (m: mark) : binary vertex :=
  fun x y => M x = m.

Definition marked M (m: mark) : binary vertex :=
  fun x y => M x = m /\ M y = m.

Hint Unfold src_marked marked : rel_crush.

Lemma marked_fupdate_covariant : forall M m x,
  rel_incl (marked M m) (marked (fupdate M x m) m).
Proof.
  intros. unfold rel_incl, marked. intros. rewrite !fupdate_eq.
  unpack. repeat case_if~.
Qed.

Lemma src_marked_fupdate_covariant : forall M m x,
  rel_incl (src_marked M m) (src_marked (fupdate M x m) m).
Proof.
  intros. unfold rel_incl, src_marked. intros. rewrite !fupdate_eq.
  unpack. repeat case_if~.
Qed.

Lemma reachable_marked_of_src_marked : forall M m x y,
  reachable (src_marked M m) x y ->
  M y = m ->
  reachable (marked M m) x y.
Proof.
  introv H Hy.
  applys rtclosure_ind_r
        (src_marked M m)
        (fun x y => M y = m -> reachable (marked M m) x y); auto.
  { intros. apply rtclosure_refl. }
  { intros y' x' z' ? Hpath Hedge ?. unfold src_marked in Hedge.
    specializes~ Hpath. applys~ rtclosure_r y'. unfolds~ marked. }
Qed.

(******************************************************************************)

Lemma reachable_incoming_level : forall G L x y,
  reachable (incoming_level G L) x y ->
  L x = L y.
Proof.
  introv. pose ltac_mark; induction 1 using rtclosure_ind_l; gen_until_mark.
  - auto.
  - intros y x z Exy Ryz Lyz. unfold incoming_level in Exy. math.
Qed.

Lemma incoming_level_of_mem_incoming_level : forall G L ys x y,
  to_set ys = incoming_level G L x -> mem y ys -> incoming_level G L x y.
Proof.
  introv HI M. rewrite mem_to_set, HI in M.
  apply M. (* sketchy *)
Qed.

Hint Resolve incoming_level_of_mem_incoming_level.

Lemma has_edge_of_incoming_level : forall G L x y,
  incoming_level G L x y -> has_edge G y x.
Proof.
  unfold incoming_level. tauto.
Qed.

Hint Resolve has_edge_of_incoming_level.

Lemma exploit_reachable_incoming_level : forall G L v x,
  reachable (incoming_level G L) v x ->
  L x = L v /\ reachable (has_edge G) x v.
Proof.
  introv HR.
  splits. now forwards~: reachable_incoming_level.
  applys covariant_rtclosure; cycle 1.
  now apply use_path_reverse, HR. rel_crush.
Qed.

Lemma has_edge_of_mem_outgoing : forall G ys x y,
  to_set ys = out_edges G x -> mem y ys -> has_edge G x y.
Proof.
  introv H ?. rewrite <-out_edges_has_edge. rewrite~ <-H.
Qed.

Hint Resolve has_edge_of_mem_outgoing.

Lemma mem_of_has_edge : forall G ys x y,
  to_set ys = out_edges G x -> has_edge G x y -> mem y ys.
Proof.
  introv H E. rewrite <-out_edges_has_edge in E. rewrites~ <-H in *.
Qed.

Hint Resolve mem_of_has_edge.

(******************************************************************************)

Definition mkedges {A} (x: A) (ys: list A) :=
  LibList.map (fun (y:A) => (x, y)) ys.

Lemma mkedges_suffix : forall A (x:A) (ys ys': list A),
  suffix ys ys' ->
  suffix (mkedges x ys) (mkedges x ys').
Proof.
  introv (zs & Hzs).
  exists (mkedges x zs). unfold mkedges. rewrite <-Hzs.
  rew_listx~.
Qed.

Hint Resolve mkedges_suffix.

Lemma mem_map_tuple_r : forall A (x x' y: A) (l: list A),
  mem (x', y) (map (fun y:A => (x, y)) l) ->
  mem y l.
Proof.
  introv M. forwards M': mem_map (@snd A A) M. simpl in *.
  equates 1. apply M'. rewrite list_map_compose, map_id_ext; auto.
Qed.

Lemma mem_map_tuple_l : forall A (x x' y: A) (l: list A),
  mem (x', y) (map (fun y:A => (x, y)) l) ->
  x = x'.
Proof.
  introv M.
  forwards M': mem_map (@fst A A) M. simpl in *.
  rewrite list_map_compose in M'. unfold compose in M'. simpl in M'.
  applys~ Forall_mem_inv. apply Forall_map. rewrite~ Forall_eq_forall_mem.
Qed.

Lemma noduplicates_mkedges : forall A (x:A) (ys:list A),
  noduplicates ys ->
  noduplicates (mkedges x ys).
Proof.
  introv H. unfold mkedges.
  induction H as [| ? ? Ml NDl] using noduplicates_ind; rew_listx.
  apply noduplicates_nil. apply~ noduplicates_cons.
  intros M. forwards~: mem_map_tuple_r M.
Qed.

Hint Resolve noduplicates_mkedges.

Lemma not_mem_mkedges : forall A (x x' y: A) (ys: list A),
  x <> x' ->
  ~ mem (x', y) (mkedges x ys).
Proof.
  introv Hxx' M. forwards~: mem_map_tuple_l M.
Qed.

Hint Resolve not_mem_mkedges.

Lemma mem_mkedges : forall A (x y: A) (ys: list A),
  mem y ys ->
  mem (x, y) (mkedges x ys).
Proof.
  intros. unfold mkedges. apply~ mem_map.
Qed.

Hint Resolve mem_mkedges.

(* TODO: generalize *)
Lemma mem_to_set_incoming : forall G L x y ys,
  to_set ys = incoming_level G L x ->
  incoming_level G L x y ->
  mem y ys.
Proof.
  introv Hys Hinc. rewrite mem_to_set, Hys.
  apply Hinc.
Qed.

Hint Resolve mem_to_set_incoming.

Lemma mkedges_cons : forall x y ys,
  mkedges x (y :: ys) = (x, y) :: mkedges x ys.
Proof.
  intros. unfold mkedges. rew_listx~.
Qed.

(******************************************************************************)

Definition edges_at_level G L (l: int): set (vertex * vertex) :=
  set_st (fun '(x, y) => has_edge G x y /\ L x = l /\ L y = l).

Lemma edges_at_level_finite : forall G L l,
  finite (edges_at_level G L l).
Proof.
  intros.
  assert (edges_at_level G L l \c edges G).
  { rew_set. intros [x y]. unfold edges_at_level. rew_set. tauto. }
  eauto using finite_incl, finite_graph_edges.
Qed.

Hint Resolve edges_at_level_finite.

(******************************************************************************)

Definition coacc_edges G (z: vertex): set (vertex * vertex) :=
  set_st (fun '(x, y) => has_edge G x y /\ reachable (has_edge G) y z).

Definition coacc_edges_at_level G L (l: int) (z: vertex): set (vertex * vertex) :=
  edges_at_level G L l \n coacc_edges G z.

Lemma coacc_edges_at_level_finite : forall G L l z,
  finite (coacc_edges_at_level G L l z).
Proof.
  intros. applys~ finite_inter.
Qed.

(******************************************************************************)

Definition with_parents {A} P (R: binary A) :=
  fun (x y: A) => R x y /\ P y = Some x.

Definition with_rev_parents {A} P (R: binary A) :=
  fun (x y: A) => R x y /\ P x = Some y.

(******************************************************************************)

Definition in_visited {A} (vis: set A) (R: binary A) :=
  fun (x y: A) => R x y /\ x \in vis /\ y \in vis.

End Paths.

Hint Unfold incoming_level outgoing_level : rel_crush.
Hint Unfold src_marked marked : rel_crush.
Hint Unfold with_parents with_rev_parents : rel_crush.
Hint Unfold in_visited : rel_crush.

Hint Resolve incoming_level_of_mem_incoming_level.
Hint Resolve has_edge_of_incoming_level.
Hint Resolve has_edge_of_mem_outgoing.
Hint Resolve mem_of_has_edge.
Hint Resolve mkedges_suffix.
Hint Resolve noduplicates_mkedges.
Hint Resolve not_mem_mkedges.
Hint Resolve mem_mkedges.
Hint Resolve mem_to_set_incoming.
Hint Resolve edges_at_level_finite.
Hint Resolve coacc_edges_at_level_finite.

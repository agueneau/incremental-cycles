Set Implicit Arguments.

From TLC Require Import LibInt LibRelation LibSet LibMap LibLogic LibTactics
                        LibEpsilon LibWf LibFun LibIntTactics.
Require Import ssreflect.
Require Import TLCBuffer.
Require Import LibRelationExtra.
Require Import GraphModel Paths.

Require Import RawGraph.

Open Scope graph_scope.
Open Scope Z_scope.

Local Ltac auto_tilde ::= eauto with maths graph.

(******************************************************************************)
(* Functional invariants *)

Section GraphFunctionalInvariants.

Context `{GM: GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)
Implicit Types E : set vertex.

Definition levels_pseudotopo G L :=
  forall x y,
    has_edge G x y ->
    L x <= L y.

Definition enough_edges_below G L x :=
  let k := L x - 1 in
  k <= card (coacc_edges_at_level G L k x).

Definition parents_in_graph G P :=
  forall x y,
  y \in vertices G -> P y = Some x ->
  x \in vertices G.

Record InvExcept E G L I P := {
  Inv_acyclic :
    acyclic G;
  Inv_pseudotopo :
    levels_pseudotopo G L;
  Inv_levels :
    forall x, 1 <= L x;
  Inv_incoming :
    I = incoming_level G L;
  Inv_parents_in_graph :
    parents_in_graph G P;
  Inv_edges_below_except :
    forall x, x \in E \/ enough_edges_below G L x;
}.

Definition Inv := InvExcept \{}.

Definition I_add_edge L I x y :=
  If L x = L y then fupdate_add I y x else I.

Lemma parents_in_graph_forget : forall G P x,
  parents_in_graph G P ->
  parents_in_graph G (fupdate P x None).
Proof. introv HP. unfolds. intros ? ? ? HH. fupdate_case_in HH. Qed.

Lemma Inv_forget_parent : forall G L I P x,
  Inv G L I P ->
  Inv G L I (fupdate P x None).
Proof.
  introv HI. constructors; try now apply HI.
  applys parents_in_graph_forget. apply HI.
Qed.

Lemma Inv_update_parent : forall G L I P x y,
  Inv G L I P ->
  y \in vertices G ->
  x \in vertices G ->
  Inv G L I (fupdate P y (Some x)).
Proof.
  introv HI ? ?. constructor; try now apply HI.
  intros ? ? ? HH. fupdate_case_in HH.
  subst; congruence. apply~ HI.
Qed.

Lemma Inv_edges_below : forall G L I P,
  Inv G L I P ->
  forall x, enough_edges_below G L x.
Proof. intros. forwards~ [|]: Inv_edges_below_except x. rew_set in *. false. Qed.

Lemma Inv_incoming_extens : forall G L I P x y,
  Inv G L I P ->
  y \in I x ->
  incoming_level G L x y.
Proof.
  intros. rewrites~ <-(>> Inv_incoming).
Qed.

Lemma Inv_incoming_in_dom : forall G L I P x,
  Inv G L I P ->
  foreach (fun y => y \in vertices G) (I x).
Proof.
  introv HInv. intros y Hy.
  rewrites~ (>>Inv_incoming) in Hy.
  forwards: edges_in_vertices y x;
  repeat (match goal with H: _ |- _ => eapply H end; eauto).
Qed.

Lemma pseudotopo_reachable : forall G L x y,
  levels_pseudotopo G L ->
  reachable (has_edge G) x y ->
  L x <= L y.
Proof.
  introv LP R. intros.
  applys~ rtclosure_ind_l (has_edge G) (fun x y => L x <= L y).
  introv He. forwards~: LP.
Qed.

Lemma Inv_pseudotopo_reachable : forall G L I P x y,
  Inv G L I P ->
  reachable (has_edge G) x y ->
  L x <= L y.
Proof.
  intros. forwards~: pseudotopo_reachable. applys~ Inv_pseudotopo.
Qed.

Lemma Inv_reachable_same_level : forall G L I P x y,
  Inv G L I P ->
  reachable (has_edge G) x y ->
  L x = L y ->
  reachable (incoming_level G L) y x.
Proof.
  introv HInv H. apply prove_path_reverse in H. revert x y H.
  lets: ltac_mark. induction 1 using rtclosure_ind_l; gen_until_mark.
  { intros. apply rtclosure_refl. }
  { intros y x z Hedge_xy Hreachable_yz Hreachable_yz_level Hlevel_xz.
    assert (L y = L x).
    { forwards~: Inv_pseudotopo Hedge_xy.
      rewrite <-rtclosure_reverse in Hreachable_yz.
      forwards~: Inv_pseudotopo_reachable Hreachable_yz. }
    applys~ rtclosure_l y. unfold incoming_level. splits~. }
Qed.

Lemma edges_at_level_add_edge_incl : forall G L l x y,
  x \in vertices G ->
  y \in vertices G ->
  edges_at_level G L l \c edges_at_level (G \+> (x, y)) L l.
Proof.
  introv Hx Hy. unfold edges_at_level. rew_set. intros [u v]. rew_set.
  intros (? & ? & ?). splits~.
Qed.

Lemma coacc_edges_add_edge_incl : forall G z x y,
  x \in vertices G ->
  y \in vertices G ->
  coacc_edges G z \c coacc_edges (G \+> (x, y)) z.
Proof.
  introv Hx Hy. rew_set. intros [u v]. unfold coacc_edges. rew_set.
  intros (Huv & HR). splits~. applys~ covariant_rtclosure.
Qed.

Lemma coacc_edges_reachable : forall G x y,
  reachable (has_edge G) x y ->
  coacc_edges G x \c coacc_edges G y.
Proof.
  introv HR. rew_set. intros [u v]. unfold coacc_edges. rew_set.
  intros (?&?). splits~. applys~ rtclosure_trans.
Qed.

Lemma coacc_edges_at_level_new_levels : forall G L L' l y,
  (forall x,
     reachable (has_edge G) x y ->
     L x = l ->
     L' x = L x) ->
  coacc_edges_at_level G L l y \c coacc_edges_at_level G L' l y.
Proof.
  introv H.
  unfold coacc_edges_at_level. rew_set. intros [u v].
  unfold edges_at_level, coacc_edges. rew_set.
  intros; unpack. splits~. split~.
  rewrite~ (H u). rewrite~ (H v). apply~ (>>rtclosure_l v).
Qed.

Lemma coacc_edges_at_level_add_vertex : forall G L l x z,
  coacc_edges_at_level (G \++ x) L l z = coacc_edges_at_level G L l z.
Proof.
  intros.
  rewrite /coacc_edges_at_level /coacc_edges /edges_at_level /has_edge.
  rewrite edges_add_vertex //.
Qed.

Lemma enough_edges_below_add_edge1 : forall G L x y z,
  x \in vertices G ->
  y \in vertices G ->
  enough_edges_below G L z ->
  enough_edges_below (G \+> (y, x)) L z.
Proof.
  introv Hx Hy H. unfold enough_edges_below in *.
  transitivity (card (coacc_edges_at_level G L (L z - 1) z)); auto.
  apply~ card_le_of_incl'.
  unfold coacc_edges_at_level.
  forwards~: edges_at_level_add_edge_incl G L (L z - 1) y x.
  forwards~: coacc_edges_add_edge_incl G z y x.
  set_prove.
Qed.

Hint Resolve enough_edges_below_add_edge1.

Lemma enough_edges_below_reachable : forall G L x y,
  enough_edges_below G L x ->
  reachable (has_edge G) x y ->
  L x = L y ->
  enough_edges_below G L y.
Proof.
  introv Hx Hxy HLxy. unfold enough_edges_below in *.
  transitivity (card (coacc_edges_at_level G L (L x - 1) x)); autos~.
  apply~ card_le_of_incl'. rewrite HLxy. unfold coacc_edges_at_level.
  forwards~: coacc_edges_reachable x y.
  set_prove.
Qed.

Lemma enough_edges_below_add_edge2 : forall G L x y,
  x \in vertices G ->
  y \in vertices G ->
  enough_edges_below G L x ->
  L x = L y ->
  enough_edges_below (G \+> (x, y)) L y.
Proof.
  introv ? ? Hx HLxy.
  applys~ enough_edges_below_reachable. applys~ rtclosure_once.
Qed.

Lemma enough_edges_below_add_edge3 : forall G L x y,
  x \in vertices G ->
  y \in vertices G ->
  L x <= card (coacc_edges_at_level G L (L x) x) ->
  L y = L x + 1 ->
  enough_edges_below (G \+> (x, y)) L y.
Proof.
  introv ? ? HLx HEx. unfold enough_edges_below.
  rewrite HEx. ring_simplify. transitivity (card (coacc_edges_at_level G L (L x) x)); autos~.
  apply~ card_le_of_incl'. unfold coacc_edges_at_level.
  forwards~: edges_at_level_add_edge_incl G L (L x) x y.
  forwards~: coacc_edges_add_edge_incl G x x y.
  forwards~: coacc_edges_reachable (G \+> (x, y)) x y. now apply~ rtclosure_once.
  set_prove.
Qed.

Lemma enough_edges_below_add_vertex : forall G L x y,
  enough_edges_below G L y ->
  enough_edges_below (G \++ x) L y.
Proof.
  introv HE. unfold enough_edges_below in *.
  rewrite coacc_edges_at_level_add_vertex //.
Qed.

Lemma Inv_init : Inv graph_empty (const 1) (const \{}) (const None).
Proof.
  constructor; try (introv; reflexivity).
  { apply acyclic_empty. }
  { rewrite incoming_level_as_sets /const.
    apply fun_ext_1; intro. rew_set. intro.
    rew_set. rewrite /has_edge graph_empty_edges. rew_set. tauto. }
  { unfold const. intros. congruence. }
  { intro x. right. unfold enough_edges_below. unfold const. math. }
Qed.

Lemma Inv_add_edge : forall G L I P E x y,
  x \in vertices G ->
  y \in vertices G ->
  InvExcept E G L I P ->
  ~ reachable (has_edge G) y x ->
  L x <= L y ->
  (forall z, enough_edges_below (G \+> (x, y)) L z) ->
  Inv (G \+> (x, y)) L (I_add_edge L I x y) P.
Proof.
  introv Hx Hy HInv Hpath HLxy Henough.
  constructor; try (solve [ autos | apply HInv ]).
  { applys~ acyclic_add_edge. apply HInv. }
  { unfold levels_pseudotopo. intros x' y' Hx'y'.
    rewrite~ has_edge_add_edge in Hx'y'. branches; [now apply~ HInv|].
    unpack; substs~. }
  { rewrite (Inv_incoming HInv) !incoming_level_as_sets.
    apply fun_ext_1. intro x'. rew_set. intro y'.
    unfold I_add_edge; case_if; rew_set; rewrite~ has_edge_add_edge.
    unfold fupdate_add; fupdate_case; subst; rew_set.
    all: split; intro; repeat first [ branches~ | progress unpack | substs~ ]; false~. }
  { intro. rewrite vertices_add_edge. apply HInv. }
Qed.

Lemma Inv_add_vertex : forall G L I P x,
  Inv G L I P ->
  P x = None ->
  Inv (G \++ x) L I P.
Proof.
  introv HInv HPx. constructor; try solve [ apply HInv ].
  { applys~ acyclic_add_vertex. apply HInv. }
  { unfold levels_pseudotopo. intros x' y' Hx'y'.
    rewrite /has_edge edges_add_vertex in Hx'y'. apply~ HInv. }
  { rewrite (Inv_incoming HInv) !incoming_level_as_sets.
    apply fun_ext_1. intro x'. rew_set. intro y'. rew_set.
    rewrite /has_edge edges_add_vertex //. }
  { intro y. rewrite vertices_add_vertex. intro z. rew_set.
    lets: Inv_parents_in_graph HInv y z. intros. branches~. congruence. }
  { intros z. right. rewrite /enough_edges_below. intros HLz.
    apply~ enough_edges_below_add_vertex. apply~ Inv_edges_below. }
Qed.

End GraphFunctionalInvariants.

Hint Resolve enough_edges_below_add_edge1.
Hint Resolve parents_in_graph_forget Inv_forget_parent.

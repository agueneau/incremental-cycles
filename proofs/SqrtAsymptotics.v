Set Implicit Arguments.

From Coq Require Import Reals.
From Coq.micromega Require Import Lra Lia.
From Coq Require Import ssreflect.

From TLC Require Import LibTactics LibLogic.
From BigO Require Import Filter FilterNary Dominated DominatedNary Generic.

Require Import TLCBuffer.

Open Scope Z_scope.

Local Ltac auto_tilde ::= eauto with maths.
Local Ltac done ::= solve [autos~].

(******************************************************************************)

Lemma dominated_sqrt_up A f g :
  ultimately A (fun x => 0 <= g x) ->
  dominated A f g ->
  dominated A (fun x => Z.sqrt_up (f x)) (fun x => Z.sqrt_up (g x)).
Proof.
  intros P D.
  forwards (c & c_pos & U): dominated_nonneg_const D.
  exists (Z.sqrt_up c).
  revert P U; filter_closed_under_intersection.
  introv (Hg & H).
  rewrite (Z.abs_eq (g a)) // in H.
  rewrite !(Z.abs_eq (Z.sqrt_up _)); try apply Z.sqrt_up_nonneg.
  tests: (0 <= f a).
  { rewrite (Z.abs_eq (f a)) // in H.
    rewrite (Z.sqrt_up_le_mono _ _ H). apply~ Z.sqrt_up_mul_above. }
  { rewrite Z.sqrt_up_eqn0. lia. eauto using Z.sqrt_up_nonneg with zarith. }
Qed.

Hint Resolve dominated_sqrt_up : dominated.

Lemma dominated_sqrt_up_nary domain N f g :
  ultimately (nFilterType domain N) (Fun' (fun p => 0 <= App g p)) ->
  dominated (nFilterType domain N) (Uncurry f) (Uncurry g) ->
  dominated (nFilterType domain N)
    (Fun' (fun p => Z.sqrt_up (App f p)))
    (Fun' (fun p => Z.sqrt_up (App g p))).
Proof. prove_nary dominated_sqrt_up. Qed.

Hint Extern 2 (dominated _ (fun '(_,_) => Z.sqrt_up _) (fun '(_,_) => Z.sqrt_up _)) =>
  apply_nary dominated_sqrt_up_nary : dominated.

Lemma limit_sqrt_up : limit Z_filterType Z_filterType Z.sqrt_up.
Proof.
  rewrite limitPZ. intros y. rewrite ZP.
  tests: (0 <= y).
  { exists (y * y). intros n Hn.
    rewrite -(Z.sqrt_up_le_mono (y * y)) //.
    rewrite Z.sqrt_up_square //. }
  { exists 0. intros. rewrite -Z.sqrt_up_nonneg //. }
Qed.

Hint Resolve limit_sqrt_up : limit.

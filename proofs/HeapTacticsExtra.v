Require Import CFML.CFLib.
Require Import BigO.HeapTactics BigO.CFMLBigO.
Require Import ssreflect.

Class GetFrame (h1 h2 h3 : hprop) :=
  MkGetFrame: h1 = h2 \* h3.

Class GetFrame' (h1 h2 h3 : hprop) :=
  MkGetFrame': GetFrame h1 h2 h3.

Instance GetFrame_of': forall h1 h2 h3,
  GetFrame' h1 h2 h3 ->
  GetFrame h1 h2 h3.
Proof. eauto. Qed.

Hint Mode GetFrame' ! - - : typeclass_instances.

Instance GetFrame'_star_l: forall h1 h1' f h2 h1'h2,
  GetFrame h1 h1' f ->
  Star h1' h2 h1'h2 ->
  GetFrame' (h1 \* h2) h1'h2 f.
Proof.
  introv -> <-. unfold GetFrame', GetFrame.
  rewrite -!star_assoc. fequal.
  now rewrite star_comm.
Qed.

Instance GetFrame'_star_r: forall h1 h2 h2' f h1h2',
  GetFrame h2 h2' f ->
  Star h1 h2' h1h2' ->
  GetFrame' (h1 \* h2) h1h2' f.
Proof.
  introv -> <-. unfold GetFrame', GetFrame.
  now rewrite -!star_assoc.
Qed.

Instance GetFrame_evar: forall h,
  IsEvar h ->
  GetFrame h \[] h.
Proof. introv _. unfold GetFrame. now rewrite star_neutral_l. Qed.

Lemma piggybank_split_for_later: forall h1 h1' p p1 p2 h2 h2' hf H p1h1' h2'H,
  GetCreditsEvar h1 h1' p ->
  GetFrame h2 h2' hf ->
  Unify p (p1 + p2) ->
  Unify hf (\$ ⟨p2⟩ \* H) ->
  Star (\$ ⟨p1⟩) h1' p1h1' ->
  Star h2' H h2'H ->
  p1h1' ==> h2'H ->
  h1 ==> h2.
Proof.
  introv -> -> -> -> <- <- HH. rewrite credits_split_eq credits_refine_eq.
  rewrite credits_refine_eq in HH. hchange HH. hsimpl.
Qed.

Ltac piggybank_split_for_later :=
  eapply piggybank_split_for_later; [ once (typeclasses eauto) .. |].

(******************************************************************************)

Definition big_opp (l: list int): list int :=
  List.map Z.opp l.

Lemma big_opp_cancel_credits: forall l,
  \[] ==> \$* l \* \$* (big_opp l).
Proof.
  induction l; rewrite //= ?credits_list_nil ?credits_list_cons.
  { hsimpl. }
  { hchange IHl. hsimpl_credits.
    match goal with |- \$ ?x ==> _ => ring_simplify x end.
    rewrite credits_zero_eq. hsimpl. }
Qed.

Class OppIntList (l1 l2 : list int) :=
  MkOppIntList : l2 = big_opp l1.

Hint Mode OppIntList ! - : typeclass_instances.

Instance OppIntList_nil:
  OppIntList nil nil.
Proof. reflexivity. Qed.

Instance OppIntList_cons: forall x l l',
  OppIntList l l' ->
  OppIntList (x :: l) (-x :: l').
Proof. introv ->. reflexivity. Qed.

Lemma move_credits_r2l : forall h1 h2 h2' cs cs' hcs,
  GetCredits h2 h2' cs ->
  OppIntList cs cs' ->
  CreditsHeap cs' hcs ->
  h1 \* hcs ==> h2' ->
  h1 ==> h2.
Proof.
  introv -> -> -> HH.
  hchange (big_opp_cancel_credits cs).
  hchange HH. hsimpl.
Qed.

Ltac credr2l :=
  eapply move_credits_r2l; [ once (typeclasses eauto) .. |].

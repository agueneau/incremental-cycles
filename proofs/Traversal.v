Set Implicit Arguments.

From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation LibTactics LibLogic.

Require Import TLCBuffer LibRelationExtra LibTacticsExtra.
Require Import GraphModel Paths.

Open Scope set_scope.
Open Scope graph_scope.

Local Ltac auto_tilde ::= eauto with graph maths.

(******************************************************************************)

Section Traversal.

Context {vertex} `{GM: GraphModel vertex}.

Record Inv
       (G: graph vertex) (P0 P: vertex -> option vertex)
       (edge: vertex -> vertex -> Prop)
       (source: vertex) (target: set vertex)
       (visited: set vertex) (visited_edges: set (vertex * vertex))
       (stack: list vertex) (processing: list (vertex * vertex))
  :=
{
  Inv_source: source \in visited;
  Inv_target: target \# visited;

  Inv_visited: visited \c vertices G;

  Inv_visited_edges_edge: forall x y,
    (x, y) \in visited_edges -> edge x y;

  Inv_visited_edges_visited: forall x y,
    (x, y) \in visited_edges -> x \in visited /\ y \in visited;

  Inv_stack: Forall (fun x => x \in visited) stack;

  Inv_stack_nodup: noduplicates stack;

  Inv_processing: Forall (fun '(x, y) =>
         x \in visited
      /\ edge x y
    ) processing;

  Inv_processing_nodup: noduplicates processing;

  Inv_visited_reachable: forall x,
    x \in visited -> reachable (in_visited visited (with_parents P edge)) source x;

  Inv_visited_edge: forall x y,
    x \in visited -> edge x y ->
    xor3 ((x, y) \in visited_edges)
         (mem x stack)
         (mem (x, y) processing);

  Inv_edge: forall x y,
    edge x y -> x \in vertices G /\ y \in vertices G;

  Inv_source_parent: P source = None;

  Inv_parents_modified: forall x, P x = P0 x \/ x \in visited;
}.

(******************************************************************************)

Lemma Inv_visited_reachable':
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  forall x,
  x \in visited ->
  reachable edge source x.
Proof.
  intros. applys~ covariant_rtclosure.
  2: applys~ Inv_visited_reachable. rel_crush.
Qed.

Lemma Inv_parent_edge:
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  y \in visited ->
  P y = Some x ->
  edge x y /\ x \in visited.
Proof.
  introv HI Hy Hyx.
  forwards~ Hpath: Inv_visited_reachable HI y.
  lets [?|[z [? [[? ?] [? ?]]]]]: rtclosure_inv_r Hpath.
  { false. forwards~: Inv_source_parent HI. congruence. }
  { assert (x = z) as -> by congruence. splits~. }
Qed.

Lemma Inv_visited_is_vertex :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  x \in visited ->
  x \in vertices G.
Proof.
  introv HI Hx.
  forwards~ HH: Inv_visited HI. rew_set in HH. applys~ HH.
Qed.

Lemma Inv_in_stack_visited :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  mem x stack ->
  x \in visited.
Proof.
  introv HI HM. lets H: Inv_stack HI.
  applys~ Forall_mem_inv H.
Qed.

Hint Resolve Inv_in_stack_visited.

Lemma Inv_in_processing_source_visited :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  mem (x, y) processing ->
  x \in visited.
Proof.
  introv HI HM. lets H: Inv_processing HI.
  forwards~: Forall_mem_inv H. simpl in *. tauto.
Qed.

Hint Resolve Inv_in_processing_source_visited.

Lemma Inv_in_processing_is_edge :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  mem (x, y) processing ->
  edge x y.
Proof.
  introv HI HM. lets H: Inv_processing HI.
  forwards~: Forall_mem_inv H. simpl in *. tauto.
Qed.

Lemma Inv_visited_in_processing_notin_stack :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  x \in visited ->
  mem (x, y) processing ->
  ~ mem x stack.
Proof.
  introv HI Hx HM. forwards~: Inv_visited_edge HI x y.
  applys~ Inv_in_processing_is_edge. branchx; tauto.
Qed.

Hint Resolve Inv_visited_in_processing_notin_stack.

Lemma Inv_visited_in_processing_notin_visited_edges :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  mem (x, y) processing ->
  (x, y) \notin visited_edges.
Proof.
  introv HI HM. forwards~: Inv_visited_edge HI x y.
  applys~ Inv_in_processing_is_edge. branchx; tauto.
Qed.

Hint Resolve Inv_visited_in_processing_notin_visited_edges.

Lemma Inv_parent_visited:
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  x \in visited ->
  P x = Some y ->
  y \in visited.
Proof.
  introv HI Hx Hxy. forwards~ [? ?]: Inv_parent_edge Hxy.
Qed.

Lemma Inv_parent_reachable_visited:
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack processing x y,
  Inv G P0 P edge source target visited visited_edges stack processing ->
  y \in visited ->
  reachable (with_parents P edge) x y ->
  x \in visited.
Proof.
  introv ? ?.
  pose ltac_mark; induction 1 using rtclosure_ind_r; gen_until_mark. auto.
  introv [? ?]. intros. forwards~: Inv_parent_visited.
Qed.

Lemma Inv_stack_pop :
  forall (G: graph vertex) P0 P edge source target visited visited_edges stack x ys,
  to_set ys = edge x -> (* XX *)
  noduplicates ys ->
  Inv G P0 P edge source target visited visited_edges (x :: stack) nil ->
  Inv G P0 P edge source target visited visited_edges stack (mkedges x ys).
Proof.
  introv Hys NDys HI.
  constructor~; try now apply HI.
  { forwards~: Forall_cons_inv (Inv_stack HI). tauto. }
  { forwards~: Inv_stack_nodup. forwards~ [? ?]: noduplicates_cons_inv stack. }
  { apply Forall_map. rewrite Forall_eq_forall_mem. intros y Hy. splits~;[].
    rewrite mem_to_set in Hy. rewrite Hys in Hy. apply Hy. (* XX *) }
  { intros x' y Hx' Hx'y. forwards~: Inv_visited_edge HI x' y. rew_listx in *.
    forwards~ [? ?]: noduplicates_cons_inv stack; [now apply HI|].
    branchx.
    { xor3. }
    { branches; subst.
      { apply~ xor3_right. apply mem_mkedges.
        rewrite mem_to_set. rewrite Hys. apply Hx'y. (* XX *) }
      { xor3. apply not_mem_mkedges. tests~: (x = x'). } }
    { xor3. false. } }
Qed.

Lemma L: forall (P: vertex -> option vertex) edge x y t w,
  reachable (with_parents P edge) w t ->
  ~ reachable (with_parents P edge) y t ->
  reachable (with_parents (fupdate P y (Some x)) edge) w t.
Proof.
  introv.
  pose ltac_mark; induction 1 using rtclosure_ind_l; gen_until_mark.
  { intros. apply rtclosure_refl. }
  intros u v w Hvu Huw Huw' nHvy.
  apply rtclosure_l with u.
  { unfolds. destruct Hvu. splits~. fupdate_case. }
  { apply Huw'. intro. apply nHvy; auto. }
Qed.

Lemma LL: forall (P: vertex -> option vertex) (edge: binary vertex) x y t w,
  edge x y ->
  reachable (with_parents P edge) w t ->
  reachable (with_parents (fupdate P y (Some x)) edge) w x ->
  reachable (with_parents (fupdate P y (Some x)) edge) w t.
Proof.
  introv Hxy.
  pose ltac_mark; induction 1 using rtclosure_ind_r; gen_until_mark.
  { intros. apply rtclosure_refl. }
  intros u v w Hvu Huw Huw' nHvy. destruct Huw.
  tests: (w = y).
  { apply rtclosure_r with x; auto. splits~. fupdate_case. }
  { apply rtclosure_r with u; auto. splits~. fupdate_case. }
Qed.

Lemma LLL: forall (P: vertex -> option vertex) (edge: binary vertex) x y t w,
  edge x y ->
  reachable (with_parents P edge) w t ->
  reachable (with_parents P edge) w x ->
  ~ reachable (with_parents P edge) y x ->
  reachable (with_parents (fupdate P y (Some x)) edge) w t.
Proof. intros. apply~ LL. apply~ L. Qed.

Hint Unfold in_visited : rel_crush.

Ltac rel_crush' :=
  autounfold with rel_crush; intros; rew_set; try fupdate_case; subst; tauto.

Lemma Inv_visit_edge_aux :
  forall (drop_edge: bool) (G: graph vertex) P0 P P' (edge: vertex -> vertex -> Prop) source target visited visited_edges stack x y ys,
  y \notin target ->
  (if drop_edge then
    y \notin visited -> forall y',
      edge y y' ->
      xor3 ((y, y') \in visited_edges)
           (mem y stack)
           (mem (y, y') ys)
  else
    y \notin visited) ->
  P' = (If y \in visited then P else fupdate P y (Some x)) ->
  Inv G P0 P edge source target visited visited_edges stack ((x, y) :: ys) ->
  Inv G P0 P' edge source target
      (visited \u \{y})
      (visited_edges \u \{(x,y)})
      (if drop_edge then stack else y :: stack)
      ys.
Proof.
  introv Hy Hystep HP' HI.
  constructor~; try now apply HI.
  { rew_set. forwards~: Inv_source. }
  { rew_set. intro z. rew_set. forwards~ HH: Inv_target.
    rew_set in HH. intros. specializes HH z __. branches; subst; try tauto.
    apply~ Hy. (* ugh *) }
  { forwards~ HH: Inv_visited. rew_set in HH. rew_set. intro z.
    rew_set. intros. branches. now specializes HH z __. subst.
    applys~ Inv_edge HI x y. applys~ Inv_in_processing_is_edge. }
  { intros x' y' Hx'y'. rew_set in Hx'y'. branches.
    applys~ HI. invp; subst. applys~ Inv_in_processing_is_edge. }
  { intros x' y' Hx'y'. rew_set in Hx'y'. rew_set. branches.
    { forwards~ [? ?]: Inv_visited_edges_visited HI x' y'. }
    { invp; subst. splits~. } }
  { case_if; try apply Forall_cons.
    all: try (applys~ Forall_pred_incl; [applys~ Inv_stack| intros ? ?; set_prove]).
    rew_set. tauto. }
  { case_if~; [| apply~ noduplicates_cons]; apply~ HI. }
  { forwards~ [[? ?] ?]: Forall_cons_inv (Inv_processing HI).
    applys~ Forall_pred_incl. intros [? ?] ?. set_prove. }
  { forwards~: noduplicates_cons_inv (Inv_processing_nodup HI). tauto. }
  { intros z Hz. rew_set in Hz. branches.
    { forwards~: Inv_visited_reachable. applys~ covariant_rtclosure.
      case_if; subst; rel_crush'. }
    { subst z. case_if; subst P'.
      { forwards~: Inv_visited_reachable. applys~ covariant_rtclosure. rel_crush'. }
      { asserts~: (x \in visited). apply rtclosure_r with x.
        { applys~ covariant_rtclosure. 2: now applys~ Inv_visited_reachable. rel_crush'. }
        repeat splits~. now applys~ Inv_in_processing_is_edge. fupdate_case.
        all: rew_set; tauto. } } }
  { intros x' y' Hx' Hx'y'. rew_set in Hx'.
    destruct (or_inv_classic_l Hx') as [?|[? ?]].
    { forwards~: Inv_visited_edge HI x' y'. rew_listx in *. rew_set.
      branchx.
      { case_if; applys~ xor3_left; rew_listx; rew_logic; splits~. congruence. }
      { case_if; applys~ xor3_middle; rew_logic~. }
      { forwards~ [? ?]: noduplicates_cons_inv (Inv_processing_nodup HI).
        branches. invp; subst.
        { case_if; applys~ xor3_left.
          rew_listx. rew_logic. split~. intros ->. auto. }
        { case_if; applys~ xor3_right; rew_listx; rew_logic; splits~.
          all: congruence. } } }
    { subst x'. case_if.
      { specializes Hystep y' __. rew_set. branchx; xor3. }
      { rew_listx. rew_set. applys~ xor3_middle. rew_logic. splits.
      { intros ?. forwards~ [? ?]: Inv_visited_edges_visited HI y y'. }
      { forwards~ [? ?]: noduplicates_cons_inv (Inv_processing_nodup HI).
        tests~: (x = y). congruence. } } } }
  { case_if; subst P'. apply~ HI. fupdate_case; try apply~ HI. false.
    forwards~: Inv_source. subst. auto. }
  { intros z. rew_set. forwards~ [?|?]: Inv_parents_modified HI;[].
    case_if; subst P'. now eauto. fupdate_case. }
Qed.

Lemma rVPswap: forall V (edge: binary vertex) P x y,
  reachable (in_visited V (with_parents P edge)) x y =
  reachable (with_parents P (in_visited V edge)) x y.
Proof.
  intros. apply prop_ext. split; intros HH; applys~ covariant_rtclosure; rel_crush.
Qed.

Lemma Inv_replace_parent :
  forall G P0 P (edge: vertex -> vertex -> Prop) source target visited visited_edges stack processing x y,
  x \in visited ->
  y \in visited ->
  edge x y ->
  ~ reachable (with_parents P edge) y x ->
  Inv G P0 P edge source target visited visited_edges stack processing ->
  Inv G P0 (fupdate P y (Some x)) edge source target visited visited_edges stack processing.
Proof.
  introv Hx Hy Hxy nRxy HI.
  constructor; try solve [apply HI].
  { intros z Hz. rew_set in Hz. rewrite rVPswap.
    apply~ LLL; rewrite <-?rVPswap; try apply~ HI. splits~.
    intro HH. apply nRxy. applys~ covariant_rtclosure. rel_crush. }
  { fupdate_case. 2: now apply HI. subst y.
    false. apply nRxy. applys covariant_rtclosure. 2: apply~ HI. rel_crush. }
  { intros. forwards~ [?|?]: Inv_parents_modified HI. fupdate_case. subst. tauto. }
Qed.

Lemma Inv_visit_edge_aux_drop' :
  forall (G: graph vertex) P0 P (edge: vertex -> vertex -> Prop) source target visited visited_edges stack x y ys,
  x \in visited ->
  edge x y ->
  ~ reachable (with_parents P edge) y x ->
  y \notin target ->
  (y \notin visited -> forall y',
      edge y y' ->
      xor3 ((y, y') \in visited_edges)
           (mem y stack)
           (mem (y, y') ys)) ->
  Inv G P0 P edge source target visited visited_edges stack ((x, y) :: ys) ->
  Inv G P0 (fupdate P y (Some x)) edge source target
      (visited \u \{y})
      (visited_edges \u \{(x,y)})
      stack ys.
Proof.
  introv ? ? ? ? ? HI.
  tests: (y \in visited).
  { apply~ Inv_replace_parent. all: try (rew_set; tauto).
    forwards~: Inv_visit_edge_aux true. case_if~. }
  { forwards~: Inv_visit_edge_aux true HI. case_if~. }
Qed.

Lemma Inv_visit_edge_aux_nodrop :
  forall (G: graph vertex) P0 P (edge: vertex -> vertex -> Prop) source target visited visited_edges stack x y ys,
  y \notin target ->
  y \notin visited ->
  Inv G P0 P edge source target visited visited_edges stack ((x, y) :: ys) ->
  Inv G P0 (fupdate P y (Some x)) edge source target
      (visited \u \{y})
      (visited_edges \u \{(x,y)})
      (y :: stack) ys.
Proof. introv ? ? HI. forwards~: Inv_visit_edge_aux false HI. case_if~. Qed.

Lemma Inv_visit_edge_stop_visited :
  forall G P0 P (edge: vertex -> vertex -> Prop) source target visited visited_edges stack x y ys,
  y \in visited ->
  Inv G P0 P edge source target visited visited_edges stack ((x, y) :: ys) ->
  Inv G P0 P edge source target
      visited
      (visited_edges \u \{(x,y)})
      stack ys.
Proof.
  introv Hy HI.
  forwards~ HIaux: Inv_visit_edge_aux true HI.
  { forwards~ HT: Inv_target HI. rew_set in *. specializes HT y. firstorder. }
  { firstorder. }
  asserts_rewrite (visited \u \{y} = visited) in *.
  { rew_set. intro. rew_set. split; try tauto. intros [?|?]; subst; tauto. }
  simpl in HIaux. case_if~.
Qed.

Lemma Inv_visit_edge_stop_noedge :
  forall G P0 P P' (edge: vertex -> vertex -> Prop) source target visited visited_edges stack x y ys,
  y \notin target ->
  (forall y', ~ edge y y') ->
  P' = (If y \in visited then P else fupdate P y (Some x)) ->
  Inv G P0 P edge source target visited visited_edges stack ((x, y) :: ys) ->
  Inv G P0 P' edge source target
      (visited \u \{y})
      (visited_edges \u \{(x,y)})
      stack ys.
Proof.
  introv HT Hy HP' HI.
  forwards~: Inv_visit_edge_aux true HI.
  intros. false. specializes~ Hy.
Qed.

Lemma Inv_visit_edge_continue :
  forall G P0 P (edge: vertex -> vertex -> Prop) source target visited visited_edges stack x y ys,
  y \notin target ->
  y \notin visited ->
  Inv G P0 P edge source target visited visited_edges stack ((x, y) :: ys) ->
  Inv G P0 (fupdate P y (Some x)) edge source target
      (visited \u \{y})
      (visited_edges \u \{(x,y)})
      (y :: stack) ys.
Proof.
  introv ? ? HI. forwards~: Inv_visit_edge_aux false HI. case_if~.
Qed.

Lemma Inv_init :
  forall G P (edge: vertex -> vertex -> Prop) source target,
  source \notin target ->
  source \in vertices G ->
  (forall x y, edge x y -> x \in vertices G /\ y \in vertices G) ->
  P source = None ->
  Inv G P P edge source target \{source} \{} (source :: nil) nil.
Proof.
  introv HT.
  constructor~; try set_prove.
  { rew_set. intros. rew_set in *. subst. autos~. }
  { rew_listx~. set_prove. }
  { apply~ noduplicates_one. }
  { rew_listx~. }
  { apply~ noduplicates_nil. }
  { intros z Hz. rew_set in Hz. subst. apply rtclosure_refl. }
  { intros x y Hx Hxy. rew_set in Hx. subst x. applys~ xor3_middle. rew_listx~. }
Qed.

Lemma Inv_complete :
  forall G P0 P (edge: vertex -> vertex -> Prop) source target visited visited_edge,
  Inv G P0 P edge source target visited visited_edge nil nil ->
  forall x, reachable edge source x -> x \in visited.
Proof.
  introv HI.
  lets: ltac_mark. induction 1 using rtclosure_ind_r; gen_until_mark.
  - applys~ HI.
  - intros y z ? ? ?. lets Hedge: Inv_visited_edge HI y z.
    rew_listx in Hedge.
    applys~ Inv_visited_edges_visited HI y z.
    specializes Hedge __ __. branchx; [auto | false | false].
Qed.

End Traversal.

Hint Resolve Inv_visited_is_vertex.
Hint Resolve Inv_source.
Hint Resolve Inv_in_stack_visited.
Hint Resolve Inv_in_processing_source_visited.
Hint Resolve Inv_visited_in_processing_notin_stack.
Hint Resolve Inv_visited_in_processing_notin_visited_edges.

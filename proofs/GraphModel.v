From TLC Require Import LibSet LibList LibRelation LibTactics LibLogic.
Require Import TLCBuffer LibTacticsExtra LibRelationExtra.
Require Import ssreflect.

Local Ltac auto_tilde ::= intuition jauto.

(********************************************************************)
(* ** Representation of graphs with oriented, unlabeled edges *)

Class GraphModel (vertex: Type) := MkGraphModel {
  graph : Type;

  graph_empty : graph;
  graph_add_vertex : graph -> vertex -> graph;
  graph_add_edge : graph -> vertex * vertex -> graph;

  vertices : graph -> set vertex;
  edges : graph -> set (vertex * vertex);

  graph_empty_vertices : vertices graph_empty = \{};
  graph_empty_edges : edges graph_empty = \{};

  edges_in_vertices g x y :
    (x, y) \in (edges g) ->
    x \in (vertices g) /\ y \in (vertices g);

  finite_graph g :
    finite (vertices g);
  finite_graph_edges g :
    finite (edges g);

  vertices_add_vertex g x :
    vertices (graph_add_vertex g x) = vertices g \u \{x};
  edges_add_vertex g x :
    edges (graph_add_vertex g x) = edges g;

  vertices_add_edge g x y :
    vertices (graph_add_edge g (x, y)) = vertices g;
  edges_add_edge g x y :
    x \in vertices g ->
    y \in vertices g ->
    edges (graph_add_edge g (x, y)) = edges g \u \{(x, y)}
}.

Arguments graph vertex {GraphModel}.

Section Graph.
Context {vertex} `{GraphModel vertex}.

(** Derived definition for working with graphs *)

Definition out_edges (G : graph vertex) i :=
  set_st (fun j => (i,j) \in edges G).

Definition has_edge (G : graph vertex) x y :=
  (x,y) \in edges G.

(********************************************************************)

Definition acyclic (G : graph vertex) :=
  forall x, tclosure (has_edge G) x x -> False.

(********************************************************************)
(* ** Basic well-formedness facts on graphs *)

Lemma out_edges_has_edge : forall (G : graph vertex) i j,
  j \in out_edges G i <-> has_edge G i j.
Proof using.
  intros. unfold has_edge, out_edges. rewrite~ in_set_st_eq.
Qed.

Lemma has_edge_vertices : forall (G : graph vertex) x y,
  has_edge G x y -> x \in vertices G /\ y \in vertices G.
Proof using.
  =>> M. rewrite <- out_edges_has_edge in M. applys* edges_in_vertices.
Qed.

Lemma has_edge_in_vertices_l : forall (G : graph vertex) x y,
  has_edge G x y -> x \in vertices G. (* trivial *)
Proof using. intros. forwards*: has_edge_vertices. Qed.

Lemma has_edge_in_vertices_r : forall (G : graph vertex) x y,
  has_edge G x y -> y \in vertices G. (* trivial *)
Proof using. intros. forwards*: has_edge_vertices. Qed.

Lemma has_edge_add_edge : forall (G: graph vertex) x y x' y',
  x \in vertices G ->
  y \in vertices G ->
  has_edge (graph_add_edge G (x, y)) x' y' = (has_edge G x' y' \/ (x = x' /\ y = y')).
Proof using.
  introv Hx Hy. unfold has_edge. rewrite~ edges_add_edge.
  rew_logic. rew_set. splits~. invp; substs~. substs~.
Qed.

Lemma has_edge_add_edge_old : forall (G : graph vertex) x y x' y',
  x \in vertices G ->
  y \in vertices G ->
  has_edge G x' y' ->
  has_edge (graph_add_edge G (x, y)) x' y'.
Proof.
  introv Hx Hy HE. unfold has_edge in *. rewrite~ edges_add_edge.
  rew_set. tauto.
Qed.

Lemma has_edge_add_edge_new : forall (G : graph vertex) x y,
  x \in vertices G ->
  y \in vertices G ->
  has_edge (graph_add_edge G (x, y)) x y.
Proof. introv ? ?. rewrite~ has_edge_add_edge. Qed.

Lemma rel_incl_add_edge : forall (G : graph vertex) x y,
  x \in vertices G ->
  y \in vertices G ->
  rel_incl (has_edge G) (has_edge (graph_add_edge G (x, y))).
Proof. unfolds~ rel_incl. applys~ has_edge_add_edge_old. Qed.

Local Ltac auto_tilde ::= eauto with maths.

Lemma add_edge_reachable : forall (G : graph vertex) x y z,
  x \in vertices G ->
  y \in vertices G ->
  rtclosure (has_edge (graph_add_edge G (x, y))) y z ->
  rtclosure (has_edge G) y z.
Proof.
  introv Hx Hy HR. sets_eq y_: y. rewrite {1}EQy_ in HR. revert HR EQy_.
  induction 1 as [| y' x' z Hx'y'new Hx'y'old Hy'z ] using rtclosure_ind_r.
  { intros ->. apply rtclosure_refl. }
  { intros ->. rewrite~ has_edge_add_edge in Hy'z. destruct Hy'z as [?|[? ?]].
    { applys~ rtclosure_r. }
    { subst y' z. apply rtclosure_refl. } }
Qed.

Lemma acyclic_empty : acyclic graph_empty.
Proof.
  intros x Hx. apply tclosure_inv_l' in Hx as [? [He ?]].
  unfold has_edge in He. rewrite graph_empty_edges in He.
  rew_set in He. auto.
Qed.

Lemma acyclic_add_edge : forall (G : graph vertex) x y,
  x \in vertices G ->
  y \in vertices G ->
  acyclic G ->
  ~ rtclosure (has_edge G) y x ->
  acyclic (graph_add_edge G (x, y)).
Proof.
  introv Hx Hy AG Hxy. intros z Hz.
  sets G': (graph_add_edge G (x, y)).
  assert (Ind: forall u, rtclosure (has_edge G) z u /\ tclosure (has_edge G') u z ->
                         rtclosure (has_edge G) y x).
  { intros u (Hu1 & Hu2).
    induction Hu2 as [u' v' Huv' | v' u' w' Huv' ? Hind] using tclosure_ind_l.
    - false. unfold G' in Huv'. rewrite~ has_edge_add_edge in Huv'. destruct Huv' as [?|[<- <-]].
      + applys~ AG u'. applys~ tclosure_of_rtclosure_r v'.
      + tauto.
    - unfold G' in Huv'. rewrite~ has_edge_add_edge in Huv'. destruct Huv' as [?|[<- <-]].
      { applys~ Hind. applys~ rtclosure_r. }
      { applys~ (>> rtclosure_trans w'). applys~ (>>add_edge_reachable x y).
        applys~ rtclosure_of_tclosure. } }
  applys~ Hxy. applys~ Ind z. splits~. applys~ rtclosure_refl.
Qed.

Lemma acyclic_add_vertex : forall (G : graph vertex) x,
  acyclic G ->
  acyclic (graph_add_vertex G x).
Proof.
  introv AG. intros z Hz.
  forwards [Hz_loop | [y [Hz1 Hz2]]] : tclosure_inv_l Hz; clear Hz.
  { rewrite /has_edge edges_add_vertex in Hz_loop. apply (AG z).
    apply~ tclosure_once. }
  { rewrite /has_edge edges_add_vertex in Hz1, Hz2. apply (AG z).
    apply tclosure_l with y; autos~. }
Qed.

Lemma card_edges_add_edge : forall (G : graph vertex) x y,
  x \in vertices G ->
  y \in vertices G ->
  ~ has_edge G x y ->
  card (edges (graph_add_edge G (x, y))) = card (edges G) + 1.
Proof.
  introv ? ? Hxy. rewrite edges_add_edge // card_disjoint_union ?card_single //.
  now apply finite_graph_edges.
  now apply finite_single.
  rew_set. intros [? ?]. rew_set. unfold has_edge in Hxy. congruence.
Qed.

Lemma card_vertices_add_vertex : forall (G : graph vertex) x,
  card (vertices (graph_add_vertex G x)) <= card (vertices G) + 1.
Proof.
  introv. rewrite vertices_add_vertex. etransitivity.
  apply card_union_le. now apply finite_graph. now apply finite_single.
  by rewrite card_single.
Qed.

Lemma card_out_edges_as_list : forall G x,
  card (out_edges G x) =
  length (map snd (filter (fun '(u, v) => u = x) (to_list (edges G)))).
Proof.
  intros. apply list_repr_inv_card. split.
  { assert (HH: noduplicates (to_list (edges G))).
    { eapply noduplicates_of_list_repr.
      apply list_repr_to_list_of_finite. apply finite_graph_edges. }
    revert HH. generalize (to_list (edges G)).
    induction 1 as [|[u v] l' HM Hl HI] using noduplicates_ind .
    - rewrite filter_nil map_nil. apply noduplicates_nil.
    - rewrite filter_cons. case_if.
      + subst u. rewrite map_cons. simpl. apply~ noduplicates_cons.
        intros HM'. forwards~ [uv' [H1 H2]]: map_mem. destruct uv' as [u' v'].
        simpl in H2. subst v'. forwards~ [? ?]: mem_filter_inv. simpl in *. subst~.
      + auto. }
  { intro z. split; intro HH.
    { unfold out_edges. rew_set. forwards~ [[u' v'] [H2 ?]]: map_mem HH.
      simpl in *. subst v'. forwards~ [? ->]: mem_filter_inv H2.
      rewrite~ finite_in_iff_mem_to_list. apply finite_graph_edges. }
    { unfold out_edges in HH. rew_set in HH.
      change z with (snd (x, z)). apply mem_map. apply~ mem_filter.
      rewrite~ <-finite_in_iff_mem_to_list. apply finite_graph_edges. } }
Qed.

End Graph.

Notation "G \++ x" := (@graph_add_vertex _ _ G x)
  (at level 50, left associativity) : graph_scope.
Notation "G \+> e" := (@graph_add_edge _ _ G e)
  (at level 50, left associativity) : graph_scope.


(******************************************************************************)
(* Hints *)

Create HintDb graph.

Hint Resolve
     has_edge_in_vertices_l has_edge_in_vertices_r
     has_edge_add_edge_old has_edge_add_edge_new
     rel_incl_add_edge
  : graph.

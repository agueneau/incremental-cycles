Set Implicit Arguments.
(* Load the CFML library. *)
Require Import ssreflect.
Require Import CFML.CFLib.
Require Import TLC.LibIntTactics TLC.LibFun.

Require Import GraphModel RawGraph.
Require Import GraphFunctionalInvariants GraphCostInvariants.
Require Import ForwardSearch_proof.

(******************************************************************************)
(* The representation predicate for the whole graph with invariants. *)

Section Graph.

Context `{GM: GraphModel vertex}.

Definition IsGraph g G : hprop :=
  Hexists L M Ms I P,
  IsRawGraph g G L M Ms I P
  \* \$ ϕ G L
  \* \[ Inv G L I P ].

Lemma IsGraph_open : forall g G,
  IsGraph g G ==>
  Hexists L M Ms I P, IsRawGraph g G L M Ms I P \* \$ ϕ G L \* \[ Inv G L I P ].
Proof. intros. xunfolds~ IsGraph. Qed.

Lemma IsGraph_close : forall g G L M Ms I P,
  Inv G L I P ->
  IsRawGraph g G L M Ms I P \* \$ ϕ G L ==> IsGraph g G.
Proof. intros. xunfolds~ IsGraph. Qed.

(* [Graph] is affine: it can be GCd (since the underlying graph is, and the
   amount of credits stored in Graph is nonnegative) *)

Lemma affine_prop_r : forall H (P: Prop),
  (P -> affine H) ->
  affine (H \* \[ P ]).
Proof.
  introv A. unfold affine in *. intros h Hh.
  rewrite star_comm in Hh. apply heap_star_prop_elim in Hh as (?&?). auto.
Qed.

Hint Resolve IsRawGraph_affine : affine.

Lemma IsGraph_affine : forall g G,
  affine (IsGraph g G).
Proof.
  intros. unfold IsGraph.
  affine. intros L. affine. intros M. affine. intros I. affine. intros Ms.
  affine. intros P. apply affine_star; [affine|].
  apply affine_prop_r. intros ?. affine. rewrite le_zarith. apply~ ϕ_nonneg.
Qed.

(* As an immediate corrolary, IsGraph is GC-able *)
Lemma IsGraph_GC : forall g G,
  IsGraph g G ==> \GC.
Proof. intros. apply hsimpl_gc, IsGraph_affine. Qed.

Lemma IsGraph_init : forall g M Ms,
  IsRawGraph g graph_empty (const 1) M Ms (const \{}) (const None) ==>
  IsGraph g graph_empty.
Proof.
  intros. hchange IsGraph_close.
  - apply Inv_init.
  - rewrite /ϕ /const net_graph_empty Z.mul_0_r credits_zero_eq. hsimpl.
  - hsimpl.
Qed.

Lemma IsGraph_acyclic : forall g G,
  IsGraph g G ==>
  IsGraph g G \* \[ forall x, ~ tclosure (has_edge G) x x ].
Proof. intros. xunfold IsGraph. hsimpl~. intro. applys~ Inv_acyclic. Qed.

Lemma init_graph_IsGraph_spec : exists k,
  app Raw_graph_ml.init_graph [tt]
    PRE (\$ k)
    POST (fun g => IsGraph g graph_empty).
Proof.
  exists (CFMLBigO.cost init_graph_spec tt). xapp. intro.
  hchange~ IsGraph_init. hsimpl~.
Qed.

End Graph.

Hint Resolve IsGraph_affine : affine.

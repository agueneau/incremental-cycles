Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the CF definitions. *)
Require Import Raw_graph_ml.
Require Import BigO.CFMLBigO.

From TLC Require Import LibFun.
Require Import TLCBuffer.
Require Import GraphModel.

Open Scope graph_scope.

Notation graph_ml := Raw_graph_ml.graph_.
Notation vertex := Raw_graph_ml.vertex_.
Notation mark := Raw_graph_ml.mark_.

Section RawGraph.

Context `{GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types Ms : set mark. (* set of valid marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)
Implicit Types P : vertex -> option vertex.

Local Hint Resolve finite_graph.

(******************************************************************************)

Definition IsNewVertex (v : vertex) :=
  v ~> `{ outgoing' := @nil vertex;
          incoming' := @nil vertex;
          mark' := -1;
          level' := 1;
          parent' := v }.

Definition Vertices (G : graph vertex) (L : vertex -> int) (M : vertex -> mark)
                    (I : vertex -> set vertex) (P : vertex -> option vertex)
                    (vs : set vertex) :=
  fold sep_monoid (fun v =>
    Hexists outl inl parent,
      \[to_set outl = out_edges G v /\ noduplicates outl /\
        to_set inl = I v /\ noduplicates inl] \*
       v ~> `{ outgoing' := outl; incoming' := inl;
               mark' := M v; level' := L v;
               parent' := unsome_default parent (P v) }) vs.

Lemma Vertices_isolate: forall G L M I P v,
  v \in vertices G ->
  Vertices G L M I P (vertices G) =
  Vertices G L M I P (vertices G \-- v) \*
  Hexists outl inl parent,
    \[to_set outl = out_edges G v /\ noduplicates outl /\
      to_set inl = I v /\ noduplicates inl] \*
     v ~> `{ outgoing' := outl; incoming' := inl;
             mark' := M v; level' := L v;
             parent' := unsome_default parent (P v) }.
Proof.
  introv Hv. unfold Vertices.
  rewrites~ (>>fold_isolate v Comm_monoid_sep). rewrite~ star_comm.
Qed.

Definition IsRawGraph (g : graph_ml) (G : graph vertex)
           (L : vertex -> int) (M : vertex -> mark) (Ms : set mark)
           (I : vertex -> set vertex) (P : vertex -> option vertex) :=
  Hexists next_mark,
   \[(forall_ m \in Ms, m < next_mark) /\
     (forall v, M v \in Ms) /\
     (forall v, v \notin vertices G -> L v = 1) /\
     (forall v, v \notin vertices G -> M v = -1) /\
     (forall v, v \notin vertices G -> I v = \{}) /\
     (forall v, v \notin vertices G -> P v = None) ] \*
   g ~> `{ next_mark' := next_mark } \*
   Vertices G L M I P (vertices G).

Lemma IsRawGraph_affine : forall g G L M Ms I P,
  affine (IsRawGraph g G L M Ms I P).
Proof.
  introv. unfold IsRawGraph.
  affine=>m. affine.
  { apply affine_record_repr. }
  { applys~ (>> fold_induction Comm_monoid_sep).
    { simpl. affine. }
    { simpl. affine=>outl. affine=>inl. affine=>parent. affine. apply affine_record_repr. } }
Qed.

Lemma IsRawGraph_forget_parent : forall g G L M Ms I P x,
  x \in vertices G ->
  IsRawGraph g G L M Ms I P ==> IsRawGraph g G L M Ms I (fupdate P x None).
Proof.
  intros. unfold IsRawGraph. hpull. intros m ?. unpack. hsimpl~. 2: fupdate_case.
  do 2 rewrites~ (>>Vertices_isolate x). hpull;=> inl outl par_def ?. unpack.
  hsimpl (P x). now fupdate_case.
  { unfold Vertices. rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
    { eauto with finite. }
    { intros v Hv. simpl. rew_set in Hv. fupdate_case. } }
  autos.
Qed.

Lemma new_mark_spec :
  spec1 [cost]
    (forall g G L M Ms I P,
     app new_mark [g]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (m:mark) => IsRawGraph g G L M (Ms \u \{m}) I P \*
                                       \[ forall n, M n <> m ])).
Proof.
  xspecO_refine straight_line. introv. xcf. xpay.
  unfold IsRawGraph. xpull=>m [Hm [HMs [HL [HM HI]]]]. xapps. xapp. xret.
  hsimpl.
  { intros v <-. forwards~ : Hm (M v). math. }
  { splits*.
    { intros m' Hm'. rew_set in Hm'. destruct Hm' as [?| ->]; [|math].
      forwards~ : Hm m'. math. }
    { intros v. rew_set. auto. } }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma vertex_eq_spec :
  spec1 [cost]
    (forall (x y: vertex),
     app vertex_eq [x y]
       PRE (\$ cost tt)
       POST \[= isTrue (x = y)]).
Proof.
  xspecO_refine straight_line. introv. xcf. xpay. xapp~.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma is_marked_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex) (m: mark),
     x \in vertices G ->
     app is_marked [g x m]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (b:bool) => IsRawGraph g G L M Ms I P \*
                                  \[if b then M x = m else M x <> m])).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  rewrites~ (>>Vertices_isolate x). xpull=>m' [Hm' ?] ????. xapps. xret.
  hsimpl~. case_if~.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma set_mark_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex) (m: mark),
     x \in vertices G ->
     m \in Ms ->
     app set_mark [g x m]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (_:unit) => IsRawGraph g G L (fupdate M x m) Ms I P)).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  do 2 rewrites~ (>>Vertices_isolate x). xpull=>m' Hm' ????. xapp. hsimpl~.
  { fupdate_case. }
  { unfold Vertices. rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
    { eauto with finite. }
    { intros v Hv. simpl. rew_set in Hv. fupdate_case. } }
  { fupdate_case. }
  { fupdate_case. }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma get_level_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex),
     x \in vertices G ->
     app get_level [g x]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun l => IsRawGraph g G L M Ms I P \* \[l = L x])).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  rewrites~ (>>Vertices_isolate x). xpull=>m Hm ????. xapp. hsimpl~.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma set_level_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex) (l: int),
     x \in vertices G ->
     app set_level [g x l]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (_:unit) => IsRawGraph g G (fupdate L x l) M Ms I P)).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  do 2 rewrites~ (>>Vertices_isolate x). xpull=>m' Hm' ????. xapp. hsimpl~.
  { fupdate_case. }
  { unfold Vertices. rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
    { eauto with finite. }
    { intros v Hv. simpl. rew_set in Hv. fupdate_case. } }
  { fupdate_case. }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma get_incoming_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex),
     x \in vertices G ->
     app Raw_graph_ml.get_incoming [g x]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (xs: list vertex) => IsRawGraph g G L M Ms I P \*
         \[ noduplicates xs /\ to_set xs = I x ])).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  rewrites~ (>>Vertices_isolate x). xpull.
  intros m Hm ? inl ? (? & ? & ? & ?). xapp. hsimpls~.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma clear_incoming_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex),
     x \in vertices G ->
     app clear_incoming [g x]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (# IsRawGraph g G L M Ms (fupdate I x \{}) P)).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  do 2 rewrites~ (>>Vertices_isolate x). xpull.
  intros m Hm ? inl ? ?. xapp. hsimpls~.
  { unfold Vertices. rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
    { eauto with finite. }
    { intros v Hv. simpl. rew_set in Hv. fupdate_case. } }
  { fupdate_case. rew_set=>v. rewrite <-mem_to_set, mem_nil_eq. tauto. }
  { apply noduplicates_nil. }
  { fupdate_case. }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma add_incoming_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x y: vertex),
     x \in vertices G ->
     y \in vertices G ->
     y \notin I x ->
     app add_incoming [g x y]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (# IsRawGraph g G L M Ms (fupdate_add I x y) P)).
Proof.
  xspecO_refine straight_line. introv Hx Hy Hxy. xcf. xpay. unfold IsRawGraph.
  do 2 rewrites~ (>>Vertices_isolate x). xpull. intros m Hm ? inl ? (? & ? & Hinl & ?).
  xapps. xapp. hsimpls~.
  { unfold Vertices. rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
    { eauto with finite. }
    { intros v Hv. rew_set in Hv. unfold fupdate_add. fupdate_case. } }
  { unfold fupdate_add, to_set. fupdate_case. rew_set=>v. rew_set.
    rewrite mem_cons_eq, <-Hinl, mem_to_set. tauto. }
  { apply~ noduplicates_cons. apply Hxy. now rewrite <- Hinl, <- mem_to_set. }
  { unfold fupdate_add. fupdate_case. }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma get_outgoing_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x: vertex),
     x \in vertices G ->
     app Raw_graph_ml.get_outgoing [g x]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (xs: list vertex) => IsRawGraph g G L M Ms I P \*
          \[ noduplicates xs /\ to_set xs = out_edges G x ])).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  rewrites~ (>>Vertices_isolate x). xpull.
  intros m Hm outl ? ? (? & ? & ? & ?). xapps. hsimpls~.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma set_parent_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x p: vertex),
      x \in vertices G ->
      app set_parent [g x p]
        PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
        POST (fun (tt:unit) => IsRawGraph g G L M Ms I (fupdate P x (Some p)))).
Proof.
  xspecO_refine straight_line. introv Hx. xcf. xpay. unfold IsRawGraph.
  xpull. intros nm (? & ? & ? & ? & ? & ?).
  do 2 rewrites~ (>>Vertices_isolate x). xpull. intros outl inl p_def (? & ? & ? & ?).
  xapps. hsimpls~ p_def. now fupdate_case.
  { unfold Vertices. rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
    { eauto with finite. }
    { intros v Hv. rew_set in Hv. apply fun_ext_1. intro. fupdate_case. } }
  { fupdate_case. }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma get_parent_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x p: vertex),
      x \in vertices G ->
      P x = Some p ->
      app get_parent [g x]
        PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
        POST (fun r => IsRawGraph g G L M Ms I P \* \[ r = p ])).
Proof.
  xspecO_refine straight_line. introv Hx HPx. xcf. xpay. unfold IsRawGraph.
  xpull. intros nm (? & ? & ? & ? & ? & ?).
  rewrites~ (>>Vertices_isolate x). xpull. intros outl inl p_def (? & ? & ? & ?).
  xapps. hsimpls~ p_def. rewrite HPx. reflexivity.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma raw_add_edge_spec :
  spec1 [cost]
    (forall g G L M Ms I P (x y: vertex),
     x \in vertices G ->
     y \in vertices G ->
     (x, y) \notin edges G ->
     app raw_add_edge [g x y]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P)
       POST (fun (_:unit) =>
         IsRawGraph g (G \+> (x, y)) L M Ms I P)).
Proof.
  xspecO_refine straight_line. introv Hx Hy Hxy. xcf. xpay. unfold IsRawGraph.
  rewrites~ (>>Vertices_isolate x). xpull.
  intros m (Hm & HMs & HL & HM & HI & HP) outl inl p_def (Houtl & ? & ? & ?). xapps. xapp.
  assert (Vertices G L M I P (vertices G \-- x) =
          Vertices (G \+> (x, y)) L M I P (vertices G \-- x)) as ->.
  { unfold Vertices. applys (>>fold_congruence Comm_monoid_sep).
    { auto with finite. }
    { intros v Hv.
      assert (out_edges G v = out_edges (G \+> (x, y)) v) as ->; [|reflexivity].
      unfold out_edges. rew_set in Hv. rew_set=>w. rewrite* edges_add_edge. rew_set.
      intuition congruence. } }
  rewrites <- (>>vertices_add_edge G x y).
  xchange~ <- (>>Vertices_isolate (G \+> (x, y)) x).
  { rewrite~ vertices_add_edge. }
  { splits~.
    { rew_set=>v. unfold to_set, out_edges. rew_set.
      rewrite* edges_add_edge. rewrite mem_cons_eq, mem_to_set, Houtl.
      unfold out_edges. rew_set. intuition congruence. }
    { apply* noduplicates_cons. rewrite mem_to_set, Houtl.
      unfold out_edges. rew_set. tauto. } }
  clear dependent outl. clear dependent inl. hsimpl.
  rewrite vertices_add_edge. splits~.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma raw_add_vertex_spec :
  spec1 [cost]
    (forall g G L M Ms I P x,
     app raw_add_vertex [g x]
       PRE (\$ cost tt \* IsRawGraph g G L M Ms I P \* IsNewVertex x)
       POST (fun (_:unit) => IsRawGraph g (G \++ x) L M Ms I P)).
Proof.
  xspecO_refine straight_line. introv. xcf. xpay. xret.
  unfold IsRawGraph. hpull=>m [Hm [HMs [HL [HM [HI HP]]]]].
  assert (forall v, v \notin vertices (G \++ x) -> v \notin vertices G).
  { intros v Hv Hv'. apply Hv. rewrite vertices_add_vertex. rew_set. auto. }
  hsimpl~. rewrite vertices_add_vertex. tests: (x \in vertices G).
  { assert (vertices G \u '{x} = vertices G) as ->.
    { rew_set=>v. rew_set. intuition now subst. }
    { unfold Vertices.
      rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
      { auto with finite. }
      { intros v Hv. simpl.
        assert (out_edges (G \++ x) v = out_edges G v) as ->; [|trivial].
        unfold out_edges. rew_set=>w. rew_set. rewrite~ edges_add_vertex. }
      { unfold IsNewVertex. apply affine_record_repr. } } }
  unfold Vertices, IsNewVertex.
  rewrites~ (>>fold_union Comm_monoid_sep).
  { auto with finite. }
  { rew_set=>v. rew_set. intros ? ->. tauto. }
  { rewrites~ (>>fold_single Monoid_sep). simpl. hsimpl x.
    { rewrite HL, HM, HP; auto. }
    { rewrites (>>fold_congruence Comm_monoid_sep); [..|hsimpl].
      { auto with finite. }
      { intros v Hv. simpl.
        assert (out_edges (G \++ x) v = out_edges G v) as ->; [|trivial].
        unfold out_edges. rew_set=>w. rew_set. rewrite~ edges_add_vertex. } }
    { splits~.
      { rew_set=>v. unfold to_set, out_edges. rew_set.
        rewrite edges_add_vertex, mem_nil_eq. split~.
        forwards~ : edges_in_vertices. }
      { apply noduplicates_nil. }
      { rew_set=>v. unfold to_set, out_edges. rew_set.
        rewrite* HI. rewrite mem_nil_eq. rew_set. tauto. }
      { apply noduplicates_nil. } } }
  cleanup_cost. monotonic. dominated.
Qed.

Lemma init_graph_spec :
  spec1 [cost]
    (app init_graph [tt]
       PRE (\$ cost tt)
       POST (fun (g:graph_ml) =>
               IsRawGraph g graph_empty (const 1) (const (-1)) \{-1} (const \{}) (const None))).
Proof.
  xspecO_refine straight_line. xcf. xpay.
  unfold IsRawGraph, Vertices. rewrite graph_empty_vertices, fold_empty.
  xapps. hsimpl. piggybank: *rhs.
  splits~; rew_set in *; auto; []. subst. math.
  cleanup_cost. monotonic. dominated.
Qed.

Lemma new_vertex_spec :
  spec1 [cost]
    (app new_vertex [tt]
       PRE (\$ cost tt)
       POST (fun (v:vertex) => IsNewVertex v)).
Proof.
  xspecO_refine straight_line. xcf. xpay. unfold IsNewVertex.
  xapps. xrets. piggybank: *rhs.
  cleanup_cost. monotonic. dominated.
Qed.

End RawGraph.

Hint Extern 1 (RegisterSpec new_mark) => Provide (provide_specO new_mark_spec).
Hint Extern 1 (RegisterSpec vertex_eq) => Provide (provide_specO vertex_eq_spec).
Hint Extern 1 (RegisterSpec is_marked) => Provide (provide_specO is_marked_spec).
Hint Extern 1 (RegisterSpec set_mark) => Provide (provide_specO set_mark_spec).
Hint Extern 1 (RegisterSpec get_level) => Provide (provide_specO get_level_spec).
Hint Extern 1 (RegisterSpec set_level) => Provide (provide_specO set_level_spec).
Hint Extern 1 (RegisterSpec get_incoming) => Provide (provide_specO get_incoming_spec).
Hint Extern 1 (RegisterSpec clear_incoming) => Provide (provide_specO clear_incoming_spec).
Hint Extern 1 (RegisterSpec add_incoming) => Provide (provide_specO add_incoming_spec).
Hint Extern 1 (RegisterSpec get_outgoing) => Provide (provide_specO get_outgoing_spec).
Hint Extern 1 (RegisterSpec set_parent) => Provide (provide_specO set_parent_spec).
Hint Extern 1 (RegisterSpec get_parent) => Provide (provide_specO get_parent_spec).
Hint Extern 1 (RegisterSpec raw_add_edge) =>
  Provide (provide_specO raw_add_edge_spec).
Hint Extern 1 (RegisterSpec raw_add_vertex) =>
  Provide (provide_specO raw_add_vertex_spec).
Hint Extern 1 (RegisterSpec init_graph) => Provide (provide_specO init_graph_spec).
Hint Extern 1 (RegisterSpec new_vertex) => Provide (provide_specO new_vertex_spec).

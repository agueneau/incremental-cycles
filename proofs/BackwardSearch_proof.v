Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the CF definitions. *)
Require Import Simple_sparse_ml.

Require Import BigO.CFMLBigO.
Require Import ssreflect.
Require Import PolTac.PolTac.
Require Import BigO.elia.

From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation.

Require Import TLCBuffer LibRelationExtra LibTacticsExtra.
Require Import GraphModel RawGraph Paths.
Require Import GraphFunctionalInvariants GraphCostInvariants.
Require Import IFold_proof.
Require Traversal.

Open Scope set_scope.
Open Scope graph_scope.

Section BackwardSearch.

Context `{GM: GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types Ms : set mark. (* valid marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)
Implicit Types P : vertex -> option vertex.

Local Ltac auto_tilde ::= eauto with graph maths.
Local Ltac done ::= solve [autos~].

(******************************************************************************)

Definition visited M m : set vertex :=
  \set{ x | M x = m }.

Record invb
       G L M P0 P (m: mark) (source target: vertex)
       (visited_edge: set (vertex * vertex))
       (stack: list vertex) (processing: list (vertex * vertex))
  :=
{
  invb_trav:
    Traversal.Inv
      G P0 P (incoming_level G L) source \{target}
      (visited M m) visited_edge
      stack processing;

  invb_visited_edge_finite:
    finite visited_edge;
}.

Hint Resolve invb_visited_edge_finite.

(******************************************************************************)
(* Hints *)

Hint Resolve invb_trav.

Lemma visited_prove : forall M m x,
  x \in visited M m ->
  M x = m.
Proof. introv H. unfold visited in H. set_prove. Qed.

Hint Resolve visited_prove.

Lemma invb_target_notmarked : forall G L M P0 P m source target visited_edge stack processing,
  invb G L M P0 P m source target visited_edge stack processing ->
  M target <> m.
Proof.
  introv HI. forwards~ HT: Traversal.Inv_target HI.
  rew_set in HT. applys~ HT. set_prove.
Qed.

Hint Resolve invb_target_notmarked.

(******************************************************************************)

Lemma invb_stack_pop : forall G L M P0 P m source target visited_edge x ys stack,
  to_set ys = incoming_level G L x ->
  noduplicates ys ->
  invb G L M P0 P m source target visited_edge (x :: stack) nil ->
  invb G L M P0 P m source target visited_edge stack (mkedges x ys).
Proof.
  introv Hys NDys HI. constructor; [| apply HI ..]. apply~ Traversal.Inv_stack_pop.
Qed.

Lemma invb_skip_marked : forall G L M P0 P m source target visited_edge x y ys stack,
  M y = m ->
  invb G L M P0 P m source target visited_edge stack ((x, y) :: ys) ->
  invb G L M P0 P m source target (visited_edge \u \{(x, y)}) stack ys.
Proof.
  introv Hy HI.
  constructor; try apply HI.
  { apply~ Traversal.Inv_visit_edge_stop_visited. }
  { apply finite_union; [ apply HI | apply finite_single ]. }
Qed.

Lemma invb_mark : forall G L M P0 P m source target visited_edge x y ys stack,
  incoming_level G L x y ->
  M y <> m ->
  y <> target ->
  invb G L M P0 P m source target visited_edge stack ((x, y) :: ys) ->
  invb G L (fupdate M y m) P0 (fupdate P y (Some x)) m source target (visited_edge \u \{(x, y)}) (y :: stack) ys.
Proof.
  introv Hedge Hy Hytarget HI.
  constructor.
  { equates 4. applys~ Traversal.Inv_visit_edge_continue (visited M m).
    unfold visited. rew_set. intro z. rew_set. fupdate_case; subst; tauto. }
  { apply finite_union; [ apply HI | apply finite_single ]. }
Qed.

Lemma invb_parents: forall G L M P0 P m source target visited_edge stack processing,
  parents_in_graph G P0 ->
  invb G L M P0 P m source target visited_edge stack processing ->
  parents_in_graph G P.
Proof.
  introv HP Hinv. unfolds. intros x y Hy Hyx.
  forwards [?|?]: Traversal.Inv_parents_modified Hinv y.
  { applys~ HP. congruence. }
  { forwards~ [? ?]: Traversal.Inv_parent_edge Hinv. }
Qed.

Lemma invb_init : forall G L M P m source target,
  source \in vertices G ->
  target \in vertices G ->
  source <> target ->
  (forall x, M x = m <-> x = source) ->
  P source = None ->
  parents_in_graph G P ->
  invb G L M P P m source target \{} (source :: nil) nil.
Proof.
  introv Hsource Htarget Hrt HM HP HP'. constructors~.
  { equates 4. applys~ Traversal.Inv_init.
    { intros. splits~. }
    { unfold visited. rew_set. tauto. } }
  { apply finite_empty. }
Qed.

Lemma invb_complete : forall G L M P0 P m source target visited_edge,
  invb G L M P0 P m source target visited_edge nil nil ->
  forall x, reachable (incoming_level G L) source x -> M x = m.
Proof.
  introv HI. applys~ Traversal.Inv_complete HI.
Qed.

(******************************************************************************)

Definition swap : vertex*vertex -> vertex*vertex :=
  fun '(x, y) => (y, x).

Lemma swap_inj : LibOperation.injective swap.
Proof. intros [u v] [u' v'] ?. simpl in *. congruence. Qed.

Definition edges_rev (S: set (vertex * vertex)) :=
  image swap S.

Lemma edges_rev_card : forall S,
  finite S ->
  card (edges_rev S) = card S.
Proof. introv FS. applys~ injective_image_card. apply swap_inj. Qed.

Lemma edges_rev_eq : forall S x y,
  (x, y) \in edges_rev S = (y, x) \in S.
Proof.
  intros. unfold edges_rev, image, swap. rew_set.
  rew_logic. splits~. intros [[? ?] [? ?]].  congruence.
Qed.

Lemma invb_visited_edge_at_level : forall G L M P0 P m source target visited_edge stack processing,
  invb G L M P0 P m source target visited_edge stack processing ->
  (edges_rev visited_edge) \c coacc_edges_at_level G L (L source) source.
Proof.
  introv HI. rew_set. intros [x y].
  unfold coacc_edges_at_level, edges_at_level, coacc_edges. rew_set. intro Hxy.
  rewrite edges_rev_eq in Hxy.
  forwards~: Traversal.Inv_visited_edges_visited HI y x. unpack.
  forwards~: exploit_reachable_incoming_level (Traversal.Inv_visited_reachable' (invb_trav HI) x).
  forwards~: exploit_reachable_incoming_level (Traversal.Inv_visited_reachable' (invb_trav HI) y).
  forwards~ ?: Traversal.Inv_visited_edges_edge HI y x.
  splits~. tauto.
Qed.

Lemma Inv_invb_update_parent : forall G L M I P0 P m source target visited_edge stack processing,
  Inv G L I P0 ->
  invb G L M P0 P m source target visited_edge stack processing ->
  Inv G L I P.
Proof.
  introv HI Hi. constructor; try now apply HI. applys~ invb_parents. apply HI.
Qed.

Hint Resolve Inv_invb_update_parent.

(******************************************************************************)

Class visit_backward_spec_ty := {
  vb_a : Z; vb_b : Z; vb_c : Z;
  vb_a_nonneg : 0 <= vb_a;
  vb_b_nonneg : 0 <= vb_b;
  visit_backward_spec :
   forall g G L M Ms I P0 P,
   forall (source target: vertex) (m: mark) (fuel: int) visited_edge (stack: list vertex),
   let cost := (fun '(x, y) => vb_a * x + vb_b * y + vb_c) in
     m \in Ms ->
     target \in vertices G -> (* FIXME? *)
     0 <= fuel ->
     Inv G L I P0 ->
     invb G L M P0 P m source target visited_edge stack nil ->
     app visit_backward [g target m fuel stack]
       PRE (\$ cost (fuel, fuel + length stack) \* IsRawGraph g G L M Ms I P)
       POST (fun (res: visit_backward_result_) =>
         Hexists M' P', IsRawGraph g G L M' Ms I P' \*
         \[ Inv G L I P' /\
            (forall x, M' x = m ->
               reachable (in_visited (visited M' m) (with_parents P' (incoming_level G L))) source x) /\
            M' target <> m /\
            M' source = m /\
            match res with
            | VisitBackwardCyclic =>
              reachable (with_parents P' (incoming_level G L)) source target
            | VisitBackwardCompleted =>
              forall x, reachable (incoming_level G L) source x -> M' x = m
            | VisitBackwardInterrupted =>
              card visited_edge + fuel <= card (coacc_edges_at_level G L (L source) source)
            end
         ]
       )
}.

Instance visit_backward_spec_raw : visit_backward_spec_ty.
Proof.
  begin defer assuming step_cost stop_cost in _g.
  let t := type of step_cost in unify t int.
  let t := type of stop_cost in unify t int.
  begin defer assuming a b c in gcost.
  defer Ha: (0 <= a). defer Hb: (0 <= b).
  constructor 1 with a b c; auto.
  introv. sets_eq measure: (fuel + length stack). gen M P visited_edge fuel stack.
  induction_wf IH: (downto (-1)) measure. hide IH.
  introv -> Hm Htarget Hfuel HInv Hinvb.
  xcf. weaken. xpay. xvals. xmatch_no_intros.
  { (* VisitBackwardCompleted *)
    intros ->. xrets. splits~; try apply Hinvb. applys~ invb_complete. }

  intros x stack' ->.
  xcleanpat.
  xapp~ as ys. intros [incoming_nodup Hincoming].
  rewrites~ (>> Inv_incoming) in Hincoming.
  xfun as ifoldf. hide Sifoldf.

  pose cred_inv fuel := (fuel * step_cost + stop_cost).

  (* Loop invariant for ifold *)
  xlet as ifold_res.
  xapp_spec (spec interruptible_fold_remaining_spec)
    (fun (ys': list vertex) last_step =>
      Hexists M' P' visited_edge', IsRawGraph g G L M' Ms I P' \*
      match last_step with
      | Break timeout => \[ exists processing stack'',
        invb G L M' P0 P' m source target visited_edge' stack'' processing /\
        if (timeout: bool) then
          nat_to_Z(card visited_edge') = card visited_edge + fuel
        else
          M' x = m /\ mem target ys ]
      | Continue (stack'', fuel') =>
        \$ cred_inv fuel' \* \[
        invb G L M' P0 P' m source target visited_edge' stack'' (mkedges x ys') /\
        0 <= fuel' <= fuel /\
        fuel' + length stack'' <= fuel + length stack' /\
        card visited_edge' + fuel' = card visited_edge + fuel ]
      end
    ).

  (* body of the loop *)
  { intros y ys' [stack'' fuel'] Hys.
    xpull. intros M' P' visited_edge'.
    intros (Hinvb' & Hfuel'le & Hfuel' & Hmeasure).

    weaken. xapp. xpay. xmatch. xrets~. xif_guard.
    { subst fuel'. xbreak~. }
    { assert (0 < fuel') by math. clear C.
      xapps~. intros. xif.
      { (* M y = m *)
        xcontinue~.
        rewrite mkedges_cons in Hinvb'.
        split. applys~ invb_skip_marked x. splits~;[].
        rewrite <-Hmeasure. rewrite~ card_disjoint_union_single. }
      xapps~. xif.
      { (* y = target *)
        subst y. xbreak~. do 2 eexists. splits~. }
      { (* Mark y and add it to the stack *)
        xapps~. xapps~. xcontinue~.
        rewrite mkedges_cons in Hinvb'.
        split. applys~ invb_mark. splits~.
        { rew_listx. math. }
        { rewrite <-Hmeasure. rewrite~ card_disjoint_union_single. } } }

    { case_if.
      { subst fuel'. simp. defer in _g. }
      { rewrite (_:cred_inv fuel' = cred_inv ((fuel' - 1) + 1)). now rewrite Z.sub_add.
        unfold cred_inv. cancel (fuel' - 1).
        case_max; auto; defer in _g. math. repeat case_max; auto; defer in _g. } } }

  (* entering the loop *)
  { split. now applys~ invb_stack_pop. splits~. }

  (* the rest of the program *)
  xpull. intros ys' M' P' visited_edge' Hres.
  xmatch_no_intros. intros timeout ->.
  xpull. intros [p [stack'' (Hinvb' & Hfuel')]]. xif.
  { (* VisitBackwardInterrupted *)
    xrets. splits~; try now apply Hinvb'.
    rewrite <-Hfuel'. rewrites~ <-(>> edges_rev_card visited_edge').
    rewrite <-to_nat_le_nat_le, !to_nat_nat by math.
    applys~ card_le_of_incl. applys~ invb_visited_edge_at_level.
  }
  { (* VisitBackwardCyclic *)
    xapps~. xrets. unpack.
    (* Updating target's parent does not invalidate the reachable invariant *)
    assert (HR: forall z, M' z = m ->
      reachable (in_visited (visited M' m) (with_parents (fupdate P' target (Some x)) (incoming_level G L)))
                source z).
    { intros. applys~ covariant_rtclosure; cycle 1. applys~ Traversal.Inv_visited_reachable.
      forwards~: invb_target_notmarked.
      autounfold with rel_crush. intros. fupdate_case. false. subst y. unfold visited in *.
      rew_set in *. tauto. }
    splits~; try solve [apply Hinvb'']. applys~ Inv_update_parent.
    { applys~ rtclosure_r x. applys~ covariant_rtclosure. rel_crush. splits~; fupdate_case. } }

  intros stack'' fuel' ->. subst ys'.
  xpull. intros (Hinvb' & Hfuel'le & Hfuel' & Hmeasure).
  (* Tail-recursive call *)
  xapp~. now rew_list~.
  { piggybank: *. rewrite -Z.le_sub_le_add_l /cred_inv.
    transitivity
      ((a - step_cost) * fuel' + b * (fuel' + length stack'') + c - stop_cost).
    math_nia.
    defer?: (0 <= a - step_cost).
    rewrite Hfuel' (proj2 Hfuel'le).
    rewrite (_: length stack' = length (x :: stack') - 1). now rew_list.
    rewrite Z.add_sub_assoc. piggybank: * done. }

  intros res. hpull; intros M'' P'' ?. unpack. hsimpl.
  splits~. now destruct~ res.

  simp. deferred; intros.
  cancel (fuel + length stack). math_lia.
  cancel fuel. repeat case_max; try pols; try done; defer.
  repeat case_max; try pols; defer.
  end defer. elia. end defer. elia.
Qed.

Hint Extern 1 (RegisterSpec visit_backward) =>
  Provide visit_backward_spec.

Arguments Filter.sort : simpl never.

Class backward_search_spec_ty := {
  bs_a : Z; bs_b : Z;
  bs_a_nonneg : 0 <= bs_a;
  backward_search_spec :
   forall fuel g G L M Ms I P (v w: vertex),
   let cost := (fun n => bs_a * n + bs_b) in
   0 <= fuel ->
   v <> w ->
   v \in vertices G ->
   w \in vertices G ->
   L w <= L v ->
   Inv G L I P ->
   app backward_search [fuel g v w]
     PRE (\$ cost fuel \* IsRawGraph g G L M Ms I P)
     POST (fun (res: backward_search_result_) =>
       Hexists M' Ms' P',
         IsRawGraph g G L M' Ms' I P' \*
         \[ Inv G L I P' /\
            match res with
            | BackwardCyclic => reachable (with_rev_parents P' (has_edge G)) w v
            | BackwardAcyclic => L v = L w /\ ~ reachable (has_edge G) w v
            | BackwardForward new_w_level visited_mark =>
              M' v = visited_mark
              /\ M' w <> visited_mark
              /\ (forall x,
                     M' x = visited_mark ->
                     L x = L v /\
                     reachable (in_visited (visited M' visited_mark) (with_rev_parents P' (has_edge G))) x v)
              /\ ((L w < L v /\
                   new_w_level = L v /\
                   (forall x,
                       L x = L v ->
                       reachable (has_edge G) x v ->
                       M' x = visited_mark))
                  \/
                  (new_w_level = L v + 1 /\
                   fuel <= card (coacc_edges_at_level G L (L v) v)))
            end
         ])
}.

Instance backward_search_spec_raw : backward_search_spec_ty.
Proof.
  begin defer assuming a b. constructor 1 with a b.
  defer Ha: (0 <= a). assumption.
  introv Hfuel Hvw Hv Hw Hlevels HInv. xcf.
  weaken. xpay. xapp as freshmark. intros Hfreshmark.
  xapps~. xapps~.
  { rew_set. auto. }
  xchange (>> IsRawGraph_forget_parent g0 v). auto. apply pred_incl_refl. (* XXX *)
  forwards Hinv1: Inv_forget_parent v HInv.
  xapp~ as visit_res.
  { rew_set. auto. }
  { apply~ invb_init. intro x. fupdate_case. tauto. iff HH; now false~.
    fupdate_case. apply parents_in_graph_forget, HInv. }

  intros M' P' (HI' & Hmarked & Hw_nomark & Hv_mark & Hres). xmatch.
  { (* visit_res = VisitBackwardCyclic *)
    xrets. unpack. splits~. rewrite -rtclosure_reverse. rel_covariant. }
  { (* visit_res = VisitBackwardInterrupted *)
    xcleanpat. xrets. splits~.
    intros. split.
    { forwards~ [? ?]: exploit_reachable_incoming_level G L.
      applys~ (>> covariant_rtclosure Hmarked). rel_crush. }
    { apply use_path_reverse. rel_covariant. } }
  { (* visit_res = VisitBackwardCompleted *)
    xcleanpat. xapps~. xrets. xif.
    { (* L w = L v *)
      xrets. splits~.
      intro Hpath. forwards~: prove_path_reverse Hpath. forwards~: Hres w.
      applys~ Inv_reachable_same_level. }
    { (* L w <> L v *)
      xrets. splits~.
      { intros. splits~.
        { forwards~ [? ?]: exploit_reachable_incoming_level G L.
          applys~ (>> covariant_rtclosure Hmarked). rel_crush. }
        { applys use_path_reverse. rel_covariant. } }
      { left. splits~. intros. forwards~: Inv_reachable_same_level. } } }

  set Lv := L v. rew_list.
  cancel fuel. defer. defer. end defer. elia.
Qed.

End BackwardSearch.

Existing Instance backward_search_spec_raw.

Hint Extern 1 (RegisterSpec backward_search) =>
  Provide backward_search_spec.

Hint Resolve bs_a_nonneg.

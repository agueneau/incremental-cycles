Set Implicit Arguments.
From TLC Require Import LibTactics LibEqual LibLogic LibFun.
From TLC Require LibSet LibMap LibList LibListZ LibListSub.
From TLC Require LibIntTactics.
From Coq Require Reals.
From Coq.micromega Require Import Lia Lra.
From Coq Require Import ZArith.

(******************************************************************************)

Section List_Set.

Import TLC.LibSet TLC.LibList.

Lemma mem_to_set : forall A (L: list A) x,
  mem x L = x \in to_set L.
Proof. intros. extens. unfold to_set. rew_set. tauto. Qed.

Lemma length_to_set_noduplicates: forall A (L: list A) S,
  noduplicates L ->
  to_set L = S ->
  length L = card S.
Proof.
  introv ? <-. erewrite list_repr_inv_card with (L:=L). auto.
  split~. intros. rewrite mem_to_set. reflexivity.
Qed.

Lemma double_inclusion : forall A (s1 s2 : set A),
  s1 \c s2 ->
  s2 \c s1 ->
  s1 = s2.
Proof. introv H1 H2. rew_set in *. firstorder. Qed.

End List_Set.

(******************************************************************************)

Hint Rewrite LibList.mem_rev_eq : rew_listx.

(*
Section ListZ.
Import LibListZ.

Lemma mem_take_nat : forall A (l: list A) x (n: nat),
  mem x (LibList.take n l) ->
  mem x l.
Proof.
  induction l; intros; rew_listx in *.
  - tauto.
  - destruct n; rew_listx in *.
    + tauto.
    + branches; [ left~ | right ]. eauto using IHl.
Qed.

Lemma mem_take : forall A (l: list A) x n,
  mem x (take n l) ->
  mem x l.
Proof. introv H. applys (>> mem_take_nat H). Qed.

End ListZ.
*)

(******************************************************************************)

Section List.
Import LibList LibListSub.

(* todo: _eq variants ? *)
Lemma noduplicates_cons_inv : forall A (l: list A) (x: A),
  noduplicates (x :: l) ->
  noduplicates l /\ ~ mem x l.
Proof. introv H. inversion~ H. Qed.

Lemma noduplicates_cons_tail : forall A (l: list A) x,
  noduplicates (x :: l) ->
  noduplicates l.
Proof. intros. forwards*: noduplicates_cons_inv. Qed.

Lemma noduplicates_cons_nomem : forall A (l: list A) x,
  noduplicates (x :: l) ->
  ~ mem x l.
Proof. intros. forwards*: noduplicates_cons_inv. Qed.

(*
Lemma noduplicates_last : forall A (l: list A) (x: A),
  noduplicates l ->
  ~ mem x l ->
  noduplicates (l & x).
Proof.
  introv ? ?. applys~ noduplicates_app.
  - applys~ noduplicates_one.
  - intros. rew_listx in *. branches~. substs~.
Qed.

Lemma noduplicates_last_inv : forall A (l: list A) (x: A),
  noduplicates (l & x) ->
  noduplicates l /\ ~ mem x l.
Proof.
  introv H. forwards (?&?&HM): noduplicates_app_inv H.
  splits~. rew_logic in HM. specializes HM x. rew_logic in HM.
  branches~.
Qed.

Lemma noduplicates_rev : forall A (l: list A),
  noduplicates (rev l) = noduplicates l.
Proof.
  intros. induction l; rew_listx~.
  rew_logic. split; intro H.
  { forwards [? ?]: noduplicates_last_inv H. rew_listx in *.
    rewrite IHl in *. applys~ noduplicates_cons. }
  { forwards [? ?]: noduplicates_cons_inv H.
    applys~ noduplicates_last. rewrite~ IHl. rew_listx~. }
Qed.
*)

Lemma prefix_last_mem : forall A (x:A) xs xs',
  prefix (xs' & x) xs ->
  mem x xs.
Proof.
  introv H. destruct H. subst xs. rew_listx~.
Qed.

Definition suffix {A} (ys xs : list A) :=
  exists zs, zs ++ ys = xs.

Lemma suffix_cons_mem : forall A (x:A) xs xs',
  suffix (x :: xs') xs ->
  mem x xs.
Proof.
  introv H. destruct H. subst xs. rew_listx~.
Qed.

(*
Lemma suffix_mem : forall A x (xs xs': list A),
  suffix xs' xs ->
  mem x xs' ->
  mem x xs.
Proof.
  introv H1 H2. destruct H1. subst xs. rew_listx~.
Qed.
*)

Lemma Forall_map : forall A B (P: B -> Prop) (f: A -> B) (l: list A),
  Forall (fun a => P (f a)) l ->
  Forall P (LibList.map f l).
Proof.
  induction l as [| a l Hind]; rew_listx~.
  intros (? & ?). autos.
Qed.

Lemma Forall_combine : forall A (P Q: A -> Prop) (l: list A),
  Forall P l ->
  Forall Q l ->
  Forall (fun a => P a /\ Q a) l.
Proof.
  introv HP HQ. induction l; rew_listx~ in *. tauto.
Qed.

End List.

Hint Resolve prefix_last_mem suffix_cons_mem.

(******************************************************************************)
(* fupdate *)

Ltac fupdate_case :=
  rewrite fupdate_eq; case_if~.

Ltac fupdate_case_in H :=
  rewrite fupdate_eq in H; case_if~.

Lemma fupdate_fupdate_same : forall A B f (x:A) (y y':B),
  fupdate (fupdate f x y) x y' = fupdate f x y'.
Proof.
  intros. apply fun_ext_1. intro. repeat fupdate_case.
Qed.

(******************************************************************************)

Section Set_.

Import TLC.LibSet TLC.LibIntTactics.
Open Scope set_scope.

Lemma comprehension_incl : forall A (E: set A) (P: A -> Prop),
  incl \set{ x \in E | P x } E.
Proof.
  intros. rew_set. intro. rew_set. tauto.
Qed.

(*
Lemma incl_split : forall A (s1 s2: set A),
  s1 \c s2 ->
  s2 = s1 \u (s2 \- s1).
Proof.
  intros. rew_set. intros. rew_set. splits; intros.
  - tests~: (x \in s1).
  - branches.
    + eapply incl_inv; eassumption.
    + tauto.
Qed.

Lemma card_diff_incl : forall A (s1 s2: set A),
  finite s1 ->
  finite s2 ->
  s2 \c s1 ->
  card (s1 \- s2) = (card s1 - card s2)%nat.
Proof.
  intros.
  rewrite incl_split with (s1 := s2) (s2 := s1) at 2 by assumption.
  rewrite card_disjoint_union; auto using remove_disjoint, finite_remove.
  math.
Qed.

Lemma card_diff_incl' : forall A (s1 s2: set A),
  finite s1 ->
  finite s2 ->
  s2 \c s1 ->
  Z_of_nat (card (s1 \- s2)) = card s1 - card s2.
Proof.
  introv ? ? HI. forwards~ HH: card_diff_incl s1 s2. rewrite HH.
  enough (card s2 <= card s1) by math_lia.
  apply~ card_le_of_incl.
Qed.
*)

Lemma card_le_of_incl' : forall A (X Y: set A),
  LibSet.finite Y ->
  X \c Y ->
  (card X <= card Y)%Z.
Proof.
  intros. forwards~: card_le_of_incl; eauto. math.
Qed.

Lemma card_diff_single' : forall (A : Type) (E : set A) (x : A),
  finite E ->
  x \in E ->
  Z_of_nat (card (E \-- x)) = (card E) - 1.
Proof.
  intros. rewrite~ card_diff_single. enough (1 <= card E) by math_lia.
  assert (1 = Z.of_nat 1%nat) as -> by math_lia.
  rewrite <-(card_single x). apply~ card_le_of_incl'.
  rew_set. intro. rew_set. intros ->. auto.
Qed.

(*
Lemma disj_union_singl : forall A (E1 E2: set A) (x: A),
  ~ x \in E1 ->
  E1 \# E2 ->
  E1 \# E2 \u '{x}.
Proof.
  introv Hx HD. rew_set in *.
  intros z ? ?. rew_set in *. specializes HD z.
  branches~. subst z. false.
Qed.
*)

Definition prod {A B} (a : set A) (b : set B) : set (A * B) :=
  fun '(u, v) => a u /\ b v.

Lemma in_prod_eq : forall A B (a : set A) (b : set B) u v,
  (u, v) \in prod a b = (u \in a /\ v \in b).
Proof. now unfold prod. Qed.

Lemma finite_prod : forall A B (a : set A) (b : set B),
  finite a -> finite b -> finite (prod a b).
Proof.
  introv [La Ha] [Lb Hb]. exists (List.list_prod La Lb).
  intros [u v] [Hu Hv]. specialize (Ha u Hu). specialize (Hb v Hv).
  induction Ha as [La|u0 La Ha IH].
  - simpl. generalize (List.list_prod La Lb). now induction Hb; simpl; auto.
  - revert IH. simpl. generalize (List.list_prod La Lb).
    clear Hb. now induction Lb; simpl; auto.
Qed.

Lemma card_prod : forall A B (a : set A) (b : set B),
  finite a -> finite b ->
  card (prod a b) = (card a * card b)%nat.
Proof.
  assert (Hin : forall T l (x : T), LibList.mem x l <-> List.In x l).
  { intros T l x. induction l as [|x' l IH]. now split; inversion 1.
    simpl. rewrite <-IH. rewrite LibList.mem_cons_eq. clear. now intuition. }
  assert (Happ : forall T (l1 l2 : list T), app l1 l2 = LibList.app l1 l2).
  { induction l1 as [|x l1 IH]. auto. intros l2. simpl. now rewrite IH. }
  assert (Hlen : forall T (l : list T), length l = LibList.length l).
  { induction l as [|x l IH]; [now auto|]. simpl. now rewrite IH. }
  introv Hfa Hfb.
  rewrite (card_eq_length_to_list Hfa), (card_eq_length_to_list Hfb).
  rewrite (list_repr_inv_card (L:=List.list_prod (to_list a) (to_list b))).
  { now rewrite <-!Hlen, List.prod_length. }
  split.
  - generalize (noduplicates_of_list_repr (list_repr_to_list_of_finite Hfb)).
    generalize (noduplicates_of_list_repr (list_repr_to_list_of_finite Hfa)).
    generalize (to_list a), (to_list b). clear -Hin Happ Hlen.
    intros a b Ha Hb. induction Ha as [|u a Hnotmem Ha IH]; [now constructor|simpl].
    rewrite Happ. apply LibList.noduplicates_app; [|now auto|].
    { clear -Hb. induction Hb as [|? b Hmem _ IH]; constructor; auto.
      contradict Hmem. clear -Hmem.
      now induction b; inversion Hmem; subst; constructor; auto. }
    intros [u' v'] Hu'v'1%Hin [Hu'v'2 _]%Hin%List.in_prod_iff.
    rewrite List.in_map_iff in Hu'v'1. destruct Hu'v'1 as (v & [= <- <-] & _).
    apply Hnotmem, Hin, Hu'v'2.
  - intros [u v]. rewrite Hin, List.in_prod_iff, <-!Hin.
    destruct (list_repr_to_list_of_finite Hfa) as [_ ->].
    destruct (list_repr_to_list_of_finite Hfb) as [_ ->].
    unfold prod. now auto.
Qed.

Lemma finite_in_iff_mem_to_list : forall A (E: set A) (x: A),
  LibSet.finite E ->
  (x \in E <-> LibList.mem x (to_list E)).
Proof.
  introv HF. rewrite~ finite_eq_in_iff_mem_to_list in HF.
Qed.

Definition fupdate_add A B (f: A -> set B) a b :=
  fupdate f a (f a \u \{b}).

Lemma fupdate_add_fupdate : forall A B (f: A -> set B) x Y y,
  fupdate_add (fupdate f x Y) x y = fupdate f x (Y \u \{y}).
Proof.
  intros. unfold fupdate_add. fupdate_case. apply fun_ext_1;=> x'.
  fupdate_case; subst; repeat fupdate_case.
Qed.

End Set_.

(******************************************************************************)

Definition xor3 (P Q R: Prop) :=
  (P /\ ~ Q /\ ~ R) \/ (Q /\ ~ P /\ ~ R) \/ (R /\ ~ P /\ ~ Q).

Lemma xor3_left : forall (P Q R: Prop),
  P -> ~ Q -> ~R ->
  xor3 P Q R.
Proof. unfold xor3. tauto. Qed.

Lemma xor3_middle : forall (P Q R: Prop),
  Q -> ~ P -> ~ R ->
  xor3 P Q R.
Proof. unfold xor3. tauto. Qed.

Lemma xor3_right : forall (P Q R: Prop),
  R -> ~ P -> ~ Q ->
  xor3 P Q R.
Proof. unfold xor3. tauto. Qed.

Ltac branchx H :=
  let HH := fresh in
  destruct H as [HH | [HH | HH]]; (
    let H0 := fresh in
    let N1 := fresh in
    let N2 := fresh in
    destruct HH as (H0 & N1 & N2);
    rew_logic in N1; rew_logic in N2;
    unpack N1; unpack N2
  ).

Tactic Notation "branchx" :=
  match goal with H: xor3 _ _ _ |- _ => branchx H end.

Ltac xor3 :=
  match goal with
  | _: ~ ?Q, _: ~ ?R |- xor3 ?P ?Q ?R => apply~ xor3_left
  | _: ~ ?P, _: ~ ?R |- xor3 ?P ?Q ?R => apply~ xor3_middle
  | _: ~ ?P, _: ~ ?Q |- xor3 ?P ?Q ?R => apply~ xor3_right
  | _: ?P |- xor3 ?P ?Q ?R => apply~ xor3_left
  | _: ?Q |- xor3 ?P ?Q ?R => apply~ xor3_middle
  | _: ?R |- xor3 ?P ?Q ?R => apply~ xor3_right
  end.

(******************************************************************************)

Section Fun.

Import TLC.LibSet TLC.LibFun TLC.LibList.
Import TLC.LibInt.

(*
Lemma image_card : forall A B (E: set A) (f: A -> B),
  finite E ->
  card (image f E) <= card E.
Proof.
  introv FE.
  forwards [L [H ->]]: finite_inv_list_repr_and_card FE.
  forwards~ [L' [H' ->]]: finite_inv_list_repr_and_card (image f E) finite_image.
  destruct H as [H1 H2]. destruct H' as [H1' H2'].
  transitivity (length (map f L)).
  { applys~ noduplicates_length_le. intros ? H.
    rewrite H2' in H. unfold image in H; rew_set in H. destruct H as [y [? ->]].
    apply mem_map. rewrite~ H2. }
  rewrite length_map. math.
Qed.
*)

Lemma mem_map_injective : forall A B (f: A -> B) (L: list A) x,
  LibOperation.injective f ->
  mem (f x) (map f L) ->
  mem x L.
Proof.
  introv I M. induction L; rew_listx in M.
  - false.
  - destruct~ M as [M|M]. forwards: I M. subst~.
Qed.

Lemma map_mem : forall A B (f: A -> B) (L: list A) (y:B),
  mem y (map f L) ->
  exists x, mem x L /\ y = f x.
Proof.
  introv M. induction L as [| x]; rew_listx in M.
  - false.
  - destruct~ M as [M|M].
    now exists~ x.
    destruct (IHL M) as [x' [? ->]]. exists~ x'.
Qed.

Lemma noduplicates_map_injective : forall A B (f: A -> B) (L: list A),
  LibOperation.injective f ->
  noduplicates L ->
  noduplicates (LibList.map f L).
Proof.
  induction 2 using noduplicates_ind; rew_listx.
  - apply noduplicates_nil.
  - apply~ noduplicates_cons. intros M. forwards~: mem_map_injective M.
Qed.

Lemma injective_image_card : forall A B (E: set A) (f: A -> B),
  LibOperation.injective f ->
  finite E ->
  card (image f E) = card E.
Proof.
  introv I FE.
  forwards [L [H ->]]: finite_inv_list_repr_and_card FE.
  forwards~ [L' [H' ->]]: finite_inv_list_repr_and_card (image f E) finite_image.
  destruct H as [H1 H2]. destruct H' as [H1' H2'].
  transitivity (LibList.length (LibList.map f L)).
  { applys~ LibList.noduplicates_length_eq.
    now applys~ noduplicates_map_injective.
    intros x. rewrite H2'. unfold image. rew_set. splits.
    { intros [? [? ->]]. applys~ mem_map. rewrite <-H2 in *. auto. }
    { intros H. forwards~ [z [? ->]]: map_mem H. exists~ z. rewrite~ <-H2. } }
  rewrite~ length_map.
Qed.

End Fun.

(******************************************************************************)

Section Monoid.

Import TLC.LibInt.

Definition Zadd_monoid :=
  LibMonoid.monoid_make Z.add 0%Z.

Instance Zadd_Monoid : LibMonoid.Monoid Zadd_monoid.
Proof.
  unfold Zadd_monoid. constructor; unfolds; math.
Qed.

Instance Zplus_Comm_monoid : LibMonoid.Comm_monoid Zadd_monoid.
Proof.
  constructor. apply Zadd_Monoid. unfold Zadd_monoid. unfolds; math.
Qed.

End Monoid.

Existing Instance Zadd_Monoid.
Existing Instance Zplus_Comm_monoid.

(******************************************************************************)

Section R.
Import Reals.

Open Scope Z_scope.

Lemma Int_part_nonneg:
  forall (r: R),
  (0 <= r)%R ->
  0 <= Int_part r.
Proof.
  intros * Hr.
  enough (-1 < Int_part r) by lia. apply lt_IZR.
  destruct (base_Int_part r) as [_ ?]. lra.
Qed.

Lemma Int_part_upper : forall r,
  (IZR (Int_part r) <= r)%R.
Proof. intros. apply base_Int_part. Qed.

Lemma Int_part_lower : forall r,
  (r - 1 < IZR (Int_part r))%R.
Proof. intros. destruct (base_Int_part r). lra. Qed.

Lemma Int_part_mono : forall (r1 r2: R),
  (r1 <= r2)%R ->
  Int_part r1 <= Int_part r2.
Proof.
  intros * Hr.
  assert (Hl2: (r1 - 1 < IZR (Int_part r2))%R).
  { forwards: Int_part_lower r2. lra. }
  tests: (r1 <= IZR (Int_part r2))%R.
  { forwards: Int_part_upper r1. apply le_IZR. lra. }
  { (* Both [IZR (Int_part r1)] and [IZR (Int_part r2)] are between [r1 - 1] and
       [r1], and are integers. Hence they are equal. *)
    forwards ->: single_z_r_R1 (r1 - 1)%R (Int_part r1) (Int_part r2); try lra.
    now apply Int_part_lower. now forwards: Int_part_upper r1; lra. apply Z.le_refl. }
Qed.

End R.
Set Implicit Arguments.

From Coq Require Import Reals.
From Coq.micromega Require Import Lra Lia.
From PolTac Require Import PolTac.

From TLC Require Import LibInt LibRelation LibSet LibMap LibLogic LibTactics
                        LibEpsilon LibWf LibFun LibIntTactics.
Require Import ssreflect.
Require Import TLCBuffer.
Require Import LibRelationExtra.
Require Import GraphModel Paths.

Require Import RawGraph.
Require Import GraphFunctionalInvariants.
Require Import PowerTwoThird.

Open Scope graph_scope.
Open Scope Z_scope.
Close Scope boolean_if_scope.
Open Scope general_if_scope.

Local Ltac auto_tilde ::= eauto with maths graph.

(******************************************************************************)
(* Cost invariants *)

Section GraphCostInvariants.

Context `{GM: GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)

(******************************************************************************)
(* Upper bound on the number of levels *)

Definition max_level (m n : Z) :=
  Z.min
    (Z.sqrt_up (2 * m))
    (Int_part (power_two_third (3/2 * IZR n)))
  + 1.

Lemma max_level_nonneg: forall m n,
  0 <= max_level m n.
Proof.
  intros. unfold max_level.
  apply~ Z.add_nonneg_nonneg. apply Z.min_glb.
  now apply Z.sqrt_up_nonneg. apply Int_part_nonneg.
  apply power_two_third_nonneg.
Qed.

Lemma max_level_mono_l : forall m m' n,
  m <= m' ->
  max_level m n <= max_level m' n.
Proof.
  intros. unfold max_level.
  forwards~: Z.sqrt_up_le_mono (2 * m) (2 * m'). math_lia.
Qed.

Lemma max_level_mono_r : forall m n n',
  n <= n' ->
  max_level m n <= max_level m n'.
Proof.
  intros. unfold max_level.
  apply Zplus_le_compat_r, Z.min_le_compat_l.
  apply Int_part_mono, power_two_third_mono.
  forwards~: IZR_le n n'. lra.
Qed.

(******************************************************************************)

(* If [Inv] holds, then the number of levels is indeed bounded by
   [max_level]... *)

Lemma Inv_card_edges_lower_bound_rec :
  forall G L I P k,
  Inv G L I P ->
  (exists x, L x = k) ->
  ((IZR k-1)² / 2 <=
   IZR (card (set_st (fun '(x, y) => has_edge G x y /\ L x < k /\ L y < k)%Z)))%R.
Proof.
  introv INV. induction_wf IH: (downto 1) k. introv [x <-].
  forwards~ [HLx%Z.lt_gt|<-]%Zle_lt_or_eq : Inv_levels x; cycle 1.
  { apply (Rle_trans _ 0%R). unfold Rsqr. lra. apply IZR_le. math. }
  forwards~ Hcard : Inv_edges_below x. unfold enough_edges_below in Hcard.
  forwards IH' : IH (L x - 1); [math| |].
  { assert (exists e, e \in coacc_edges_at_level G L (L x - 1) x)
      as [[a b] [[?[??]]?]]; [|now eauto].
    apply exists_of_not_forall. contradict Hcard.
    rewrite (is_empty_prove (coacc_edges_at_level _ _ _ _)) // card_empty.
    math. }
  forwards Hcard' : Rplus_le_compat.
  { apply IZR_le, Hcard. } { apply IH'. }
  rewrite <-plus_IZR, <-plus_nat_eq_plus_int, <-card_disjoint_union in Hcard'; cycle 1.
  { auto. }
  { eapply finite_incl, (finite_graph_edges G). apply incl_prove.
    intros [u v]. rewrite in_set_st_eq. tauto. }
  { apply disjoint_prove. intros [u v] [[_ [? _]] _]. rewrite in_set_st_eq. math. }
  eapply Rle_trans; [|eapply Rle_trans; [apply Hcard'|]].
  { rewrite minus_IZR. unfold Rsqr. lra. }
  apply IZR_le, card_le_of_incl'.
  { eapply finite_incl, (finite_graph_edges G). apply incl_prove.
    intros [u v]. rewrite in_set_st_eq. tauto. }
  apply incl_prove. intros [u v].
  unfold coacc_edges_at_level, edges_at_level, coacc_edges.
  rewrite set_in_union_eq set_in_inter_eq !in_set_st_eq. now intuition math.
Qed.

Lemma Inv_card_edges_lower_bound : forall G L I P k,
  Inv G L I P ->
  (exists x, L x = k) ->
  ((IZR k-1)² / 2 <= IZR (card (edges G)))%R.
Proof.
  introv INV Hk. eapply Rle_trans. eapply Inv_card_edges_lower_bound_rec; now eauto.
  apply IZR_le, card_le_of_incl'; [now apply finite_graph_edges|].
  apply incl_prove. intros [u v]. rewrite in_set_st_eq. intuition.
Qed.

Lemma Inv_max_level_edges : forall G L I P k,
  Inv G L I P ->
  (exists x, L x = k) ->
  k <= Z.sqrt_up (2 * card (edges G)) + 1.
Proof.
  introv INV Hk. apply Z.le_sub_le_add_r, Z.square_le_simpl_nonneg.
  { apply Z.sqrt_up_nonneg. }
  etransitivity; [|now apply Z.sqrt_sqrt_up_spec; math].
  apply le_IZR. rewrite !mult_IZR minus_IZR.
  forwards Hle : Inv_card_edges_lower_bound G L I P k; [now auto..|].
  unfold Rsqr in Hle. lra.
Qed.

Lemma Inv_card_vertices_lower_bound_rec : forall G L I P k,
  Inv G L I P ->
  (exists x, L x = k) ->
  (2/3 * (IZR k-1) * sqrt (IZR k-1) <=
     IZR (card (set_st (fun x => x \in vertices G /\ L x < k)%Z)))%R.
Proof.
  introv INV. induction_wf IH: (downto 1) k. introv [x <-].
  forwards~ [HLx%Z.lt_gt|HLx]%Zle_lt_or_eq : Inv_levels x; cycle 1.
  { rewrite <- HLx. ring_simplify. apply IZR_le. math. }
  forwards~ Hcard_edges : Inv_edges_below x.
  unfold enough_edges_below in Hcard_edges.
  forwards IH' : IH (L x - 1); [math| |].
  { assert (exists e, e \in coacc_edges_at_level G L (L x - 1) x)
      as [[a b] [[?[??]]?]]; [|now eauto].
    apply exists_of_not_forall. contradict Hcard_edges.
    rewrite (is_empty_prove (coacc_edges_at_level _ _ _ _)) // card_empty.
    math. }
  assert (HcardLx : (sqrt (IZR (L x) - 1) <=
            IZR (card (set_st (fun y => y \in vertices G /\ L y = L x - 1)%Z)))%R).
  { apply Rsqr_incr_0_var; [|apply IZR_le; math].
    rewrite <-minus_IZR, Rsqr_sqrt; [|apply IZR_le; math].
    unfold Rsqr. rewrite <-mult_IZR. apply IZR_le.
    etransitivity; [now apply Hcard_edges|].
    assert (LibSet.finite (set_st (fun y : vertex => y \in vertices G /\ L y = (L x - 1)%I))).
    { eapply finite_incl, (finite_graph G). apply incl_prove.
      intros u. rewrite in_set_st_eq. tauto. }
    rewrite <-Nat2Z.inj_mul, <-card_prod; [|now auto..].
    apply inj_le, card_le_of_incl. now auto using finite_prod.
    apply incl_prove. intros [u v]. unfold coacc_edges_at_level, edges_at_level.
    rewrite in_inter_eq in_prod_eq !in_set_st_eq. intros [(Huv & -> & ->) _].
    repeat splits~. apply~ has_edge_in_vertices_l. apply~ has_edge_in_vertices_r. }
  forwards Hcard' : Rplus_le_compat.
  { apply HcardLx. } { apply IH'. }
  rewrite <-plus_IZR, <-plus_nat_eq_plus_int, <-card_disjoint_union in Hcard'; cycle 1.
  { eapply finite_incl, (finite_graph G). apply incl_prove.
    intros u. rewrite in_set_st_eq. tauto. }
  { eapply finite_incl, (finite_graph G). apply incl_prove.
    intros u. rewrite in_set_st_eq. tauto. }
  { apply disjoint_prove. intros u. rewrite !in_set_st_eq. math. }
  eapply Rle_trans; [|eapply Rle_trans; [apply Hcard'|]]; cycle 1.
  { apply IZR_le, card_le_of_incl'.
    - eapply finite_incl, (finite_graph G). apply incl_prove.
      intros u. rewrite in_set_st_eq. tauto.
    - apply incl_prove. intros u. rewrite in_union_eq !in_set_st_eq.
      clear. now intuition math. }
  clear -HLx. rewrite minus_IZR. apply Z.gt_lt, Zlt_le_succ, IZR_le in HLx. simpl in HLx.
  replace (IZR (L x) - 1 - 1)%R with (IZR (L x) - 2)%R by lra.
  assert (2 * (IZR (L x) - 2) * (sqrt (IZR (L x) - 1) - sqrt (IZR (L x) - 2)) <=
          sqrt (IZR (L x) - 1))%R; [|lra].
  assert (0 < sqrt (IZR (L x) - 1) + sqrt (IZR (L x) - 2))%R as Hdemompos.
  { rewrite <-(Rplus_0_r 0). apply Rplus_lt_le_compat, sqrt_pos.
    apply sqrt_lt_R0. lra. }
  replace (sqrt (IZR (L x) - 1) - sqrt (IZR (L x) - 2))%R
    with (/(sqrt (IZR (L x) - 1) + sqrt (IZR (L x) - 2)))%R; cycle 1.
  { eapply Rmult_eq_reg_r; [rewrite -> Rinv_l; [|lra]|lra].
    rewrite Rsqr_minus_plus !Rsqr_sqrt; lra. }
  eapply Rmult_le_reg_r; [apply Hdemompos|]. rewrite Rmult_assoc ?Rinv_l; [lra|].
  rewrite Rmult_plus_distr_l sqrt_def; [lra|].
  eapply Rle_trans, Rplus_le_compat_l, Rmult_le_compat_r, sqrt_le_1_alt.
  - rewrite -> sqrt_def; lra.
  - apply sqrt_pos.
  - lra.
Qed.

Lemma Inv_card_vertices_lower_bound : forall G L I P k,
  Inv G L I P ->
  (exists x, L x = k) ->
  (2/3 * (IZR k-1) * sqrt (IZR k-1) <= IZR (card (vertices G)))%R.
Proof.
  introv INV Hk. eapply Rle_trans. eapply Inv_card_vertices_lower_bound_rec; now eauto.
  apply IZR_le, card_le_of_incl'; [now apply finite_graph|].
  apply incl_prove. intros u. rewrite in_set_st_eq. intuition.
Qed.

Lemma Inv_max_level_vertices : forall G L I P k,
  Inv G L I P ->
  (exists x, L x = k) ->
  k <= Int_part (power_two_third (3/2 * IZR (card (vertices G)))) + 1.
Proof.
  introv INV Hk.
  assert (IZR k <= power_two_third (3 / 2 * IZR (card (vertices G))) + 1)%R
    as Hbound; cycle 1.
  { eapply Zlt_succ_le, lt_IZR, Rle_lt_trans.
    - apply Hbound.
    - rewrite succ_IZR plus_IZR.
      match goal with
      | |- context [Int_part ?X] => destruct (base_Int_part X) as [_ ?]; lra
      end. }
  forwards Hbound : Inv_card_vertices_lower_bound G L I P k; [now auto..|].
  assert (1 <= IZR k)%R as Hk1.
  { apply IZR_le. destruct Hk as [x <-]. eauto using Inv_levels. }
  unfold power_two_third. destruct Rle_dec as [?|?%Rnot_le_lt].
  - assert ((IZR k - 1) <= 0 \/ sqrt (IZR k - 1) <= 0)%R as [?|Hbound'] by nra; [lra|].
    apply Rsqr_incr_1 in Hbound'; [|apply sqrt_pos|lra].
    rewrite Rsqr_0 Rsqr_sqrt in Hbound'; lra.
  - destruct (Rlt_dec 1 (IZR k)); cycle 1.
    { unfold Rpower.
      match goal with | |- context [exp ?X] => forwards : exp_pos X end; lra. }
    replace (IZR k) with (IZR k - 1 + 1)%R by lra. apply Rplus_le_compat_r.
    rewrite <-(Rpower_1 (IZR k - 1)); [|lra].
    replace 1%R with ((1+/2) * (2/3))%R at 2 by lra.
    rewrite <-Rpower_mult. apply Rle_Rpower_l; [lra|]. split; [apply exp_pos|].
    rewrite Rpower_plus Rpower_1 ?Rpower_sqrt; lra.
Qed.

Lemma Inv_max_level : forall G L I P x,
  Inv G L I P ->
  L x <= max_level (card (edges G)) (card (vertices G)).
Proof.
  introv ?. unfold max_level. rewrite <-Z.add_min_distr_r.
  eauto 10 using Z.min_glb, Inv_max_level_edges, Inv_max_level_vertices.
Qed.

(******************************************************************************)
(* The potential detained by the graph structure, and responsible for the
   amortization argument. *)

Definition received (m n : Z) :=
  m * (max_level m n + 1).

Definition spent G L : Z :=
  fold Zadd_monoid (fun '(u, v) => L u) (edges G).

Definition graph_received G : Z :=
  received (card (edges G)) (card (vertices G)).

Definition net G L : Z :=
  graph_received G - spent G L.

(* Basic correctness property for [net]: if [Inv] holds, then it holds
   at least one potential per edge. *)
Lemma Inv_net_lowerbound : forall G L I P,
  Inv G L I P ->
  card (edges G) <= net G L.
Proof.
  intros. unfold net, spent, graph_received, received.
  rewrite Z.mul_add_distr_l Z.mul_1_r. pols.
  rewrite {1}fold_eq_fold_to_list {1}card_eq_length_to_list;
    [apply finite_graph_edges|].
  apply Zle_minus_le_0.
  assert (Hlst : forall u v, LibList.mem (u, v) (to_list (edges G)) -> u \in vertices G).
  { intros u v.
    forwards [_ He] : list_repr_to_list_of_finite (edges G);
      [now apply finite_graph_edges|].
    now intros [? _]%He%edges_in_vertices. }
  revert Hlst. induction (to_list (edges G)) as [|[u v] lst IH].
  - intros _. rewrite LibList.fold_nil LibList.length_nil. simpl. math.
  - intros Hlst. rewrite LibList.fold_cons LibList.length_cons. simpl.
    rewrite -> Z_of_nat_S, Z.mul_succ_l, Z.add_comm.
    eauto 10 using Inv_max_level, Zplus_le_compat.
Qed.

Lemma net_graph_empty L :
  net graph_empty L = 0.
Proof.
  rewrite /net /spent /graph_received.
  rewrite graph_empty_edges graph_empty_vertices !card_empty.
  rewrite /received Z.mul_0_l Z.sub_0_l.
  rewrite LibSet.fold_empty. reflexivity.
Qed.

(******************************************************************************)
(* Core lemmas for justifying that the potential is preserved by the forward
   traversal. There are two cases: either we bump the level of a vertex and we
   expect to get some credits back, or we can just bump the level of a vertex and
   not expect anything in return (hence weakening the potential).

   This corresponds to the two [net_*_level] lemmas.
*)

Import LibList.

Lemma spent_bump_level : forall G L x,
  spent G L + card (out_edges G x) = spent G (fupdate L x (L x + 1)).
Proof.
  intros. unfold spent.
  rewrite !fold_eq_fold_to_list card_out_edges_as_list.
  generalize (to_list (edges G)). intro edges. induction edges as [|[u v] edges' IH].
  { rewrite fold_nil length_nil Z.add_0_r. reflexivity. }
  { rewrite fold_cons filter_cons fold_cons. case_if.
    { subst u. rewrite map_cons length_cons.
      fupdate_case. rewrite <-IH. unfold Zadd_monoid, LibMonoid.monoid_oper. math. }
    { fupdate_case. rewrite <-IH. unfold Zadd_monoid, LibMonoid.monoid_oper. math. } }
Qed.

Lemma spent_weaken_level : forall G L x l,
  L x <= l ->
  spent G L <= spent G (fupdate L x l).
Proof.
  introv Hl. unfold spent.
  rewrite !fold_eq_fold_to_list.
  generalize (to_list (edges G)). intro edges. induction edges as [|[u v] edges' IH].
  { rewrite !fold_nil. reflexivity. }
  { rewrite !fold_cons. simpl. fupdate_case. subst. math. }
Qed.

Lemma net_bump_level : forall G L x,
  net G L = net G (fupdate L x (L x + 1)) + card (out_edges G x).
Proof.
  intros. unfold net. rewrite~ <-spent_bump_level.
Qed.

Lemma net_weaken_level : forall G L x l,
  L x <= l ->
  net G (fupdate L x l) <= net G L.
Proof.
  intros. unfold net. forwards~: spent_weaken_level G L.
Qed.

(******************************************************************************)
(* Alternative formulation of the two lemmas above, with an explicit expression
   [deltap] for the number of potential "lost" by weakening when bumping a vertex
   several levels at once, and using only the potential for bumping of one
   level.

   This is only required for technical reasons (we need to keep track of the
   amount of credits that would be "lost" when using [net_weaken_level].
   We cannot GC them immediately, because afterwards it would be difficult to
   justify a posteriori that we didn't GC "too much credits", which could make
   the net negative.

   So we have to keep track of the explicit number of unneeded credits, as well
   as the right inequation to justify that we can eventually GC them (this
   appears in the specification and proof of visit_forward). *)

(* The difference of [net] when moving vertex [x] from level [l1] to [l2]. *)
Definition deltap G L x l1 l2 :=
  net G (fupdate L x l1) - net G (fupdate L x l2).

Lemma deltap_nonneg: forall G L x l1 l2,
  l1 <= l2 ->
  0 <= deltap G L x l1 l2.
Proof.
  intros. unfold deltap. rewrite Z.le_0_sub.
  assert (fupdate L x l2 = fupdate (fupdate L x l1) x l2) as ->.
  { now rewrite fupdate_fupdate_same. }
  apply net_weaken_level. rewrite~ fupdate_same.
Qed.

Lemma net_bump_levels_sub: forall G L x l,
  L x < l ->
  net G (fupdate L x l) = net G L
                          - card (out_edges G x)
                          - deltap G L x (L x + 1) l.
Proof.
  intros. rewrite (net_bump_level G L x). unfold deltap. math.
Qed.

(******************************************************************************)
(* Some administrative lemmas about [received] *)

Lemma received_mono_l : forall m m' n,
  0 <= m <= m' ->
  received m n <= received m' n.
Proof.
  intros. unfold received.
  forwards~: max_level_nonneg m n.
  forwards~: max_level_nonneg m' n.
  forwards~: max_level_mono_l m m' n.
  math_nia.
Qed.

Lemma received_mono_r : forall m n n',
  0 <= m ->
  n <= n' ->
  received m n <= received m n'.
Proof.
  intros. unfold received.
  forwards~: max_level_nonneg m n.
  forwards~: max_level_nonneg m n'.
  forwards~: max_level_mono_r m n n'.
  math_nia.
Qed.

Lemma delta_received_add_edge : forall m n,
  0 <= m ->
  max_level m n <= received (m + 1) n - received m n.
Proof.
  intros. unfold received. rewrite Z.mul_add_distr_r Z.mul_1_l.
  forwards~ lub_mono: max_level_mono_l m (m+1) n. math_nia.
Qed.

Lemma received_nonneg : forall m n,
  0 <= m ->
  0 <= received m n.
Proof.
  intros. rewrite /received -max_level_nonneg //. math_nia.
Qed.

(******************************************************************************)
(* Variation of [net] when adding an edge/vertex *)

Lemma spent_add_edge : forall G L v w,
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  spent (G \+> (v, w)) L = spent G L + L v.
Proof.
  intros. rewrite /spent edges_add_edge //.
  rewrite LibSet.fold_union. apply finite_graph_edges. apply finite_single.
  { rew_set. intros [? ?]. unfold has_edge in *. rew_set. congruence. }
  simpl. rewrite LibSet.fold_single. reflexivity.
Qed.

Lemma spent_add_vertex : forall G L v,
  spent (G \++ v) L = spent G L.
Proof.  intros. rewrite /spent edges_add_vertex //. Qed.

Lemma delta_net_add_edge : forall G L v w,
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  net (G \+> (v, w)) L - net G L =
  graph_received (G \+> (v, w)) - graph_received G - L v.
Proof. intros. rewrite /net spent_add_edge //. math. Qed.

Lemma delta_net_add_vertex : forall G L v,
  net (G \++ v) L - net G L =
  graph_received (G \++ v) - graph_received G.
Proof. intros. rewrite /net spent_add_vertex. math. Qed.

End GraphCostInvariants.
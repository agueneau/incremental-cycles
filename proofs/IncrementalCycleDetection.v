From Coq Require Import ZArith Reals.

Require Simple_sparse_ml SimpleSparse_proof.
Require Import RawGraph.
Require Import GraphModel.
Require Import Graph.

From TLC Require Import LibSet LibRelation.
From BigO Require Import Dominated.
From CFML Require Import CFLib.
Require Import LibRelationExtra.

(* It is up to the user to use this specific logical graph model, or provide one
   implementing the GraphModel interface. *)
Require Import GraphModelInst.
(* alternatively (in a section): Context `{GM: GraphModel} *)

(* External "potential" cost function that describes the cost of cumulated edge
   and vertex insertions. Its definition is not exposed. *)
Definition ψ (m n : Z) : Z :=
  SimpleSparse_proof.ψ m n.

(* ψ is O(m * min (m^1/2, n^2/3) + n) *)
Theorem dominated_ψ :
  dominated (product_filterType Z_filterType Z_filterType)
    (fun '(m, n) => ψ m n)
    (fun '(m, n) =>
       m * Z.min (Z.sqrt_up m)
                 (Int_part (Rpower (IZR n) (2/3)))
       + n).
Proof. apply SimpleSparse_proof.dominated_ψ. Qed.

(* ψ is non-negative *)
Theorem ψ_nonneg : forall m n,
  0 <= m -> 0 <= n ->
  0 <= ψ m n.
Proof. apply SimpleSparse_proof.ψ_nonneg. Qed.

(* ψ is monotonic *)
Lemma ψ_mono : forall m n m' n',
  0 <= m <= m' ->
  n <= n' ->
  ψ m n <= ψ m' n'.
Proof. apply SimpleSparse_proof.ψ_mono. Qed.

(* Adding a vertex or an edge costs the variation of ψ *)

(* Adding a vertex to a graph with m edges and n vertices has cost
   [ψ m (n+1) - ψ m n]
*)
Theorem add_vertex_spec : forall g G v,
  let m := card (edges G) in
  let n := card (vertices G) in
  app Simple_sparse_ml.add_vertex [g v]
    PRE (IsGraph g G \* IsNewVertex v \* \$ (ψ m (n+1) - ψ m n))
    POST (fun (_:unit) => IsGraph g (G \++ v)).
Proof. apply SimpleSparse_proof.add_vertex_spec. Qed.

Definition get_cycle_cost (n: Z) : Z :=
  SimpleSparse_proof.get_cycle_cost n.

Theorem get_cycle_cost_dominated :
  dominated Z_filterType get_cycle_cost (fun n => n).
Proof. apply~ SimpleSparse_proof.get_cycle_cost_dominated. Qed.

Definition get_cycle_spec f g G L M Ms I P v w :=
  app f [tt]
    PRE (IsRawGraph g G L M Ms I P)
    POST (fun l => Hexists lpath,
      \$ (- get_cycle_cost (length l)) \*
      IsRawGraph g G L M Ms I P \*
      \[ l = w :: LibList.map snd lpath /\
         is_path (has_edge G) lpath w v ]).

(* Adding an edge to a graph with m edges and n vertices has (amortized) cost
   [ψ (m+1) n - ψ m n]
*)
Theorem add_edge_spec : forall g G v w,
  let m := card (edges G) in
  let n := card (vertices G) in
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  app Simple_sparse_ml.add_edge_or_detect_cycle [g v w]
    PRE (IsGraph g G \* \$ (ψ (m+1) n - ψ m n))
    POST (fun (res: Simple_sparse_ml.add_edge_result_) =>
      match res with
      | Simple_sparse_ml.EdgeAdded =>
        IsGraph g (G \+> (v, w))
      | Simple_sparse_ml.EdgeCreatesCycle get_cycle =>
        Hexists M' Ms' L' I' P', IsRawGraph g G L' M' Ms' I' P' \*
        \[ rtclosure (has_edge G) w v /\
           get_cycle_spec get_cycle g G L' M' Ms' I' P' v w ]
      end).
Proof. apply SimpleSparse_proof.add_edge_spec. Qed.

(* From the specifications for [add_vertex] and [add_edge], it immediately
   follows that adding k edges and h vertices to a graph with initially m edges
   and n vertices has amortized cost [ψ (m + k) (n + h) - ψ m n].
*)

Theorem IsGraph_acyclic : forall g G,
  IsGraph g G ==>
  IsGraph g G \* \[ forall x, ~ tclosure (has_edge G) x x ].
Proof. apply Graph.IsGraph_acyclic. Qed.

Theorem IsGraph_GC : forall g G,
  IsGraph g G ==> \GC.
Proof. apply Graph.IsGraph_GC. Qed.

Theorem init_graph_spec : exists k,
  app Raw_graph_ml.init_graph [tt]
    PRE (\$ k) POST (fun g => IsGraph g graph_empty).
Proof. apply Graph.init_graph_IsGraph_spec. Qed.

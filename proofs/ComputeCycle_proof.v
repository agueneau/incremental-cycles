Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the CF definitions. *)
Require Import Simple_sparse_ml.

Require Import ssreflect.
Require Import PolTac.PolTac.
Require Import BigO.elia.
Require Import BigO.CFMLBigO.
Require Import HeapTacticsExtra.
From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation.

Require Import TLCBuffer LibRelationExtra LibTacticsExtra.
Require Import GraphModel RawGraph Paths.
Require Import GraphFunctionalInvariants.

Open Scope set_scope.
Open Scope graph_scope.

Section ComputeCycle.

Context `{GM: GraphModel vertex}.

Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)

Local Ltac auto_tilde ::= eauto with graph maths.
Local Ltac done ::= solve [autos~].

(******************************************************************************)

Lemma list_of_parents_spec :
  specZ [cost \in_O (fun n => n)] (
    forall g x y acc G M Ms L I P edge,
    reachable (with_parents P edge) x y ->
    (forall z p,
      z \in vertices G ->
      P z = Some p ->
      p \in vertices G) ->
    (forall x, ~ tclosure edge x x) ->
    x \in vertices G ->
    y \in vertices G ->
    app list_of_parents [g y x acc]
      PRE (IsRawGraph g G L M Ms I P)
      POST (fun l' => Hexists lpath,
        \$ (- cost (length lpath)) \*
        IsRawGraph g G L M Ms I P \*
        \[ l' = LibList.map fst lpath ++ acc /\
           is_path (with_parents P edge) lpath x y ])).
Proof.
  xspecO_refine recursive. introv _M D _g.
  introv Hpath Hvis HA Hx Hy.
  forwards [l Hl]: rtclosure_is_path Hpath.
  clear Hpath. revert acc y Hy Hl.
  induction l as [| [u v] l' IHl] using list_ind_last.
  { intros. forwards <-: is_path_nil_inv Hl.
    xcf. xpay. credr2l. hsimpl.
    xapps~. credr2l. hsimpl.
    xif; [|now false].
    xret. hsimpl (@nil vertex). rew_listx. 2: split~; rew_listx~.
    hsimpl_credits. rewrite le_zarith. rew_list. defer. }
  { intros. forwards~ [-> [HP He]]: is_path_last_inv Hl.
    clear Hl. destruct He as [He HPy].
    xcf. xpay. credr2l. hsimpl.
    xapps~. credr2l. hsimpl.
    xif.
    { subst y. false. apply (HA x). applys~ (>> tclosure_of_rtclosure_l u).
      applys covariant_rtclosure. 2: applys~ is_path_rtclosure. rel_crush. }
    xapps~. credr2l. hsimpl.
    xvals. xapps~. credr2l. hsimpl.
    xif.
    { subst u. xret. hsimpl ((x, y) :: nil).
      { hsimpl_credits. rewrite le_zarith. rew_list. defer. }
      rew_listx. splits~. repeat constructors~. }
    xapp~. intro l''. hpull. intros ll [Hl'' ?]. hsimpl (ll & (u, y)).
    { rew_list. hsimpl_credits. rewrite le_zarith.
      asserts~ Hll: (0 <= length ll). generalize (length ll) Hll. defer. }
    rew_listx. splits~. applys~ is_path_last. split~. }

  close_cost.
  begin defer assuming a b c. exists (fun n => If 0 <= n then a*n+b else c). split.
  { splits; (intros; repeat case_if~; pols; defer). }
  cleanup_cost.
  { defer?: (0 <= a). intros ? ? ?. do 2 case_if; try math_nia.
    defer?:(c <= b). math_nia. }
  { apply dominated_transitive with (fun n:Z_filterType => a*n+b).
    { apply dominated_ultimately_eq. exists 0. intros. case_if~. }
    dominated. }
  { intros. deferred;intros. case_if~. defer?:(0 <= b). math_nia. defer. }
  end defer. elia.
Qed.

Hint Extern 1 (RegisterSpec list_of_parents) =>
  Provide (provide_specO list_of_parents_spec).

Lemma compute_cycle_spec :
  specZ [cost \in_O (fun n => n)] (
    forall g v w z t G M Ms L I P edge,
    reachable (with_parents P edge) w z ->
    edge z t ->
    reachable (with_rev_parents P edge) t v ->
    (forall z p,
      z \in vertices G ->
      P z = Some p ->
      p \in vertices G) ->
    (forall x, ~ tclosure edge x x) ->
    v \in vertices G -> w \in vertices G ->
    z \in vertices G -> t \in vertices G ->
    app compute_cycle [g v w z t]
      PRE (IsRawGraph g G L M Ms I P)
      POST (fun l => Hexists lpath,
        \$ (- cost (length l)) \*
        IsRawGraph g G L M Ms I P \*
        \[ l = w :: LibList.map snd lpath /\ is_path edge lpath w v ])).
Proof.
  xspecO_refine recursive. introv _M _D _g. introv.
  intros HPwz HEzt HPtv HinG HA Hv Hw Hz Ht.
  xcf. xpay. credr2l. hsimpl.
  assert (reachable (with_parents P (reverse edge)) v t).
  { rewrite -rtclosure_reverse. rel_covariant. }
  xapp~ as ll.
  { intros x Hxx. apply (HA x). applys~ use_tclosure_reverse. }
  intros ltv. rew_list. intros [-> Hltv].
  xrets. xapp~. intros ll. hpull; => lwz [-> Hlwv].
  hsimpl (lwz & (z, t) ++ rev (LibList.map (fun '(x, y) => (y, x)) ltv)).
  { hsimpl_credits. rewrite le_zarith. rew_listx. rewrite !length_map. pols.
    asserts~ HH1: (0 <= length lwz). asserts~ HH2: (0 <= length ltv).
    generalize (length lwz) (length ltv) HH1 HH2. defer. }
  { split.
    { rew_listx. simpl.
      forwards HR: is_path_mapfst_mapsnd Hlwv.
      rewrite -app_cons_l -HR. rew_listx. do 3 fequal.
      rewrite map_rev list_map_compose /compose //=. fequal.
      fequal. apply fun_ext_1. tauto. }
    { rew_listx.
      applys~ (>>is_path_concat z). now applys~ covariant_is_path; rel_crush.
      eapply is_path_cons; auto. apply is_path_reverse.
      applys~ covariant_is_path. rel_crush. } }
  close_cost.
  exists (fun n:Z_filterType => 2 * cost list_of_parents_spec n + 1). split.
  { intros n m ? ?.
    set Cm := cost list_of_parents_spec m.
    set Cn := cost list_of_parents_spec n.
    set Cmn := cost list_of_parents_spec (n + (1 + (1 + m))).
    assert (Cm <= Cmn) by apply~ cost_monotonic.
    assert (Cn <= Cmn) by apply~ cost_monotonic. math_lia. }
  cleanup_cost. monotonic. dominated. intros.
  eauto using cost_nonneg with zarith.
Qed.

End ComputeCycle.

Hint Extern 1 (RegisterSpec list_of_parents) =>
  Provide (provide_specO list_of_parents_spec).
Hint Extern 1 (RegisterSpec compute_cycle) =>
  Provide (provide_specO compute_cycle_spec).

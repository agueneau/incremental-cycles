Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the CF definitions. *)
Require Import Simple_sparse_ml.

From Coq Require Import Reals.
From Coq.micromega Require Import Lra.

From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation.
Require Import ssreflect.
Require Import PolTac.PolTac.
Require Import BigO.elia.
Require Import BigO.CFMLBigO.
Require Import BigO.HeapTactics.
Require Import BigO.BigEnough.

Require Import TLCBuffer LibRelationExtra LibTacticsExtra.
Require Import HeapTacticsExtra.
Require Import GraphModel RawGraph Graph Paths.
Require Import GraphFunctionalInvariants GraphCostInvariants.
Require Import PowerTwoThird SqrtAsymptotics.

Require Import BackwardSearch_proof.
Require Import ForwardSearch_proof.
Require Import ComputeCycle_proof.

Open Scope set_scope.
Open Scope graph_scope.

Section SimpleSparse.

Context `{GM: GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)

Local Ltac auto_tilde ::= eauto with graph maths.
Local Ltac done ::= solve [autos~].

(******************************************************************************)

Lemma path_crossing_edge : forall G L x y,
  levels_pseudotopo G L ->
  L x < L y ->
  reachable (has_edge G) x y ->
  exists z1 z2,
    reachable (has_edge G) x z1 /\
    has_edge G z1 z2 /\
    reachable (has_edge G) z2 y /\
    L z1 < L y /\
    L z2 = L y.
Proof.
  introv LP Lxy.
  lets: ltac_mark. induction 1 using rtclosure_ind_l; gen_until_mark.
  { false. math. }
  { rename z into y. (* ?? *)
    intros z Exz Rzy Hind.
    tests: (L z < L y).
    { forwards~ (z1&z2&?): Hind. unpack.
      exists z1 z2. splits~. apply~ (>>rtclosure_l z). }
    { asserts: (L z = L y). { forwards~: pseudotopo_reachable. }
      clear C.
      exists x z. splits~. apply rtclosure_refl. } }
Qed.

Lemma enough_edges_below1: forall G L new_level x y z,
  x \in vertices G ->
  y \in vertices G ->
  (forall u,
      reachable (has_edge G) x u /\ L u = new_level \/ enough_edges_below G L u) ->
  ~ reachable (has_edge G) x y ->
  L x = new_level ->
  L y = new_level ->
  enough_edges_below (G \+> (y, x)) L z.
Proof.
  introv ? ? HLoR Hxy HLx HLy. forwards~ [(?&?)|?]: HLoR z;[].
  applys~ enough_edges_below_reachable x; [| now applys~ covariant_rtclosure].
  applys~ enough_edges_below_add_edge2.
  forwards~ [(?&?)|?]: HLoR y. false.
Qed.

Lemma enough_edges_below2: forall G L L' new_level x y z,
  x \in vertices G ->
  y \in vertices G ->
  L y <= card (coacc_edges_at_level G L (L y) y) ->
  (forall u, L' u = L u \/ reachable (has_edge G) x u) ->
  (reachable (has_edge G) x z /\ L' z = new_level \/ enough_edges_below G L' z) ->
  ~ reachable (has_edge G) x y ->
  L' x = new_level ->
  L' y + 1 = new_level ->
  enough_edges_below (G \+> (y, x)) L' z.
Proof.
  introv ? ? HEy HLoR HLoRz Hxy HLx HLy. forwards~ [(?&?)|?]: HLoRz;[].
  applys~ enough_edges_below_reachable x; [| now applys~ covariant_rtclosure].
  applys~ enough_edges_below_add_edge3.
  forwards~ [->|?]: HLoR y; [| now false].
  transitivity (card (coacc_edges_at_level G L (L y) y)); autos.
  applys~ card_le_of_incl'. apply coacc_edges_at_level_new_levels.
  intros u ? ?. forwards~ [?|?]: HLoR u. false. forwards~: rtclosure_trans.
Qed.

Hint Resolve Inv_pseudotopo.

Lemma reachable_in_visited_update_parents: forall G M m P P' x y,
  reachable
    (in_visited (BackwardSearch_proof.visited M m)
                (with_rev_parents P (has_edge G))) x y ->
  (forall x : vertex, P' x <> P x -> M x <> m) ->
  reachable (with_rev_parents P' (has_edge G)) x y.
Proof.
  introv HR HPP'.
  pose ltac_mark; induction HR using rtclosure_ind_r; gen_until_mark.
  { apply rtclosure_refl. }
  { intros Hxy. destruct H as [[? ?] [? ?]]. applys~ (>>rtclosure_r y).
    splits~. tests: (P' y = P y). congruence.
    false. forwards~: HPP' y. }
Qed.

Definition Δψ₁ G v w :=
  graph_received (G \+> (v, w)) - graph_received G.

Lemma Δψ₁_nonneg : forall G v w,
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  0 <= Δψ₁ G v w.
Proof.
  intros. rewrite /Δψ₁.
  apply Zle_minus_le_0. rewrite /graph_received vertices_add_edge.
  apply received_mono_l. splits~.
  now rewrite card_edges_add_edge //.
Qed.

Lemma Δψ₁_to_ϕ L : forall G v w,
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  ϕ_cst * Δψ₁ G v w = ϕ (G \+> (v, w)) L - ϕ G L + ϕ_cst * L v.
Proof.
  intros. rewrite /Δψ₁ /ϕ. polf. fequal. rewrite delta_net_add_edge //.
Qed.

Lemma Δψ₁_Lv : forall G L I P v w,
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  Inv G L I P ->
  L v <= Δψ₁ G v w.
Proof.
  intros. rewrite /Δψ₁.
  transitivity (max_level (card (edges G)) (card (vertices G))).
  { applys~ Inv_max_level. }
  { rewrite /graph_received card_edges_add_edge // vertices_add_edge.
    rewrite plus_nat_eq_plus_int //=. apply~ delta_received_add_edge. }
Qed.

Definition get_cycle_spec f cost g G L M Ms I P v w :=
  app f [tt]
    PRE (IsRawGraph g G L M Ms I P)
    POST (fun l => Hexists lpath,
      \$ (- cost (length l)) \*
      IsRawGraph g G L M Ms I P \*
      \[ l = w :: LibList.map snd lpath /\ is_path (has_edge G) lpath w v ]).

Class add_edge_spec_ty := {
  get_cycle_cost : Z_filterType -> Z;
  get_cycle_cost_nonneg : forall n, 0 <= get_cycle_cost n;
  get_cycle_cost_monotonic : monotonic Z.le Z.le get_cycle_cost;
  get_cycle_cost_dominated : dominated Z_filterType get_cycle_cost (fun n => n);
  ψ₁_cst : Z;
  ψ₁_cst_nonneg : 0 <= ψ₁_cst;
  add_edge_raw_spec :
    forall g G v w,
    v \in vertices G ->
    w \in vertices G ->
    ~ has_edge G v w ->
    app add_edge_or_detect_cycle [g v w]
      PRE (IsGraph g G \* \$ (ψ₁_cst * Δψ₁ G v w + ψ₁_cst))
      POST (fun (res: add_edge_result_) =>
        match res with
        | EdgeAdded => IsGraph g (G \+> (v, w))
        | EdgeCreatesCycle get_cycle =>
          Hexists M' Ms' L' I' P', IsRawGraph g G L' M' Ms' I' P' \*
          \[ reachable (has_edge G) w v /\
             get_cycle_spec get_cycle get_cycle_cost
               g G L' M' Ms' I' P' v w
           ]
        end);
}.

Instance add_edge_spec_raw : add_edge_spec_ty.
Proof.
  begin defer assuming compute_cycle_cost ψ₁_cst in _g.
  constructor 1 with compute_cycle_cost ψ₁_cst.
  defer. defer. defer. defer.
  introv Hv Hw Hvw. xcf.
  xchange IsGraph_open. xpull; => L M Ms I P HInv.
  weaken (ψ₁_cst * _ + ψ₁_cst). xpay.
  (* Specification for the [success] local function *)
  (* FIXME?: explicit cost *)
  xfun (fun success => forall M' Ms' L' I' P',
    ~ has_edge G v w ->
    Inv (G \+> (v, w)) L' (I_add_edge L' I' v w) P' ->
    v \notin I' w ->
    app success [tt]
    PRE (\$ (cost raw_add_edge_spec tt +
             2 * cost get_level_spec tt +
             cost add_incoming_spec tt +
             1) \*
         \$ (2 * potential_cst * Δψ₁ G v w) \*
         \$ ϕ G L' \*
         IsRawGraph g G L' M' Ms' I' P')
    POST (fun (res: add_edge_result_) =>
      IsGraph g (G \+> (v, w)) \*
      \[ res = EdgeAdded ])
  ).
  { weaken ((_ + _) + 1). xpay.
    xapp~. xapps~. xapps~. xrets. xseq. xpost (fun (_:unit) => IsGraph g (G \+> (v, w))).
    2: now xrets~.
    unfold I_add_edge in *.
    xif.
    { xapps~. intros _. rewrite (Δψ₁_to_ϕ L') //.
      (* XXX xchange~ IsGraph_close. *)
      xchange_base ltac:(fun tt => hsimpl; hsimpl_cleanup_trysolve) IsGraph_close __.
      now case_if~. now hsimpl_credits.
      hsimpl~. rewrite le_zarith. pols.
      (* note that we throw away some credits: we ask for the variation of the
         maximum potential, but we need the variation of the potential: it may
         be less if the vertex v is at a higher level.
         However we do not need to be that precise, and it would add a dependency
         on L, so we just throw away the extra credits. *)
      lets: ϕ_cst_nonneg. forwards~: Inv_levels L' v. math_nia. }
    { xret.
      (* XXX *)
      xchange_base ltac:(fun tt => hsimpl; hsimpl_cleanup_trysolve) IsGraph_close __. case_if~.
      rewrite (Δψ₁_to_ϕ L') //. hsimpl_credits. hsimpl~. rewrite le_zarith. pols.
      lets: ϕ_cst_nonneg. forwards~: Inv_levels L' v. math_nia. }
    { pols. simp. math_lia. } }
  xapps~. xif.
  { (* v = w *)
    subst w.
    xfun. xrets. now applys~ ϕ_nonneg. split. now apply rtclosure_refl.
    unfold get_cycle_spec. xapp. xpay. credr2l. (* hm... *) hsimpl. xret.
    hsimpl (@List.nil (vertex*vertex)%type). 2: now splits~; constructors~.
    rew_listx. hsimpl_credits. rewrite le_zarith. simp. defer. }
  xapps~. xapps~. xif as CC.
  { (* L w > L v *)
    xapp~.
    { apply~ Inv_add_edge.
      { intro. forwards~: Inv_pseudotopo_reachable w v. math. }
      { intro z. forwards~: Inv_edges_below z. } }
    { rewrites (>> Inv_incoming HInv). intros [??]. math. }
    { hpull;=> ->. hsimpl. } }
  asserts~: (L w <= L v). clear CC.
  xapps~. xapp~ L as bres. now forwards~: Inv_levels L v.
  intros M' Ms' P' (HI & Hres). xmatch.
  { (* BackwardCyclic *)
    xfun. xrets~. applys~ ϕ_nonneg. split.
    { applys~ covariant_rtclosure. rel_crush. }
    { unfold get_cycle_spec. xapp. xpay. credr2l. hsimpl.
      assert (reachable (with_parents P' (reverse (has_edge G))) v w).
      { rewrite -rtclosure_reverse. rel_covariant. }
      xapp~ as ll. now forwards~: Inv_parents_in_graph HI.
      { intros z Hzz. rewrite -tclosure_reverse /reverse in Hzz.
        applys~ Inv_acyclic. }
      intros lcycle. rew_list. intros [-> Hispath].
      xrets. xret. hsimpl (rev (LibList.map (fun '(x, y) => (y, x)) lcycle)).
      { hsimpl_credits. rewrite le_zarith. rew_list. rewrite length_map. simp.
        asserts~ Hlength: (0 <= length lcycle). generalize (length lcycle) Hlength. defer. }
      { split.
        { rewrite map_rev list_map_compose. repeat fequal. apply fun_ext_1. tauto. }
        { forwards~: is_path_reverse Hispath.
          applys~ covariant_is_path. rel_crush. } } } }
  { (* BackwardAcyclic *)
    unpack. xapp.
    { intro. now forwards~: rtclosure_once. }
    { apply~ Inv_add_edge. intro z. forwards~: Inv_edges_below z. }
    { rewrites (>> Inv_incoming HInv). intros [? _]. tauto. }
    { hpull;=> ->. hsimpl~. } }
  { (* BackwardForward *)
    destruct Hres as (Hres1 & Hres2 & Hres3 & Hres4).
    xlet as fres. xapp_no_simpl; try hsimpl; autos~.
    { branches; math. }
    { piggybank: *rhs. }
    xpull. intros L' I' P'' (HL'w & HL' & HPP'). xmatch_no_intros.
    { (* ForwardCyclic *)
      intros z t ->. xfun. xpull. intros (Ht&Hwz&Hzt&HDI). xrets. split.
      { applys~ (>>rtclosure_trans z).
        applys~ covariant_rtclosure. rel_crush.
        applys~ (>>rtclosure_l t).
        applys covariant_rtclosure; cycle 1. applys~ Hres3. rel_crush. }
      { unfold get_cycle_spec. xapp. xpay. credr2l. hsimpl. xapp~.
        { forwards~ [? ?]: Hres3 t. applys~ reachable_in_visited_update_parents. }
        { intros x Hxx. applys~ Inv_acyclic. }
        intros ll. hpull. intros lpath [-> ?]. hsimpl lpath. 2:now splits~.
        hsimpl_credits. rewrite le_zarith. rew_list. rewrite length_map. simp.
        asserts~ HH: (0 <= length lpath). generalize (length lpath) HH. defer. } }
    { (* ForwardCompleted *)
      intros ->. xpull. intros (Rnomark & HI'). xapp. now auto.
      { destruct Hres4 as [(Lwv & Lv & RLv) | (Lv & HEv)].
        { assert (~ reachable (has_edge G) w v).
          { intro Rwv. forwards~ (z1&z2&?): path_crossing_edge L w v.
            unpack. forwards~: Rnomark z1 z2. }
          assert (L' v = new_level).
          { forwards~ [?|?]: HL' v. false. }
          apply~ Inv_add_edge. intro. applys~ enough_edges_below1. apply HI'. }
        { assert (~ reachable (has_edge G) w v).
          { intro Rwv. forwards~ [|(z&?&?)]: rtclosure_inv_r Rwv.
            forwards~: Rnomark z v. forwards~: Inv_pseudotopo L z v. }
          assert (L' v + 1 = new_level).
          { forwards~ [?|?]: HL' v. false. }
          apply~ Inv_add_edge. intro. applys~ enough_edges_below2. apply HI'. } }
      { rewrites (>> Inv_incoming HI'). intros [? _]. tauto. }
      { hpull;=> ->; hsimpl. } } }

  deferred?: (0 <= ψ₁_cst). forwards~: Δψ₁_nonneg v w.
  rewrite (Δψ₁_Lv v w) //. cancel (Δψ₁ G v w). 1,2: defer.
  end defer.
  begin defer assuming a b c. defer?: (0 <= a). defer?:(0 <= b). defer?:(0 <= c).
  exists (fun n:Z_filterType =>
    a * cost list_of_parents_spec n + b * cost compute_cycle_spec n + c).
  (* fixme: make elia able to handle this *)
  exists_big k Z.
  set Cl := cost list_of_parents_spec. set Cc := cost compute_cycle_spec.
  assert (Hl: forall n, 0 <= Cl n) by applys~ cost_nonneg.
  assert (Hc: forall n, 0 <= Cc n) by applys~ cost_nonneg.
  repeat split; try match goal with |- context [ k ] => big end.
  ultimately_greater. unfold Cl; unfold Cc; monotonic. unfold Cl; unfold Cc; dominated.
  { rewrite Z.add_0_r. cancel (Cl 1). cancel (Cc 1). defer. }
  { intros. rewrite (_:Cl z <= Cl (1+z)). 2: now applys~ cost_monotonic.
    cancel (Cl (1+z)). defer. cancel (Cc (1+z)). deferred; math. }
  { intros. cancel (Cc (1+z)). defer. cancel (Cl (1+z)). deferred; math. }
  close. end defer. elia.
Qed.

Hint Resolve ψ₁_cst_nonneg.

Definition Δψ₂ G v :=
  graph_received (G \++ v) - graph_received G.

Lemma Δψ₂_nonneg : forall G v, 0 <= Δψ₂ G v.
Proof.
  intros. rewrite /Δψ₂. apply Zle_minus_le_0.
  rewrite /graph_received edges_add_vertex.
  apply received_mono_r. math. rewrite vertices_add_vertex.
  tests~: (v \in vertices G).
  { apply Z.eq_le_incl. do 2 fequal. rew_set. intros. rew_set.
    split~. intros [?|?]; subst; auto. }
  { rewrite card_disjoint_union_single //. apply finite_graph. }
Qed.

Lemma Δψ₂_ϕ L : forall G v,
  ϕ_cst * Δψ₂ G v = ϕ (G \++ v) L - ϕ G L.
Proof.
  intros. rewrite /Δψ₂ /ϕ. polf. fequal. rewrite delta_net_add_vertex //.
Qed.

Class add_vertex_spec_ty := {
  ψ₂_cst : Z;
  ψ₂_cst_nonneg : 0 <= ψ₂_cst;
  add_vertex_raw_spec :
    forall g G v,
    app add_vertex [g v]
      PRE (IsGraph g G \* IsNewVertex v \* \$ (ψ₂_cst * Δψ₂ G v + ψ₂_cst))
      POST (fun (_:unit) => IsGraph g (G \++ v));
}.

Instance add_vertex_spec_raw : add_vertex_spec_ty.
Proof.
  begin defer assuming ψ₂_cst in _g. exists ψ₂_cst. defer.
  intros *. xcf. xchange IsGraph_open. xpull;=> L M Ms I P HInv.
  weaken (ψ₂_cst * _ + _). xpay.
  xapp_no_simpl. { hsimpl. piggybank_split_for_later. piggybank: *rhs. }
  { intros _.
    xchange IsRawGraph_forget_parent.
    { instantiate (1 := v). rewrite vertices_add_vertex. rew_set. tauto. }
    apply pred_incl_refl. (* FIXME? *)
    xchange IsGraph_close.
    { applys~ Inv_add_vertex. fupdate_case. }
    { piggybank: *. apply Z.le_sub_le_add_l. rewrite -Δψ₂_ϕ. piggybank: *done. }
    { hsimpl. } }

  deferred; intros.
  forwards~: Δψ₂_nonneg G v. cancel (Δψ₂ G v); defer.
  end defer. elia.
Qed.

Hint Resolve ψ₂_cst_nonneg.

Definition ψ_cst :=
  Z.max ψ₁_cst ψ₂_cst.

Lemma ψ_cst_nonneg : 0 <= ψ_cst.
Proof. rewrite /ψ_cst. eauto with zarith. Qed.
Hint Resolve ψ_cst_nonneg.

Definition ψ (m n : Z) :=
  ψ_cst * (received m n + m + n).

Lemma ψ_nonneg : forall m n,
  0 <= m -> 0 <= n ->
  0 <= ψ m n.
Proof.
  intros. unfold ψ, ψ_cst.
  eauto using received_nonneg with zarith.
Qed.

Lemma ψ_mono : forall m n m' n',
  0 <= m <= m' ->
  n <= n' ->
  ψ m n <= ψ m' n'.
Proof.
  intros.
  transitivity (ψ m' n); unfold ψ;
    eauto using received_mono_l, received_mono_r with zarith.
Qed.

Theorem add_edge_spec : forall g G v w,
  v \in vertices G ->
  w \in vertices G ->
  ~ has_edge G v w ->
  app add_edge_or_detect_cycle [g v w]
    PRE (IsGraph g G \*
         \$ (ψ (card (edges G) + 1) (card (vertices G)) -
             ψ (card (edges G)) (card (vertices G))))
    POST (fun (res: add_edge_result_) =>
      match res with
      | EdgeAdded => IsGraph g (G \+> (v, w))
      | EdgeCreatesCycle get_cycle =>
        Hexists M' Ms' L' I' P', IsRawGraph g G L' M' Ms' I' P' \*
        \[ reachable (has_edge G) w v /\
           get_cycle_spec get_cycle get_cycle_cost
             g G L' M' Ms' I' P' v w ]
      end).
Proof.
  intros. xapply~ add_edge_raw_spec. hsimpl_credits.
  rewrite le_zarith -/ψ₁_cst.
  assert (Hψ: ψ₁_cst <= ψ_cst). { unfold ψ_cst. math_lia. }
  rewrite /Δψ₁ /ψ /graph_received. polf. pols.
  apply Zle_minus_le_0. rewrite vertices_add_edge.
  rewrite card_edges_add_edge // plus_nat_eq_plus_int.
  rewrite (_: nat_to_Z (1%nat) = 1) //.
  apply~ Zmult_le_compat. apply~ Z.add_nonneg_nonneg.
  apply Zle_minus_le_0. apply~ received_mono_l.
Qed.

Theorem add_vertex_spec : forall g G v,
  app add_vertex [g v]
    PRE (IsGraph g G \* IsNewVertex v \*
         \$ (ψ (card (edges G)) (card (vertices G) + 1) -
             ψ (card (edges G)) (card (vertices G))))
    POST (fun (_:unit) => IsGraph g (G \++ v)).
Proof.
  intros. (* xapply~ add_vertex_raw_spec. *) (*XXX??*)
  eapply local_frame_gc; [ xlocal | sapply add_vertex_raw_spec | | xok].
  hsimpl_credits. rewrite -/ψ₂_cst le_zarith.
  assert (Hψ: ψ₂_cst <= ψ_cst). { unfold ψ_cst. math_lia. }
  rewrite /Δψ₂ /ψ /graph_received edges_add_vertex.
  forwards: card_vertices_add_vertex G v.
  forwards: received_mono_r (card (edges G))
            (card (vertices (G \++ v))) (card (vertices G) + 1); [math..|].
  polf. pols. apply Zle_minus_le_0. etransitivity.
  { apply~ Zmult_le_compat_l. apply~ Zplus_le_compat_r. apply~ Zplus_le_compat_r. }
  { apply~ Zmult_le_compat. apply~ Z.add_nonneg_nonneg.
    apply~ Zle_minus_le_0. apply~ received_mono_r. }
Qed.

Lemma dominated_mul_1 A B f g :
  dominated (product_filterType A B)
    (fun '(_, _) => 1)
    (fun '(m, n) => g m n) ->
  dominated (product_filterType A B)
    (fun '(m, n) => f m n)
    (fun '(m, n) => f m n * g m n).
Proof.
  intros. eapply dominated_transitive with (fun '(m, n) => f m n * 1).
  now dominated. apply_nary dominated_mul_nary. reflexivity. assumption.
Qed.

Lemma limit_Int_part :
  limit R_filterType Z_filterType Int_part.
Proof.
  rewrite limitP. intros P HP.
  rewrite ZP in HP. destruct HP as [n0 HP].
  rewrite RP. exists (IZR n0 + 1)%R. intros x Hx.
  apply HP, le_IZR. forwards: Int_part_lower x. lra.
Qed.

Lemma limit_Rpower_l : forall p,
  (0 < p)%R ->
  limit R_filterType R_filterType (fun (x:R) => Rpower x p).
Proof.
  intros p Hp. rewrite limitP. intros P HP.
  rewrite RP in HP. destruct HP as [x0 HP].
  rewrite RP.
  lets Hx1 : (Rmax_r 1 x0). lets Hx1' : (Rmax_l 1 x0).
  set (x1 := Rmax 1 x0) in *.
  exists (Rpower x1 (1/p)). intros x Hx. apply HP.
  apply Rle_trans with x1; auto.
  forwards HH: Rle_Rpower_l (Rpower x1 (1/p)) x p. lra.
  { split; try lra. unfold Rpower. apply exp_pos. }
  rewrite Rpower_mult in HH.
  rewrite /Rdiv Rmult_1_l Rinv_l in HH. lra. rewrite Rpower_1 in HH. lra.
  auto.
Qed.

Lemma limit_IZR :
  limit Z_filterType R_filterType IZR.
Proof.
  rewrite limitP. intros P HP.
  rewrite RP in HP. destruct HP as [x0 HP].
  rewrite ZP. exists (Int_part x0 + 1). intros n Hn.
  apply HP. apply IZR_le in Hn. rewrite plus_IZR in Hn.
  forwards: Int_part_lower x0. lra.
Qed.

Lemma dominated_1_sqrt_pow23 :
  dominated (product_filterType Z_filterType Z_filterType)
    (fun '(m, _) => 1)
    (fun '(m, n) => Z.min (Z.sqrt_up m) (Int_part (Rpower (IZR n) (2/3)))).
Proof.
  apply_nary dominated_min_distr_nary.
  { apply_nary dominated_cst_limit_nary. limit. }
  { limit. apply_nary dominated_cst_limit_nary.
    limit. eapply limit_comp with R_filterType; [| apply limit_Int_part].
    eapply limit_comp with (B:=R_filterType) (g := fun (x:R) => Rpower x (2/3)).
    2: now apply limit_Rpower_l; lra.
    apply limit_IZR. }
Qed.

Lemma Rpower_nonneg: forall (x y: R),
  (0 <= Rpower x y)%R.
Proof. intros. unfold Rpower. apply Rlt_le, exp_pos. Qed.

Lemma dominated_product_lift2 A B f g :
  dominated B f g ->
  dominated (product_filterType A B) (fun '(_, x) => f x) (fun '(_, x) => g x).
Proof.
  intros [c U]. exists c.
  (* FIXME? apply ultimately_lift2. *)
  eapply filter_closed_under_inclusion. apply ultimately_lift2. apply U.
  intros [? ?]. auto.
Qed.

Lemma dominated_max_level :
  dominated
    (product_filterType Z_filterType Z_filterType)
    (fun '(m, n) => max_level m n)
    (fun '(m, n) => Z.min (Z.sqrt_up m) (Int_part (Rpower (IZR n) (2/3)))).
Proof.
  unfold max_level.
  apply_nary dominated_sum_distr_nary; [| apply dominated_1_sqrt_pow23].
  apply_nary dominated_min_nary;
    [ apply filter_universe_alt; intros [? ?] .. | | ];
    try apply Z.sqrt_up_nonneg;
    [ apply Int_part_nonneg, power_two_third_nonneg
    | apply Int_part_nonneg, Rpower_nonneg | ..].
  { apply_nary dominated_sqrt_up_nary.
    { apply ultimately_lift1. ultimately_greater. }
    { dominated. } }
  { apply dominated_product_lift2. unfold power_two_third.
    transitivity (fun x : Z_filterType => Int_part (Rpower (3/2 * IZR x) (2/3))).
    { apply dominated_ultimately_eq. rewrite ZP. exists 1.
      intros n Hn. case_if; [| trivial]. false. forwards: IZR_le Hn. lra. }
    transitivity (fun x : Z_filterType => 2 * (Int_part (Rpower (IZR x) (2/3)) + 1)).
    { apply dominated_ultimately_le. exists 1=>n Hn. rewrite !Z.abs_eq.
      { apply Int_part_nonneg, Rpower_nonneg. }
      { assert (0 <= Int_part (Rpower (IZR n) (2 / 3))); [|math].
        apply Int_part_nonneg, Rpower_nonneg. }
      { apply le_IZR. eapply Rle_trans; [apply Int_part_upper|].
        rewrite mult_IZR plus_IZR -[Rpower (_ * _) _]Rmult_1_l
               -[X in (X * Rpower _ _)%R](Rinv_r 2) // (Rmult_assoc 2).
        apply Rmult_le_compat_l; [lra|].
        rewrite -[(/2 * _)%R]Rplus_0_r -(Rplus_opp_l 1) -Rplus_assoc.
        eapply Rplus_le_compat_r, Rlt_le, Rle_lt_trans, Int_part_lower.
        apply Rplus_le_compat_r.
        rewrite -Rpower_mult_distr; [lra|forwards~: IZR_le; lra|].
        rewrite -Rmult_assoc -[X in (_ <= X)%R]Rmult_1_l.
        apply Rmult_le_compat_r; [apply Rpower_nonneg|].
        eapply (Rmult_le_reg_l 2); [lra|].
        rewrite -Rmult_assoc Rinv_r // Rmult_1_l Rmult_1_r.
        apply (Rle_trans _ (Rpower (3/2) 1)); [|rewrite Rpower_1; lra].
        apply Rle_Rpower; lra. } }
    { apply dominated_mul_cst_l_1, dominated_sum_distr, dominated_cst_limit; [dominated|].
      unshelve eapply (limit_comp (limit_comp limit_IZR (limit_Rpower_l _))
                                  limit_Int_part); lra. } }
Qed.

Lemma dominated_received :
  dominated
    (product_filterType Z_filterType Z_filterType)
    (fun '(m, n) => received m n)
    (fun '(m, n) => m * Z.min (Z.sqrt_up m) (Int_part (Rpower (IZR n) (2/3)))).
Proof.
  unfold received. dominated.
  apply_nary dominated_sum_distr_nary.
  apply dominated_max_level. apply dominated_1_sqrt_pow23.
Qed.

Theorem dominated_ψ :
  dominated
    (product_filterType Z_filterType Z_filterType)
    (fun '(m, n) => ψ m n)
    (fun '(m, n) => m * Z.min (Z.sqrt_up m) (Int_part (Rpower (IZR n) (2/3))) + n).
Proof.
  unfold ψ.
  transitivity (fun '(m, n) => ψ_cst * (received m n + m) + ψ_cst * n).
  { apply dominated_eq. intros [m n]. math_lia. }
  apply_nary dominated_sum_nary; try solve [dominated].
  { rewrite -/(product_filterType Z_filterType Z_filterType) productP.
    do 2 exists (fun n => 0 <= n). repeat split; try solve [ultimately_greater].
    intros x y ? ?. forwards: Z.sqrt_up_nonneg x.
    forwards: Int_part_nonneg (Rpower (IZR y) (2/3)).
    now apply Rpower_nonneg. math_nia. }
  { rewrite -/(product_filterType Z_filterType Z_filterType) productP.
    do 2 exists (fun n => 0 <= n). repeat split; ultimately_greater. }
  { dominated.
    apply_nary dominated_sum_distr_nary;
      [| apply dominated_mul_1, dominated_1_sqrt_pow23 ].
    apply dominated_received. }
Qed.

End SimpleSparse.

Existing Instance add_edge_spec_raw.

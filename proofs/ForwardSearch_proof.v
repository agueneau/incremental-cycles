Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the CF definitions. *)
Require Import Simple_sparse_ml.

From TLC Require Import
  LibSet LibFun LibListZ LibListSub
  LibWf LibIntTactics LibRelation.

Require Import ssreflect.
Require Import TLCBuffer LibRelationExtra LibTacticsExtra.
Require Import GraphModel RawGraph Paths.
Require Import GraphFunctionalInvariants GraphCostInvariants.
Require Import IFold_proof.

Require Import BigO.CFMLBigO.
Require Import BigO.BigEnough.
Require Import PolTac.PolTac.

Require Traversal.

Open Scope set_scope.
Open Scope graph_scope.

Section ForwardSearch.

Context `{GM: GraphModel vertex}.

Implicit Types x y z : vertex.
Implicit Types G : graph vertex.
Implicit Types L : vertex -> int. (* levels *)
Implicit Types M : vertex -> mark. (* marks *)
Implicit Types Ms : set mark. (* valid marks *)
Implicit Types I : vertex -> set vertex. (* incoming vertices *)
Implicit Types P : vertex -> option vertex.

Local Ltac auto_tilde ::= eauto with graph maths.
Local Ltac done ::= solve [autos~].

(******************************************************************************)

Definition edge_to_upd G L0 new_level :=
  fun x y => has_edge G x y /\ L0 x < new_level.

Hint Unfold edge_to_upd : rel_crush.
Hint Extern 1 (edge_to_upd _ _ _ _ _) => now (unfold edge_to_upd; splits~).

Definition incoming_partial G L (stack: list vertex) (processing : list (vertex * vertex)) I y :=
  I y = \set{x | incoming_level G L y x /\ ~ mem x stack /\ ~ mem (x, y) processing }.

Record invf
       G L0 L M I0 I P0 P (new_level: int) (source: vertex) (target: set vertex)
       (visited: set vertex) (visited_edge: set (vertex * vertex))
       (stack: list vertex) (processing: list (vertex * vertex))
  :=
{
  invf_trav:
    Traversal.Inv
      G P0 P (edge_to_upd G L0 new_level)
      source target
      visited visited_edge
      stack processing;

  invf_visited_level x:
    x \in visited -> new_level <= L x;

  invf_notvisited_level x:
    ~ x \in visited -> L x = L0 x;

  invf_level_increases x:
        L x = L0 x
    \/ (L0 x < L x /\ L x = new_level);

  invf_stack: Forall (fun x =>
       L x = new_level
    /\ L0 x < new_level
  ) stack;

  invf_level_source :
    L source = new_level;

  invf_levels_pseudotopo:
    levels_pseudotopo G L0;

  invf_incoming0:
    I0 = incoming_level G L0;

  invf_incoming_notvisited y:
    ~ y \in visited ->
    I y = I0 y;

  invf_incoming_level y:
    L y = new_level ->
    incoming_partial G L stack processing I y;

  invf_incoming_above_level y:
    new_level < L0 y ->
    I0 y = I y;

  invf_edges_below x :
       (reachable (has_edge G) source x
        /\ L x <= new_level)
    \/ enough_edges_below G L x;

  invf_acyclic :
    acyclic G;

  invf_parents0 :
    parents_in_graph G P0;
}.

Hint Resolve invf_trav.

(******************************************************************************)
(* Auxiliary definitions for the termination argument: the function terminates
   because it consumes the stack, or decreases the number of vertices below the new
   level (and there is a finite number of vertices). *)

Definition vertices_below G L (l: int): set vertex :=
  \set{ x \in vertices G | L x < l }.

Lemma finite_vertices_below : forall G L l, finite (vertices_below G L l).
Proof.
  eauto using finite_incl, comprehension_incl, finite_graph.
Qed.

Lemma vertices_below_fupdate : forall G L x l,
  L x < l ->
  vertices_below G (fupdate L x l) l = vertices_below G L l \-- x.
Proof.
  intros. unfold vertices_below. rew_set. intro z.
  splits; intros HH; rew_set in *.
  - rewrite fupdate_eq in HH. case_if~. false. math.
  - unpack; fupdate_case.
Qed.

(******************************************************************************)
(* Auxiliary definitions for the complexity analysis in the case where the
   algorithm detects a cycle. *)

Definition edges_unvisited G L new_level :=
  set_st (fun '(x, y) => has_edge G x y /\ L x < new_level).

Lemma edges_unvisited_edges: forall G L new_level,
  edges_unvisited G L new_level \c edges G.
Proof.
  intros. unfold edges_unvisited. rew_set. intros [? ?]. rew_set. tauto.
Qed.

(* TODO: cleanup this mess *)
Lemma edges_unvisited_fupdate : forall G L x new_level,
  L x < new_level ->
  card (edges_unvisited G (fupdate L x new_level) new_level) + card (out_edges G x) =
  card (edges_unvisited G L new_level).
Proof.
  introv HLx.
  rewrite -plus_nat_eq_plus_int.
  apply eq_int_of_eq_nat.
  assert (card (out_edges G x) = card (set_st (fun '(x', y) => has_edge G x' y /\ x = x'))) as ->.
  { etransitivity.
    { symmetry. apply injective_image_card with (f := (fun y => (x, y))).
      { congruence. } eapply finite_incl; [| eapply (finite_graph G)]. unfold out_edges. rew_set. intro.
      rew_set. autos~. }
    { fequal. apply double_inclusion.
      { rew_set. intro. rew_set. unfold LibFun.image. rew_set. intros [? [? ?]]. subst. autos~. }
      { rew_set. intros [? ?]. rew_set. unfold LibFun.image. rew_set. intros [? ?]. subst.
        eexists. splits~. autos~. } } }

  rewrite -card_disjoint_union.
  { eapply finite_incl. apply edges_unvisited_edges. apply finite_graph_edges. }
  { eapply finite_incl; [| eapply (finite_graph_edges G)]. unfold edges_unvisited. rew_set. intros [? ?].
    rew_set. tauto. }
  { unfold edges_unvisited. rew_set. intros [? ?]. rew_set. intros [? ?] [? ?]. subst.
    match goal with h: _ |- _ => fupdate_case_in h end. math. }

  fequal. apply double_inclusion.
  { rew_set. intros [? ?]. unfold edges_unvisited. rew_set. intros H.
    fupdate_case_in H; branches; unpack; subst; splits~. }
  { rew_set. intros [? ?]. unfold edges_unvisited. rew_set. intros [? ?].
    fupdate_case. }
Qed.

Lemma edges_unvisited_fupdate_sub : forall G L x new_level,
  L x < new_level ->
  nat_to_Z (card (edges_unvisited G (fupdate L x new_level) new_level)) =
  card (edges_unvisited G L new_level) - card (out_edges G x).
Proof. intros. forwards~: edges_unvisited_fupdate G L x new_level. Qed.

(******************************************************************************)

Hint Resolve noduplicates_cons_tail noduplicates_cons_nomem.

Lemma incoming_partial_unrelated_I : forall G L I stack processing y y' I'y',
  y <> y' ->
  incoming_partial G L stack processing I y ->
  incoming_partial G L stack processing (fupdate I y' I'y') y.
Proof. unfold incoming_partial. introv ? HI. fupdate_case. Qed.

Lemma incoming_partial_unrelated_ve : forall G L I stack processing x y y',
  y <> y' ->
  incoming_partial G L stack ((x, y')::processing) I y ->
  incoming_partial G L stack processing I y.
Proof.
  unfold incoming_partial. introv ? ->. rew_set=>x'. rew_set.
  split; intros [?[? Hpro]]; splits~. contradict Hpro. inversion* Hpro.
Qed.

Lemma incoming_partial_add_edge : forall G L I stack processing x y,
  has_edge G x y ->
  L x = L y ->
  ~ mem x stack ->
  ~ mem (x, y) processing ->
  incoming_partial G L stack ((x,y)::processing) I y ->
  incoming_partial G L stack processing (fupdate_add I y x) y.
Proof.
  unfold incoming_partial. introv ? ? ? ? HI. unfold fupdate_add. fupdate_case.
  rewrite HI. rew_set=>x'. rew_set. split.
  { intros [?| ->]; splits*. split~. }
  { tests~: (x = x'). left. splits*. inversion 1. congruence. jauto. }
Qed.

Lemma incoming_partial_bump_unrelated_level : forall G L I new_level stack processing y y',
  L y < new_level ->
  new_level <= L y' ->
  incoming_partial G L stack processing I y' ->
  incoming_partial G (fupdate L y new_level) (y::stack) processing I y'.
Proof.
  unfold incoming_partial, incoming_level. introv ? ? ->. rew_set=>x'.
  rew_set. assert (y <> y') by (intros ->; math). repeat fupdate_case.
  { subst. split.
    { intros [[_ ?]_]. math. }
    { intros [_ [Hy _]]. destruct~ Hy. } }
  { rewrite* mem_cons_eq. }
Qed.

Lemma incoming_partial_pop : forall G L I stack x y ys,
  to_set ys = out_edges G x ->
  incoming_partial G L (x::stack) nil I y ->
  incoming_partial G L stack (mkedges x ys) I y.
Proof.
  unfold incoming_partial, incoming_level. introv ->. rew_set=>x'. rew_set.
  split; intros [[??][Hstack Hpro]].
  { splits~. apply not_mem_mkedges. contradict Hstack. subst. left. }
  { splits~; [|exact (@mem_nil_inv _ _)]. contradict Hstack.
    inversion~ Hstack. subst. destruct Hpro. applys~ mem_mkedges. }
Qed.

Lemma enough_edges_below_bump_level_notreachable: forall G L source new_level y z,
  L y < new_level ->
  reachable (has_edge G) source y ->
  ~ reachable (has_edge G) source z ->
  enough_edges_below G L z ->
  enough_edges_below G (fupdate L y new_level) z.
Proof.
  introv HLy HRy HRz HEz. unfold enough_edges_below in *.
  fupdate_case.
  transitivity (card (coacc_edges_at_level G L (L z - 1) z)); autos.
  applys~ card_le_of_incl'. applys~ coacc_edges_at_level_new_levels.
  intros u Huz Hu. fupdate_case. substs~. false.
  forwards~: rtclosure_trans y source z.
Qed.

Lemma enough_edges_below_bump_level_abovelevel: forall G L new_level y z,
  L y < new_level ->
  new_level < L z ->
  enough_edges_below G L z ->
  enough_edges_below G (fupdate L y new_level) z.
Proof.
  introv HLy HLz HEz. unfold enough_edges_below in *.
  fupdate_case; [ now subst~ |].
  transitivity (card (coacc_edges_at_level G L (L z - 1) z)); autos.
  applys~ card_le_of_incl'. applys~ coacc_edges_at_level_new_levels.
  intros u Huz Hu. fupdate_case. substs~.
Qed.

Lemma invf_parents: forall G L0 L M I0 I P0 P new_level source target visited visited_edge stack processing,
  parents_in_graph G P0 ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge stack processing ->
  parents_in_graph G P.
Proof.
  introv HP Hinv. unfolds. intros x y Hy Hyx.
  forwards [?|?]: Traversal.Inv_parents_modified Hinv y.
  { applys~ HP. congruence. }
  { forwards~ [? ?]: Traversal.Inv_parent_edge Hinv. }
Qed.

Lemma invf_acyclic_parent_noreach :
  forall G L0 L M I0 I P0 P new_level source target visited visited_edge stack processing x y,
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge stack processing ->
  x \in visited ->
  has_edge G x y ->
  acyclic G ->
  ~ reachable (with_parents P (has_edge G)) y x.
Proof.
  introv HI ? ? HG. intro HR.
  assert (tclosure (has_edge G) y y).
  { apply tclosure_of_rtclosure_l with x; autos~.
    applys~ covariant_rtclosure. rel_crush. }
  unfold acyclic in HG; autos~.
Qed.

Lemma invf_already_at_level :
  forall G L0 L M I0 I P0 P new_level source target visited visited_edge stack x y ys,
  x \in visited ->
  ~ y \in target ->
  L0 x < new_level ->
  has_edge G x y ->
  L y = new_level ->
  acyclic G ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge
       stack (mkedges x (y::ys)) ->
  invf G L0 L M I0 (fupdate_add I y x) P0 (fupdate P y (Some x))
    new_level source target
    (visited \u \{y}) (visited_edge \u \{(x,y)}) stack (mkedges x ys).
Proof.
  introv Hx HTy HL0x Hxy Hy HG HI.
  constructor; try now apply HI.
  { applys~ Traversal.Inv_visit_edge_aux_drop'. intro HR.
    applys~ invf_acyclic_parent_noreach. now applys~ covariant_rtclosure; rel_crush.
    intros ? y' [? ?]. forwards~: invf_notvisited_level HI y. math. }
  { intros z Hz. rew_set in Hz. branches; [ now applys~ HI | subst z ].
    math. }
  { intros z Hz. rew_set in Hz. rew_logic in Hz. unpack.
    applys~ HI. }
  { intro y'. rew_set. rew_logic. unfold fupdate_add. fupdate_case. now tauto.
    intros (? & _). forwards~ ->: invf_incoming_notvisited HI y'. tauto. }
  { intros y'. rew_set. intros Hy'.
    forwards~: invf_incoming_level HI y'. tests: (y' = y).
    { applys~ incoming_partial_add_edge.
      { forwards~ [?|?]: invf_level_increases HI x.
        forwards~: invf_visited_level HI x. (* lemma instead of the 2 forwards? *) }
      { forwards~ : Traversal.Inv_processing_nodup HI. } }
    { applys~ incoming_partial_unrelated_ve incoming_partial_unrelated_I. } }
  { intros y' Hy'. unfold fupdate_add. fupdate_case; [|apply~ HI]. subst y'.
    forwards~ : invf_level_increases HI y. by intuition. }
Qed.

Lemma invf_bump_lower_level :
  forall G L0 L M I0 I P0 P new_level source target visited visited_edge stack x y ys,
  x \in visited ->
  ~ y \in target ->
  L0 x < new_level ->
  has_edge G x y ->
  L y < new_level ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge stack ((x,y) :: ys) ->
  invf G L0 (fupdate L y new_level) M I0 (fupdate I y \{x}) P0 (fupdate P y (Some x))
    new_level source target
    (visited \u \{y}) (visited_edge \u \{(x,y)}) (y :: stack) ys.
Proof.
  introv Hx HTy HL0x Hxy Hy HI.
  constructor; try solve[apply HI].
  { applys~ Traversal.Inv_visit_edge_aux_nodrop. intros ?.
    forwards~: invf_visited_level. math. }
  { intros z Hz. rew_set in Hz. fupdate_case.
    branches; [now applys~ HI | subst z]. false~. }
  { intros z Hz. rew_set in Hz. rew_logic in Hz. unpack.
    fupdate_case. applys~ HI. }
  { intros z. fupdate_case.
    { subst z. forwards~ [<-|?]: invf_level_increases HI y. }
    { applys~ HI. } }
  { rew_listx. repeat splits~.
    { fupdate_case. }
    { forwards~ [?|?]: invf_level_increases HI y. }
    { eapply Forall_pred_incl.
      applys Forall_combine (Traversal.Inv_stack (invf_trav HI)) (invf_stack HI).
      intros ? ?. unpack. fupdate_case. } }
  { fupdate_case. apply~ HI. }
  { intro y'. rew_set. rew_logic. intros (Hy'&?). intro z.
    fupdate_case. forwards~ ->: invf_incoming_notvisited HI y'. tauto. }
  { intro y'. fupdate_case.
    { subst y'=>_. unfold incoming_partial, incoming_level. rew_set=>x'.
      fupdate_case. rew_set.
      assert (L x = new_level).
      { forwards~ : invf_visited_level HI x.
        forwards~ [|] : invf_level_increases HI x. }
      assert (x <> y) by (intros ->; math). split.
      { intros ->. split~; split~.
        { repeat fupdate_case. }
        { forwards* [?|[?|[_[_ Hxstack]]]] : Traversal.Inv_visited_edge HI x y.
          contradict Hxstack. inversion* Hxstack. }
        { forwards~ : Traversal.Inv_processing_nodup HI. } }
      { intros ([? HLx']&Hx'stack&?). tests: (visited x').
        { forwards~ [(?&_&_)|[(?&_&_)|(Hys&_&_)]] : Traversal.Inv_visited_edge HI x' y.
          { split~. destruct~ (invf_level_increases HI x').
            forwards~ : invf_levels_pseudotopo HI x' y.
            destruct (invf_level_increases HI y); math. }
          { forwards~ [_ ?] : Traversal.Inv_visited_edges_visited HI x' y.
            forwards~ : invf_visited_level HI y. math. }
          { destruct~ Hx'stack. }
          { inversion* Hys. } }
        { rewrite fupdate_same fupdate_neq in HLx'.
          { contradict Hx'stack. subst~. }
          forwards~ : invf_notvisited_level HI x'.
          forwards~ : invf_levels_pseudotopo HI x' y.
          destruct (invf_level_increases HI y); math. } } (* lemma *) }
    { intros Hy'.
      forwards~ : invf_incoming_level HI y'.
      applys~ incoming_partial_unrelated_I.
      applys~ incoming_partial_unrelated_ve.
      applys~ incoming_partial_bump_unrelated_level. } }
  { intros y' Hy'. forwards~ -> : invf_incoming_above_level.
    rewrite fupdate_neq //. intros ->.
    forwards~ [?|?]: invf_level_increases HI y; math. }
  { intros z. forwards: invf_edges_below HI z.
    destruct (or_inv_classic_l H) as [(?&?)| (H1 & H2)]; clear H.
    { left. splits~. fupdate_case. }
    { rew_logic in H1. branches H1.
      { right. applys~ enough_edges_below_bump_level_notreachable.
        applys~ rtclosure_r x.
        forwards~: Traversal.Inv_visited_reachable HI x.
        applys~ covariant_rtclosure. rel_crush. }
      { fupdate_case. { subst. false~. }
        right. applys~ enough_edges_below_bump_level_abovelevel. } } }
Qed.

Lemma invf_skip_higher_level :
  forall G L0 L M I0 I P0 P new_level source target visited visited_edge stack x y ys,
  x \in visited ->
  ~ y \in target ->
  L0 x < new_level ->
  acyclic G ->
  has_edge G x y ->
  new_level < L y ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge stack ((x,y) :: ys) ->
  invf G L0 L M I0 I P0 (fupdate P y (Some x)) new_level source target (visited \u \{y}) (visited_edge \u \{(x,y)}) stack ys.
Proof.
  introv Hx HTy HL0x HG Hxy Hy HI.
  constructor; try now apply HI.
  { applys~ Traversal.Inv_visit_edge_aux_drop'. intro HR.
    applys~ invf_acyclic_parent_noreach. now applys~ covariant_rtclosure; rel_crush.
    { intros ? y' [? ?]. forwards~: invf_notvisited_level HI y. math. } }
  { intros z Hz. rew_set in Hz. branches; [now applys~ HI | subst z].
    math. }
  { intros z Hz. rew_set in Hz. rew_logic in Hz. unpack. applys~ HI. }
  { intro y'. rew_set. rew_logic. intros (?&?). intro z.
    forwards~ ->: invf_incoming_notvisited HI y'. tauto. }
  { intro y'. rew_set. intro Hy'.
    applys~ incoming_partial_unrelated_ve x y.
    { intros ->. math. }
    { applys~ invf_incoming_level. } }
Qed.

Lemma invf_stack_pop :
  forall G L0 L M I0 I P0 P new_level source target visited visited_edge stack x ys,
  to_set ys = out_edges G x ->
  noduplicates ys ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge (x :: stack) nil ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge stack (mkedges x ys).
Proof.
  introv Hys NDys HI.
  constructor; try now apply HI.
  { applys~ Traversal.Inv_stack_pop.
    rewrite Hys. rew_set. intro y.
    (* XX *)
    enough (y \in out_edges G x <-> edge_to_upd G L0 new_level x y) by assumption.
    unfold edge_to_upd. rewrite out_edges_has_edge. split; intro H; try tauto.
    splits~. forwards~ [? ?]: Forall_cons_inv (invf_stack HI). }
  { forwards~: invf_stack HI. rew_listx in *. tauto. }
  { intros. applys~ incoming_partial_pop HI. }
Qed.

Lemma invf_complete0 : forall G L0 L M I0 I P0 P new_level source target visited visited_edge,
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge nil nil ->
     levels_pseudotopo G L
  /\ I = incoming_level G L
  /\ (forall t, reachable (edge_to_upd G L0 new_level) source t ->
                t \in visited).
Proof.
  introv HI. splits.
  { unfold levels_pseudotopo. intros x y Hxy.
    forwards~ HL0: invf_levels_pseudotopo HI x y.
    forwards~ [->|(Ix1&Ix2)]: invf_level_increases HI x.
    { forwards~ [?|?]: invf_level_increases HI y. }
    { rewrite Ix2 in Ix1.
      forwards~ [Ey|(Iy1&Iy2)]: invf_level_increases HI y;[].
      rewrite <-Ey in *.
      tests: (x \in visited).
      { forwards~: Traversal.Inv_visited_edge HI x y.
        rew_listx in *. branchx; try (now false);[].
        forwards~: invf_visited_level HI y. applys~ HI. }
      { forwards~: invf_notvisited_level HI x. } } }
  { apply fun_ext_1. intro y.
    tests: (L y = new_level).
    { forwards~ ->: invf_incoming_level HI y. rew_set=>x. rew_set.
      rewrite in_set_st_eq. rewrite !mem_nil_eq. split*. }
    { forwards* [|[??]]: invf_level_increases HI y.
      assert (I y = I0 y) as ->.
      { tests: (y \in visited).
        { forwards~ : invf_visited_level HI y.
          forwards~ <- : invf_incoming_above_level HI. }
        { forwards~ -> : invf_incoming_notvisited HI y. } }
      forwards -> : invf_incoming0 HI. rew_set=>x.
      rewrite !in_set_st_eq /incoming_level.
      tests: (y \in visited).
      { forwards~ : invf_visited_level HI y.
        forwards [?|[??]]: invf_level_increases HI x; intuition math. }
      { forwards [?|[??]]: invf_level_increases HI x; (try intuition math); [].
        split=>-[??]; split~. tests: (x \in visited).
        { forwards~ Hxy : Traversal.Inv_visited_edge HI x y.
          rewrite !mem_nil_eq in Hxy. destruct Hxy as [(?&_&_)|[([]&_&_)|([]&_&_)]].
          forwards* [??] : Traversal.Inv_visited_edges_visited HI x y. }
        { forwards~ -> : invf_notvisited_level. } } } }
  { applys~ Traversal.Inv_complete. }
Qed.

Lemma invf_complete_edges_below : forall G L0 L M I0 I P0 P new_level source target visited visited_edge,
  L source = new_level ->
  invf G L0 L M I0 I P0 P new_level source target visited visited_edge nil nil ->
  (forall x,    (reachable (has_edge G) source x /\ L x = new_level)
             \/ enough_edges_below G L x).
Proof.
  introv HLsource HInv. intros x.
  forwards~ [[HRx ?]|?]: invf_edges_below HInv x.
  tests: (L x = new_level); [ now autos~ | false ].
  forwards~ (HL & HI & HR): invf_complete0 HInv.
  forwards~: pseudotopo_reachable HL HRx.
Qed.

(******************************************************************************)

Definition marked M m : set vertex :=
  \set{ x | M x = m }.

Definition stack_potential G stack :=
  LibList.fold (LibMonoid.monoid_make Zplus 0)
    (fun x => 2 * card (out_edges G x) + 1) stack.

Lemma stack_potential_nonneg : forall G stack,
  0 <= stack_potential G stack.
Proof.
  unfold stack_potential. induction stack as [|x stack' HI].
  { rewrite LibList.fold_nil. reflexivity. }
  { rewrite LibList.fold_cons. simpl. rewrite~ <-HI. }
Qed.

Lemma stack_potential_cons : forall G x stack,
  stack_potential G (x :: stack) =
  2 * card (out_edges G x) + 1 + stack_potential G stack.
Proof. intros. reflexivity. Qed.

Lemma stack_potential_nil : forall G,
  stack_potential G nil = 0.
Proof. intros. reflexivity. Qed.

Definition pot_inv G L new_level leftover_credits :=
     card (edges_unvisited G L new_level)
     <= net G L + leftover_credits
  /\ 0 <= leftover_credits.

Definition visit_forward_spec :
  sigT (fun (potential_cst : Z) =>
  (0 <= potential_cst) *
  spec1 [cost] (
    forall (leftover_credits: int),
    forall g G L0 L M Ms I0 I P0 P,
    forall (new_level: int) (source: vertex)
           (m: mark) (stack: list vertex) (visited: set vertex) (visited_edge: set (vertex * vertex)),
    invf G L0 L M I0 I P0 P new_level source (marked M m) visited visited_edge stack nil ->
    pot_inv G L new_level leftover_credits ->
    app visit_forward [g new_level m stack]
      PRE (\$ cost tt \*
           \$ (potential_cst * stack_potential G stack) \*
           \$ (2 * potential_cst * net G L) \*
           \$ (2 * potential_cst * leftover_credits) \*
           IsRawGraph g G L M Ms I P)
      POST (fun (res: forward_search_result_) =>
        Hexists L' I' P', IsRawGraph g G L' M Ms I' P' \*
        (match res with
         | ForwardCyclic _ _ => \[]
         | ForwardCompleted => \$ (2 * potential_cst * net G L')
         end) \*
        \[
          (forall x, L' x = L0 x \/ (L0 x < L' x /\ L' x = new_level)) /\
          (L' source = new_level) /\
          (forall x, L' x = L0 x \/ reachable (has_edge G) source x) /\
          parents_in_graph G P' /\
          (forall x, P' x <> P0 x -> M x <> m) /\
          match res with
           | ForwardCyclic z t =>
             M t = m /\
             reachable (with_parents P' (edge_to_upd G L0 new_level)) source z /\
             edge_to_upd G L0 new_level z t
           | ForwardCompleted =>
             levels_pseudotopo G L' /\
             I' = incoming_level G L' /\
             (forall x,    (reachable (has_edge G) source x /\ L' x = new_level)
                        \/ enough_edges_below G L' x) /\
             (forall x, x \notin vertices G -> L' x = L0 x) /\
             (forall t, reachable (edge_to_upd G L0 new_level) source t ->
                        M t <> m)
           end ])))%type.
Proof.
  begin defer assuming potential_cst stop_cost in _g. exists potential_cst.
  let t := type of stop_cost in unify t int.
  defer Ha: (0 <= potential_cst). split; [ apply Ha |].
  begin defer assuming costf in gcost.
  xspecO (fun (_:unit) => costf). defer Hcost: (0 <= costf).
  introv. sets_eq measure: (card (vertices_below G L new_level) + length stack).
  gen L M I P visited visited_edge stack leftover_credits.
  induction_wf IH: (downto 0) measure; hide IH.
  introv -> HInv Hpotinv.
  xcf. weaken costf. xpay. xvals. xmatch_no_intros.
  { (* the stack is empty, we are done. *)
    intros ->. xrets~.
    now rewrite -stack_potential_nonneg //.
    now unfold pot_inv in Hpotinv; math_nia.
    forwards~: invf_complete0. unpack. repeat splits~.
    { now applys~ invf_level_increases. }
    { now applys~ invf_level_source. }
    { intros x. forwards~ [?|[? ?]]: invf_level_increases HInv x.
      tests~: (x \in visited); [| now forwards~: invf_notvisited_level].
      right. forwards~: Traversal.Inv_visited_reachable.
      applys~ covariant_rtclosure. rel_crush. }
    { applys~ invf_parents. applys~ HInv. }
    { intros. forwards~ [?|?]: Traversal.Inv_parents_modified HInv x;[].
      forwards~ HT: Traversal.Inv_target. unfold marked in HT. rew_set in HT.
      specializes HT x. rew_set in HT. tauto. (* TODO: lemma? *)}
    { applys~ invf_complete_edges_below. applys~ invf_level_source. }
    { intros x Hx. applys~ invf_notvisited_level. }
    { intros t Rt Mt. forwards~ HT: Traversal.Inv_target.
      rew_set in HT. applys~ (>>HT t Mt). } }

  xcleanpat. intros x stack' ->.
  xapp~ as ys. intros [ys_nodup Hys].
  xfun as foldf. hide Sfoldf.

  (* TODO: lemma? *)
  assert (L0 x < new_level).
  { forwards~: invf_stack __. rew_listx~ in *. }

  pose cred_stop (ys stack: list vertex) L :=
    (potential_cst + (* kept for paying the recursive call afterwards *)
     2 * potential_cst * length ys +
     potential_cst * stack_potential G stack +
     2 * potential_cst * net G L).
  pose cred_continue ys stack L :=
    (cred_stop ys stack L + stop_cost).

  assert (Hcredys: forall y ys stack L,
    cred_continue (y :: ys) stack L =
    2 * potential_cst + cred_continue ys stack L).
  { intros. rewrite /cred_continue /cred_stop length_cons. by pols. }

  assert (Hcredbump: forall y ys stack L',
    L' y < new_level ->
    cred_continue ys (y :: stack) (fupdate L' y new_level) =
    potential_cst + cred_continue ys stack L'
    - 2 * potential_cst * deltap G L' y (L' y + 1) new_level).
  { intros. rewrite /cred_continue /cred_stop stack_potential_cons. pols.
    rewrite net_bump_levels_sub; math_lia. }

  (* Loop invariant for fold *)
  xlet as ifold_res.

  (* The _no_simpl variant is required; for entering the loop, we need to pay
     with (most of) the credits in our current pre-condition, not only the
     piggybank. *)
  xapp_spec_no_simpl (spec interruptible_fold_remaining_spec)
    (fun (ys': list vertex) last_step =>
      Hexists L' I' P' visited visited_edge, Hexists leftover,
      IsRawGraph g G L' M Ms I' P' \*
      \$ (2 * potential_cst * leftover) \*
      \[ pot_inv G L' new_level leftover ] \*
      match last_step with
      | Break t => Hexists ys'' stack'',
        \$ cred_stop ys'' stack'' L' \* \[
        invf G L0 L' M I0 I' P0 P' new_level source (marked M m) visited visited_edge stack'' (mkedges x ys'') /\
        mem t ys'' /\ M t = m
        ]
      | Continue stack'' =>
        \$ cred_continue ys' stack'' L' \* \[
        invf G L0 L' M I0 I' P0 P' new_level source (marked M m) visited visited_edge stack'' (mkedges x ys') /\
        (card (vertices_below G L' new_level) + length stack'' <=
         card (vertices_below G L new_level) + length stack') ]
       end
    ).

  (* body of the loop *)
  { intros y ys' stack'' Hys'. xpull.
    intros L' I' P' visited' visited_edge' leftover (Hpot&Hleft) (Hinv&Hcard).
    weaken (cred_continue _ _ _). xapp. xpay.
    xapp~. intros Hismarked. xif.
    { (* we found a cycle; stop the search *)
      apply xret_lemma. hsimpl leftover. piggybank: *rhs. now splits~. splits~. }

    forwards~: Traversal.Inv_in_processing_source_visited (invf_trav Hinv).
    assert (~ y \in (marked M m)). { unfold marked. rew_set. autos. }
    xapps~. xapps~. xif_guard as CC.
    { (* L' y < new_level *)
      xapps~. xapps~. xapps~. { fupdate_case. intro. rew_set in *. auto. }
      apply xret_lemma.
      set delta := deltap G L' y (L' y + 1) new_level.
      assert (0 <= delta) by apply~ deltap_nonneg.
      hsimpl (leftover + delta); hsimpl_hint_remove tt.
      rewrite (Z.mul_add_distr_l _ leftover) credits_split_eq. hsimpl.
      (* FIXME *) piggybank: *rhs.
      (* xcontinue~. *) (* FIXME *)
      split.
      { rewrite fupdate_add_fupdate (_: forall x, \{} \u \{x} = \{x}).
        { intro. rew_set;=> ?. rew_set. tauto. }
        apply~ invf_bump_lower_level. }
      { rew_listx. rewrite~ vertices_below_fupdate. rewrite~ card_diff_single'.
        { apply finite_vertices_below. }
        { unfold vertices_below. rew_set. splits~. } }
      { split; [| math]. rewrite~ net_bump_levels_sub.
        apply Zplus_le_reg_r with (p := card (out_edges G y)). ring_simplify.
        rewrite~ edges_unvisited_fupdate. } }

    xrets~. xif_guard as CC2.
    { (* L' y = new_level *)
      xapps~.
      { rewrites~ (>>invf_incoming_level Hinv). unfold notin.
        rew_set. intros (_&_&[]). left. }
      apply xret_lemma.
      hsimpl leftover; hsimpl_hint_remove tt. hsimpl.
      piggybank: *rhs.
      (*xcontinue~. *)
      split. now apply~ invf_already_at_level; apply HInv.
      rew_set. math. splits~. }
    { (* L' y > new_level *)
      asserts~: (L' y > new_level).
      apply xret_lemma.
      hsimpl leftover; hsimpl_hint_remove tt. hsimpl.
      piggybank: *rhs.
      (*xcontinue~. *)split. now apply~ invf_skip_higher_level; apply HInv.
      math. splits~. }

    repeat case_if; repeat case_max.
    { rewrite Hcredys Hcredbump //. pols. defer in _g. }
    { rewrite /cred_continue. pols. defer in _g. }
    { rewrite Hcredys. pols. defer in _g. }
    { rewrite /cred_continue. pols. deferred in _g; by auto. }
    { rewrite Hcredys. pols. defer in _g. }
    { rewrite /cred_continue. pols. deferred in _g; by auto. } }

  { (* Entering the loop *)
    hsimpl~ leftover_credits.
    - piggybank+ (cost interruptible_fold_remaining_spec _). piggybank: *.
      rewrite /cred_continue /cred_stop stack_potential_cons. pols.
      rewrite length_eq -(@length_to_set_noduplicates _ ys) //. pols.
      piggybank: * done.
    - split. now applys~ invf_stack_pop. math. }

  (* the rest of the program. *)
  xpull. intros ys' L' I' P' visited' visited_edge' leftover (Hpot & Hleft) Hres.
  xmatch_no_intros. intros t ->.
  { (* ForwardCyclic *)
    xpull. intros ys'' stack'' (Hinv' & Ht & HMt).
    (* fixme: hsimpl should probably collects all the credits to GC, instead of
       GCing them separately. *)
    rewrite (star_assoc (\$ _) (\$ _)) -credits_split_eq.
    xrets~.
    { rewrite le_zarith. unfold cred_stop. polf. apply Z.mul_nonneg_nonneg.
      now deferred. forwards~: stack_potential_nonneg G stack''. }

    splits~.
    now apply~ invf_level_increases.
    now applys~ invf_level_source.
    { intros x'. forwards~ [?|[? ?]]: invf_level_increases Hinv' x'.
      tests~: (x' \in visited'); [| now forwards~: invf_notvisited_level].
      right. forwards~: Traversal.Inv_visited_reachable.
      applys~ covariant_rtclosure. rel_crush. (* TODO: lemma *) }
    { applys~ invf_parents. apply Hinv'. }
    { intros z HPz. forwards~ [?|?]: Traversal.Inv_parents_modified Hinv' z;[].
      forwards~ HT: Traversal.Inv_target. unfold marked in HT. rew_set in HT.
      specializes HT z. rew_set in HT. tauto. (* TODO: lemma? *)}
    { forwards~: Traversal.Inv_visited_reachable Hinv' x. rel_covariant. }
    { applys~ (>> Traversal.Inv_in_processing_is_edge (invf_trav Hinv')). } }

  intros stack'' ->. subst ys'.
  xpull. intros (Hinv' & ?).
  (* Tail-recursive call. *)
  xapps~ Hinv'.
  (* termination argument *) now rew_list~.
  (* potential *) now rewrite /pot_inv //.
  (* credits *)
  { piggybank: *0. rewrite /cred_continue /cred_stop length_nil. pols.
    defer in gcost. }
  (* post-condition *) now hsimpl~; piggybank: *0.

  { simp. repeat case_max; try pols; defer in gcost. }
  { intros _. deferred in gcost; math. }
  now monotonic. now dominated.
  end defer. elia_core; [| math_lia]. defer. end defer.

  (* FIXME: make elia able to handle this *)
  exists_big potential_cst Z.
  exists_big stop_cost Z.
  repeat split. big. big. big.
  assert (0 <= potential_cst) by big.
  match goal with |- ?x <= _ => enough (x <= potential_cst) by math_lia end.
  big.
  assert (0 <= potential_cst) by big.
  match goal with |- ?x <= _ => enough (x <= potential_cst) by math_lia end.
  big.
  assert (0 <= potential_cst) by big. assert (0 <= stop_cost) by big. math.
  assert (1 <= potential_cst) by big. assert (1 <= stop_cost) by big. math.
  pols. apply Zplus_le_reg_r with potential_cst. pols. big.
  close. close.
Qed.

Definition potential_cst : Z := projT1 visit_forward_spec.
Definition potential_cst_nonneg : 0 <= potential_cst := fst (projT2 visit_forward_spec).
Definition visit_forward_specO := snd (projT2 visit_forward_spec).

(******************************************************************************)
(* At this point, we can define the potential function for the whole graph,
   since only [forward_search] uses it. *)

Definition ϕ_cst := 2 * potential_cst.
Lemma ϕ_cst_nonneg : 0 <= ϕ_cst.
Proof. rewrite /ϕ_cst -potential_cst_nonneg //. Qed.
Hint Resolve ϕ_cst_nonneg.

Definition ϕ G L :=
  ϕ_cst * net G L.

Lemma ϕ_lowerbound : forall G L I P,
  Inv G L I P ->
  ϕ_cst * card (edges G) <= ϕ G L.
Proof.
  intros. rewrite /ϕ /ϕ_cst. forwards~: Inv_net_lowerbound.
  lets: potential_cst_nonneg. math_nia.
Qed.

(* Immediate corollary *)
Lemma ϕ_nonneg : forall G L I P,
  Inv G L I P ->
  0 <= ϕ G L.
Proof. intros. forwards~: ϕ_lowerbound. lets: ϕ_cst_nonneg. math_nia. Qed.

(******************************************************************************)

Lemma invf_init : forall G L M I P w new_w_level m,
  w \in vertices G ->
  M w <> m ->
  L w < new_w_level ->
  P w = None ->
  Inv G L I P ->
  invf G L (fupdate L w new_w_level) M I (fupdate I w \{}) P P
       new_w_level w (marked M m) \{w} \{} (w :: nil) nil.
Proof.
  introv Nw Tw Hw HP HI.
  constructor; try now apply HI.
  { apply~ Traversal.Inv_init. intros x' y' [? ?]. splits~. }
  { intros. rew_set in *. subst x. fupdate_case. }
  { intros. fupdate_case. }
  { intros. fupdate_case. substs~. }
  { rew_listx. rew_set. repeat splits~. fupdate_case. }
  { fupdate_case. }
  { intro y. rew_set. intros. fupdate_case. tauto. }
  { intro y. unfold incoming_partial. fupdate_case; subst; fupdate_case.
    { intros _. rew_set=>x'. rew_set. split*. rewrite mem_one_eq.
      unfold incoming_level. repeat fupdate_case. tauto. intros [[??]_].
      forwards~ : Inv_pseudotopo x' w. math. }
    { intros ?. rew_set=>x'. rew_set. rewrites (>>Inv_incoming HI).
      rewrite /incoming_level in_set_st_eq mem_one_eq mem_nil_eq. split.
      { intros [??]. assert (x' <> w) by (intros ->; math).
        splits~. splits~. repeat fupdate_case. }
      { repeat fupdate_case; tauto. } } }
  { intros. fupdate_case. subst y. math. }
  { intros x. forwards HEx: Inv_edges_below HI x.
    apply or_classic_r. rew_logic. intros [HR|HLx].
    { applys~ enough_edges_below_bump_level_notreachable. apply rtclosure_refl. }
    { applys~ enough_edges_below_bump_level_abovelevel. fupdate_case_in HLx. } }
Qed.

Lemma reachable_edge_to_upd_below : forall G L new_level x y,
  levels_pseudotopo G L ->
  L x < new_level ->
  L y < new_level ->
  reachable (has_edge G) x y ->
  reachable (edge_to_upd G L new_level) x y.
Proof.
  introv LP Lx Ly.
  lets: ltac_mark. induction 1 using rtclosure_ind_l; gen_until_mark.
  { apply rtclosure_refl. }
  { intros y Hxy Ryz Hind.
    forwards~: LP x y.
    forwards~: pseudotopo_reachable y z.
    forwards~: Hind. applys~ rtclosure_l. }
Qed.

Definition forward_search_spec :
  spec1 [cost] (
    forall g G L M Ms I P (w: vertex) (new_w_level: int) (visited_backward: mark),
    w \in vertices G ->
    L w < new_w_level ->
    M w <> visited_backward ->
    app forward_search [g w new_w_level visited_backward]
      PRE (\$ cost tt \*
           \$ ϕ G L \*
           IsRawGraph g G L M Ms I P \*
           \[ Inv G L I P ])
      POST (fun (res: forward_search_result_) =>
        Hexists L' I' P', IsRawGraph g G L' M Ms I' P' \*
        \[ L' w = new_w_level /\
           (forall x, L' x = L x \/ reachable (has_edge G) w x) /\
           (forall x, P' x <> P x -> M x <> visited_backward) ] \*
         match res with
         | ForwardCyclic z t =>
           \[ M t = visited_backward
              /\ reachable (with_parents P' (has_edge G)) w z /\ has_edge G z t
              /\ parents_in_graph G P' ]
         | ForwardCompleted =>
           \$ ϕ G L' \*
           \[ (forall t1 t2,
                 L t1 < new_w_level ->
                 reachable (has_edge G) w t1 ->
                 has_edge G t1 t2 ->
                 M t2 <> visited_backward)
              /\ InvExcept
                   (set_st (fun x => reachable (has_edge G) w x /\ L' x = new_w_level))
                   G L' I' P' ]
         end)).
Proof.
  xspecO_refine straight_line.
  intros. xcf. intro HInv.
  xpay. xapp~. xapp~.
  xchange (>> IsRawGraph_forget_parent g w). auto. apply pred_incl_refl. (* XXX *)
  xapply (>> spec visit_forward_specO (deltap G L w (L w + 1) new_w_level)
             g G L (fupdate L w new_w_level) (fupdate P w None)).
  { applys~ invf_init. fupdate_case. }
  { unfold pot_inv. split; [| apply~ deltap_nonneg].
    rewrite net_bump_levels_sub // edges_unvisited_fupdate_sub //. pols.
    rewrite -Inv_net_lowerbound //. apply card_le_of_incl'.
    now apply finite_graph_edges. now apply edges_unvisited_edges. }
  { hsimpl. piggybank+ (cost visit_forward_specO _). piggybank: *.
    rewrite /ϕ -/potential_cst /ϕ_cst stack_potential_cons stack_potential_nil.
    rewrite net_bump_levels_sub //. pols. piggybank: * done. }

  intros res. hpull as L' I' P'. intros (HL & HLw & HLM & HP' & HPP' & Hres).
  piggybank: *0. destruct res as [ z t |].
  { destruct Hres as (Ht&Hwz&Hzt&Hzw). hsimpl; try splits~.
    { applys~ covariant_rtclosure. rel_crush. }
    { intros. tests~: (x = w); []. applys~ HPP'. fupdate_case. } }
  { destruct Hres as (?&?&?&?&HR). hsimpl~; splits~.
    { intros. eapply HR, rtclosure_r. apply~ reachable_edge_to_upd_below.
      now apply HInv. unfolds~. }
    { (* Prove [InvExcept] on the updated graph. *)
      constructors~.
      { applys~ Inv_acyclic. }
      { (* levels can only increase so they are still bigger than 1 *)
        intro x. specializes HL x. forwards~: Inv_levels x. branches~. } }
    { intros x ?. tests~: (x = w);[]. applys~ HPP'. fupdate_case. } }

  cleanup_cost. monotonic. dominated.
Qed.

End ForwardSearch.

Hint Unfold edge_to_upd : rel_crush.
Hint Extern 1 (edge_to_upd _ _ _ _ _) => now (unfold edge_to_upd; splits~).

Hint Extern 1 (RegisterSpec forward_search) =>
  Provide (provide_specO forward_search_spec).

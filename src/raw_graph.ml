type mark = int
type graph = {
    mutable next_mark : mark
}
type vertex = {
    mutable outgoing : vertex list;
    mutable incoming : vertex list;
    mutable mark : mark;
    mutable level : int;
    mutable parent : vertex;
}

let new_mark (g: graph): mark =
  let res = g.next_mark in
  g.next_mark <- res + 1;
  res

let vertex_eq (n1: vertex) (n2: vertex): bool =
  n1 == n2
let is_marked (_g: graph) (n: vertex) (m: mark): bool =
  n.mark = m
let set_mark (_g: graph) (n: vertex) (m: mark): unit =
  n.mark <- m
let get_level (_g: graph) (n: vertex): int =
  n.level
let set_level (_g: graph) (n: vertex) (l: int): unit =
  n.level <- l

let get_incoming (_g: graph) (n: vertex): vertex list =
  n.incoming
let clear_incoming (_g: graph) (n: vertex): unit =
  n.incoming <- []
let add_incoming (_g: graph) (n1: vertex) (n2: vertex): unit =
  n1.incoming <- n2::n1.incoming

let get_outgoing (_g: graph) (n: vertex): vertex list =
  n.outgoing

let set_parent (_g: graph) (n: vertex) (p: vertex) =
  n.parent <- p
let get_parent (_g: graph) (n: vertex) =
  n.parent

let raw_add_edge (_g: graph) (n1: vertex) (n2: vertex): unit =
  n1.outgoing <- n2::n1.outgoing

let raw_add_vertex (_g: graph) (_v: vertex): unit =
  ()

(* These two functions are not directly called by the algorithm, but the client
   needs them in order to interact with it. *)

let init_graph (): graph =
  { next_mark = 0 }

let new_vertex (): vertex =
  let rec v =
    { outgoing = [];
      incoming = [];
      mark = -1;
      level = 1;
      parent = v; } in
  v

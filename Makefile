lib:
	dune build @install

proofs: check-src
	@ $(MAKE) -C proofs all

check-src:
	dune build src/incremental_cycles_internal.cma

webapp:
	dune build webapp/src/main.bc.js

clean:
	rm -rf _build
	@ $(MAKE) -C proofs $@

.PHONY: proofs clean lib check-src webapp

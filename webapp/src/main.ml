open Cycles_lib
open! Cycles_lib.Utils
open Js_of_ocaml_tyxml.Tyxml_js
open Js_of_ocaml_lwt
open Lwt.Infix
module RList = ReactiveData.RList

(* -------------- *)
(* Read initial actions from the URL anchor *)

let initial_actions () =
  let open Js_of_ocaml in
  let url = Js.to_string Dom_html.document##.URL |> Uri.of_string in
  match Uri.fragment url with
  | Some s ->
    begin match Actions.parse s with
      | Ok l -> l
      | Error s ->
        Printf.eprintf "Cannot parse actions (%s)\n" s; []
    end
  | None ->
    []

let queue_actions st l =
  List.iter st.Controls.queue_action l

(* -------------- *)
(* Document *)

let st = Controls.create_st ()

let () =
  let main = Js_of_ocaml.Dom_html.getElementById "main" in
  Lwt.async (fun _ ->
    Lwt_js_events.domContentLoaded () >>= fun _ ->
    main##appendChild (To_dom.of_node (Controls.main_elt st)) |> ignore;
    SpaceAllocator.update_wh st.manager.view.space;
    queue_actions st (initial_actions ());
    Lwt.join [
      SpaceAllocator.follow_resize st.manager.view.space;
      Controls.run st
    ]
  );

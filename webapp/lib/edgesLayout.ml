open! Utils
open! Lwt.Infix

type circleid = int
type arrow_style = [`Normal | `Bad]

type action =
  | NewCircle of { id: circleid; x: int; y: int; size: int }
  | MoveCircleTo of { id: circleid; x: int; y: int }
  | RemoveCircle of circleid
  | NewArrow of { circle1: circleid; circle2: circleid; style: arrow_style; }
  | RemoveArrow of { circle1: circleid; circle2: circleid }

type actions = action list

(* --- *)

type circle = {
  x : int;
  y : int;
}

type arrow = {
  curved : bool;
  age : int;
}

type pst = {
  circles : circle IMap.t;
  arrows : arrow IIMap.t;
}

type st = pst ref

let create_st () =
  ref ({ circles = IMap.empty; arrows = IIMap.empty })

(* --- *)

let update_circles (circles: circle IMap.t) actions =
  List.fold_left (fun circles action ->
    match action with
    | NewCircle {id; x; y; size = _} -> IMap.add id {x;y} circles
    | MoveCircleTo {id; x; y} -> IMap.add id {x;y} circles
    | RemoveCircle id -> IMap.remove id circles
    | NewArrow _ | RemoveArrow _ -> circles
  ) circles actions

let update_arrows (arrows: arrow IIMap.t) actions =
  List.fold_left (fun arrows action ->
    match action with
    | NewArrow { circle1; circle2; style = _ } ->
      let age = IIMap.fold (fun _ {age;_} m -> max age m) arrows 0 in
      IIMap.add (circle1, circle2) { curved = false (* dummy *); age } arrows
    | RemoveArrow { circle1; circle2 } ->
      IIMap.remove (circle1, circle2) arrows
    | _ -> arrows
  ) arrows actions

let colin a b = Gg.(Float.equal V2.(dot (ortho a) b) 0.)

let arrows_overlap circles (c1, c2) (c1', c2') =
  let open Gg in
  let circle_v id =
    let c = IMap.find id circles in
    V2.v (float c.x) (float c.y) in
  let v1 = circle_v c1 and v2 = circle_v c2 in
  let v1' = circle_v c1' and v2' = circle_v c2' in
  let v1v2 = V2.(v2 - v1) and v1v2' = V2.(v2' - v1') in
  colin v1v2 v1v2' &&
  colin v1v2 V2.(v1' - v2) &&
  begin
    let uv = V2.unit v1v2 in
    let sort2 a b = if a < b then (a, b) else (b, a) in
    let z1, z2 = sort2 0. (V2.dot v1v2 uv) in
    let z1', z2' = sort2 (V2.dot V2.(v1' - v1) uv) (V2.dot V2.(v2' - v1) uv) in
    let overlap = max 0. ((min z2 z2') -. (max z1 z1')) in
    overlap > 5.
  end

let arrow_overlaps_circle circles (cx, cy) (c1, c2) =
  let open Gg in
  let circle_v id =
    let c = IMap.find id circles in
    V2.v (float c.x) (float c.y) in
  let v1 = circle_v c1 and v2 = circle_v c2 in
  let vv = V2.v (float cx) (float cy) in
  let v1v2 = V2.(v2 - v1) in
  let v1vv = V2.(vv - v1) in
  colin v1v2 v1vv &&
  begin
    let k = V2.dot v1vv (V2.unit v1v2) in
    0. < k && k < V2.norm v1v2
  end

let relayout_arrows circles (arrows: arrow IIMap.t): arrow IIMap.t =
  let arrows =
    IIMap.bindings arrows
    |> List.sort (fun (_, a1) (_, a2) -> compare a1.age a2.age)
  in
  List.fold_left (fun arrows_done ((c1, c2), a) ->
    let curved =
      OSeq.exists (fun ((c1', c2'), a') ->
        not a'.curved &&
        arrows_overlap circles (c1, c2) (c1', c2')
      ) (IIMap.to_seq arrows_done)
      || OSeq.exists (fun (c, {x = cx; y = cy; _}) ->
        c <> c1 && c <> c2 &&
        arrow_overlaps_circle circles (cx, cy) (c1, c2)
      ) (IMap.to_seq circles)
    in
    IIMap.add (c1, c2) { a with curved } arrows_done
  ) IIMap.empty arrows

let compile_actions st actions =
  let st0 = !st in
  let circles = update_circles st0.circles actions in
  let arrows = update_arrows st0.arrows actions in
  let arrows' = relayout_arrows circles arrows in
  let curvef = function true -> 0.4 | false -> 0. in
  let updatecurve_actions = IIMap.fold (fun (c1, c2) a acts ->
    match IIMap.find_opt (c1, c2) st0.arrows with
    | None -> acts (* new arrow *)
    | Some a0 ->
      if a0.curved <> a.curved then
        Viewer.SetArrowCurve { circle1 = c1; circle2 = c2;
                               curve = curvef a.curved }
        :: acts
      else
        acts
  ) arrows' [] in
  st := { circles; arrows = arrows' };
  updatecurve_actions @
  List.map (function
    | NewCircle {id; x; y; size} -> Viewer.NewCircle {id; x; y; size}
    | MoveCircleTo {id; x; y} -> Viewer.MoveCircleTo {id; x; y}
    | RemoveCircle id -> Viewer.RemoveCircle id
    | NewArrow { circle1; circle2; style } ->
      Viewer.NewArrow {
        circle1; circle2; style;
        curve = curvef (IIMap.find (circle1, circle2) arrows').curved }
    | RemoveArrow { circle1; circle2 } -> Viewer.RemoveArrow { circle1; circle2 }
  ) actions

open Utils

module G : sig
  type vertex = int
  type mark = int
  type graph

  val create : unit -> graph
  val vertices : graph -> vertex list
  val add_cycle_edge : graph -> vertex -> vertex -> unit
  val cycle_edge : graph -> (vertex * vertex) option
  val outgoing : graph -> vertex -> vertex list

  type snapshot
  val snapshot : graph -> snapshot
  val rollback : graph -> snapshot -> unit

  val next_mark : snapshot -> mark
  val marks : snapshot -> mark IMap.t
  val visited_forward : snapshot -> vertex list

  module Diff : sig
    type elt =
    | NewNode of { node: int; level: int; }
    | NewEdge of { node1: int; node2: int; style : [`Normal | `Bad] }
    | ChangeNodeLevel of { node: int; before: int; after: int }

    type t = elt list

    val compute : snapshot -> snapshot -> t
  end
end

module IC :
  Incremental_cycles.S
  with type vertex := G.vertex
   and type graph := G.graph

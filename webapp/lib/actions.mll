{
  type action =
  | AddVertex
  | AddEdge of int * int
  | Undo
  | Redo
}

rule action = parse
  | "v"
      { `Tok AddVertex }
  | "e" (['0'-'9']+ as v1) ";" (['0'-'9']+ as v2)
      { `Tok (AddEdge (int_of_string v1, int_of_string v2)) }
  | "u"
      { `Tok Undo }
  | "r"
      { `Tok Redo }
  | eof
      { `Eof }

{
(* Printer and parser for a list of actions (used for permalinks) *)
let parse s =
  let lb = Lexing.from_string s in
  let rec loop l =
    match action lb with
    | `Tok t -> loop (t :: l)
    | `Eof -> List.rev l
  in
  try Ok (loop [])
  with Failure s -> Error s

let print l =
  let b = Buffer.create 10 in
  List.iter (function a ->
    Buffer.add_string b (match a with
    | AddVertex -> "v"
    | AddEdge (x, y) -> Printf.sprintf "e%d;%d" x y
    | Undo -> "u"
    | Redo -> "r")
  ) l;
  Buffer.contents b
}

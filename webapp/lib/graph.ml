open Utils

(* Persistent graph *)
module P = struct
  type vertex = int
  (* let vertex_eq = ((=) : int -> int -> bool) *)

  type mark = int

  type graph = {
    next_mark : int;
    outgoing : vertex list IMap.t;
    level : int IMap.t;
    incoming : vertex list IMap.t;
    mark : mark IMap.t;
    parent : vertex IMap.t;
    cycle_edge : (int * int) option;
  }

  let empty =
    { next_mark = 0;
      outgoing = IMap.empty;
      level = IMap.empty;
      incoming = IMap.empty;
      mark = IMap.empty;
      parent = IMap.empty;
      cycle_edge = None;
    }

  let get_outgoing g v = IMap.find v g.outgoing
  let raw_add_edge g v w =
    { g with outgoing = IMap.add v (w :: get_outgoing g v) g.outgoing }
  let raw_add_vertex g v =
    { g with
      outgoing = IMap.add v [] g.outgoing;
      level = IMap.add v 1 g.level;
      incoming = IMap.add v [] g.incoming;
      mark = IMap.add v (-1) g.mark;
      parent = IMap.add v (-1) (* dummy *) g.parent; }
  let new_mark g =
    let m = g.next_mark in
    let g = { g with next_mark = m+1 } in
    g, m
  let is_marked g v m =
    IMap.find v g.mark = m
  let set_mark g v m =
    { g with mark = IMap.add v m g.mark }
  let get_level g v =
    IMap.find v g.level
  let set_level g v l =
    { g with level = IMap.add v l g.level }
  let get_incoming g v =
    IMap.find v g.incoming
  let clear_incoming g v =
    { g with incoming = IMap.add v [] g.incoming }
  let add_incoming g v w =
    { g with incoming = IMap.add v (w :: IMap.find v g.incoming) g.incoming }
  let get_parent g v =
    IMap.find v g.parent
  let set_parent g v w =
    { g with parent = IMap.add v w g.parent }

  let add_cycle_edge g v w =
    { g with cycle_edge = Some (v, w) }

end

module G = struct
  type vertex = int
  let vertex_eq = (=)

  type mark = int

  type graph = {
    mutable g : P.graph;
    mutable visited_forward : vertex list;
  }

  let create () = { g = P.empty; visited_forward = [] }

  let get_outgoing g v =
    g.visited_forward <- v :: g.visited_forward;
    P.get_outgoing g.g v
  let raw_add_edge g v w = g.g <- P.raw_add_edge g.g v w
  let raw_add_vertex g v = g.g <- P.raw_add_vertex g.g v
  let new_mark g =
    let g', m = P.new_mark g.g in
    g.g <- g'; m
  let is_marked g v m = P.is_marked g.g v m
  let set_mark g v m = g.g <- P.set_mark g.g v m
  let get_level g v = P.get_level g.g v
  let set_level g v l = g.g <- P.set_level g.g v l
  let get_incoming g v = P.get_incoming g.g v
  let clear_incoming g v = g.g <- P.clear_incoming g.g v
  let add_incoming g v w = g.g <- P.add_incoming g.g v w
  let get_parent g v = P.get_parent g.g v
  let set_parent g v w = g.g <- P.set_parent g.g v w

  let add_cycle_edge g v w = g.g <- P.add_cycle_edge g.g v w
  let vertices g =
    IMap.bindings (g.g.P.outgoing) |> List.map fst
  let cycle_edge g = g.g.P.cycle_edge
  let reset_visited_forward g = g.visited_forward <- []

  let outgoing g v =
    P.get_outgoing g.g v

  type snapshot = P.graph * vertex list

  let snapshot (g: graph) = g.g, g.visited_forward
  let rollback (g: graph) (s, vf) = g.g <- s; g.visited_forward <- vf
  let next_mark (g, _) = g.P.next_mark
  let marks (g, _) = g.P.mark
  let visited_forward (_, vf) = vf

  module Diff = struct
    type elt =
    | NewNode of { node: int; level: int; }
    | NewEdge of { node1: int; node2: int; style : [`Normal | `Bad] }
    | ChangeNodeLevel of { node: int; before: int; after: int }

    type t = elt list

    let compute (g1, _: snapshot) (g2, _: snapshot): t =
      let nodes g =
        IMap.bindings g.P.outgoing
        |> List.map fst
        |> ISet.of_list in
      let edges g =
        IMap.fold (fun v ws edges ->
          List.fold_left (fun edges w -> IISet.add (v, w) edges) edges ws
        ) g.P.outgoing IISet.empty
      in
      let new_nodes =
        ISet.diff (nodes g2) (nodes g1) |> ISet.elements in
      let new_edges =
        IISet.diff (edges g2) (edges g1) |> IISet.elements in
      let change_levels =
        IMap.bindings g1.level
        |> filter_map (fun (v, l1) ->
          let l2 = IMap.find v g2.level in
          if l2 <> l1 then Some (v, l1, l2) else None
        )
      in
      let cycle_edge = match g1.cycle_edge, g2.cycle_edge with
        | None, None | Some _, Some _ -> []
        | Some _, None -> assert false
        | None, Some (v, w) -> [(v, w)]
      in
      let add f l acc =
        List.fold_left (fun acc x -> f x :: acc) acc l in
      []
      |> add (fun node -> NewNode { node; level = IMap.find node g2.level })
        new_nodes
      |> add (fun (node1, node2) -> NewEdge { node1; node2; style = `Normal })
        new_edges
      |> add (fun (node, before, after) ->
        ChangeNodeLevel { node; before; after }) change_levels
      |> add (fun (node1, node2) -> NewEdge { node1; node2; style = `Bad })
        cycle_edge
  end
end

module IC = struct
  include Incremental_cycles.Make (G)

  let add_edge_or_detect_cycle g v w =
    G.reset_visited_forward g;
    ignore (G.new_mark g);
    add_edge_or_detect_cycle g v w
end

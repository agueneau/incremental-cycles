module Tbl_Make (H : Hashtbl.HashedType) = struct
  include Hashtbl.Make (H)

  let find_opt tbl x =
    try Some (find tbl x) with Not_found -> None

  let find ~msg tbl x =
    try find tbl x with Not_found -> failwith msg
end

module ITbl = Tbl_Make (struct
    type t = int
    let equal = (=)
    let hash = Hashtbl.hash
  end)

module IITbl = Tbl_Make (struct
    type t = int * int
    let equal = (=)
    let hash = Hashtbl.hash
  end)

module IMap = Map.Make (struct
  type t = int
  let compare = compare
end)

module IIMap = Map.Make (struct
  type t = int * int
  let compare = compare
end)

module ISet = Set.Make (struct
    type t = int
    let compare = compare
  end)

module IISet = Set.Make (struct
    type t = int * int
    let compare = compare
  end)

let rec filter_map f = function
  | [] -> []
  | x :: xs ->
    match f x with
    | None -> filter_map f xs
    | Some y -> y :: filter_map f xs

let rec reduce ~sep ~unit ~op = function
  | [] -> unit
  | [x] -> x
  | x :: y :: ys ->
    let (<+>) = op in
    x <+> sep <+> (reduce ~sep ~unit ~op (y :: ys))

(*******)

type 'a signal = {
  s : 'a React.S.t;
  set : ?step:React.step -> 'a -> unit
}

let create_signal x =
  let s, set = React.S.create x in
  { s; set }

let rec lwt_loop ~pause f x =
  let open Lwt.Infix in
  Js_of_ocaml_lwt.Lwt_js.sleep pause >>= fun () ->
  f x >>= fun x' ->
  lwt_loop ~pause f x'

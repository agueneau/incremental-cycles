open Utils
open Js_of_ocaml_tyxml.Tyxml_js
open Lwt.Infix
open Js_of_ocaml_lwt
module RList = ReactiveData.RList

(* -------------- *)

type circleid = int

type arrow_style = [`Normal | `Bad]

type action =
  | NewCircle of { id: circleid; x: int; y: int; size: int }
  | MoveCircleTo of { id: circleid; x: int; y: int }
  | RemoveCircle of circleid
  | NewArrow of { circle1: circleid; circle2: circleid;
                  style: arrow_style; curve: float; }
  | RemoveArrow of { circle1: circleid; circle2: circleid }
  | SetArrowCurve of { circle1: circleid; circle2: circleid; curve: float }
  | Par of action list
  | Seq of action list

(* ------------------ *)
(* Reactive data *)

type shape =
  | Circle of int
  | Arrow of int * int

type circle = {
  x : float signal;
  y : float signal;
  size : float;
  fill : Svg_types.paint signal;
  stroke : Svg_types.paint signal;
}

type arrow = {
  curve : float signal;
}

type st = {
  elements_l : (Html_types.svg_content Svg.elt * shape) RList.t;
  elements : (Html_types.svg_content Svg.elt * shape) RList.handle;
  circles : circle ITbl.t;
  arrows : arrow IITbl.t;
  clicks : int Lwt_stream.t; (* output stream *)
  queue_click : int -> unit;
  svg_elt : 'a. ([> Html_types.svg] as 'a) Html.elt;
  space: SpaceAllocator.st;
}

let make_svg_elt elements_l =
  let svg_prelude = [
    Svg.style (Svg.txt ".circle-text { font: bold 20px sans-serif; }")
  ] in
  R.Html.svg
    ~a:[Svg.a_width (100., Some `Percent);
        Svg.a_height (100., Some `Percent);
        Svg.a_style "position: fixed";
       ]
    (RList.concat
       (RList.const svg_prelude)
       (RList.map fst elements_l))

let create_st () =
  let elements_l, elements = RList.create [] in
  let circles = ITbl.create 10 in
  let arrows = IITbl.create 10 in
  let clicks, send_click = Lwt_stream.create () in
  let svg_elt = make_svg_elt elements_l in
  let space = SpaceAllocator.create_st (To_dom.of_element svg_elt) in
  { elements_l; elements; circles; arrows;
    clicks; queue_click = (fun i -> send_click (Some i));
    svg_elt; space;
  }

let circle st id =
  ITbl.find ~msg:"Unknown circle" st.circles id

let arrow st id1 id2 =
  IITbl.find ~msg:"Unknown arrow" st.arrows (id1, id2)

let remove_elements st (f: shape -> bool) =
  let elts = RList.value st.elements_l in
  let idx = ref 0 in
  List.iter (fun (_elt, shape) ->
    if f shape then RList.remove !idx st.elements
    else incr idx
  ) elts

(* ------- *)
(* Transitions *)

let transition ~x0 ~xmax ~duration set_x =
  let vmax = 2. *. (xmax -. x0) /. duration in
  let pause = 0.016 in
  let t0 = Mtime_clock.now () in
  let pi = 3.1415926535 in
  let u = duration /. (2. *. pi) in
  let v = (2. *. pi) /. duration in
  let rec loop () =
    let dt = Mtime.span t0 (Mtime_clock.now ()) |> Mtime.Span.to_s in
    if dt >= duration then (
      set_x xmax; Lwt.return ()
    ) else (
      Lwt_js.sleep pause >>= fun () ->
      set_x (
        x0 +. ((dt -. u *. sin (v *. dt)) *. vmax) /. 2.
      );
      loop ()
    )
  in
  loop ()

let signal_transition_to ~target ~duration s =
  let x0 = React.S.value s.s in
  transition ~x0 ~xmax:target ~duration s.set

let signal_transition_by ~offset ~duration s =
  let x0 = React.S.value s.s in
  transition ~x0 ~xmax:(x0 +. offset) ~duration s.set

(* ------- *)
(* Actions semantics *)

let svgrfloat (x: float React.S.t) =
  React.S.map (fun x -> (x, None)) x

let fade ~duration in_or_out base_color =
  let (start, end_) = match in_or_out with
    | `In -> 1., 0.
    | `Out -> 0., 1.
  in
  let color_of_opacity opacity =
    let c = Color.lighten opacity base_color |> Color.to_hexstring in
    `Color (c, None)
  in
  let s = create_signal (color_of_opacity start) in
  s,
  transition ~x0:start ~xmax:end_ ~duration (fun opacity ->
    s.set (color_of_opacity opacity))

let curved_arrow_control ~curve arrowv =
  let open Gg in
  (* let curve = 0.4 in *)
  V2.(0.5 * arrowv + (curve *. norm arrowv) * unit (ortho arrowv))

let curved_arrow_line ~x1 ~y1 ~x2 ~y2 ~stroke_color ~line_style ~curve =
  let open Gg in
  let d = React.S.l5 (fun x1 y1 x2 y2 curve ->
    let arrowv = V2.(v x2 y2 - v x1 y1) in
    let control = curved_arrow_control ~curve arrowv in
    Printf.sprintf "M %f %f q %f %f %f %f"
      x1 y1 (V2.x control) (V2.y control) (V2.x arrowv) (V2.y arrowv)
  ) x1 y1 x2 y2 curve in
  Svg.path ~a:([R.Svg.a_d d;
                R.Svg.a_stroke stroke_color.s;
                Svg.a_fill `None;
                Svg.a_stroke_width (3., None);] @ line_style)
    []

let arrowhead ~x1 ~y1 ~x2 ~y2 ~dist ~base ~height ~color ~curve =
  let points = React.S.l5 (fun x1 y1 x2 y2 curve ->
    let open Gg in
    let v1 = V2.v x1 y1 and v2 = V2.v x2 y2 in
    let arrow_v = V2.(v2 - v1) in
    let arrowhead_v =
      V2.unit (
        V2.(v2 - (v1 + 0.9 * curved_arrow_control ~curve arrow_v))
      ) in
    let base_v = Gg.V2.ortho arrowhead_v in
    let tip = Gg.V2.(v2 - dist * arrowhead_v) in
    let base_middle = Gg.V2.(tip - (height * arrowhead_v)) in
    let l = Gg.V2.(base_middle - base * base_v) in
    let r = Gg.V2.(base_middle + base * base_v) in
    [tip; l; r] |> List.map Gg.V2.to_tuple
  ) x1 y1 x2 y2 curve in
  Svg.polygon ~a:[R.Svg.a_points points;
                  R.Svg.a_fill color;
                 ]
    []

let curved_arrow ~x1 ~y1 ~x2 ~y2 ~color ~line_style ~curve ~head_dist
    ?(head_height = 17.) ?(head_base = 9.) ()
  =
  Svg.svg [
    curved_arrow_line ~x1 ~y1 ~x2 ~y2 ~stroke_color:color ~line_style ~curve;
    arrowhead ~x1 ~y1 ~x2 ~y2 ~curve ~dist:head_dist ~color:color.s
      ~height:head_height ~base:head_base;
  ]

let node_circle ~x ~y ~size ~stroke_color ~color ?onclick () =
  Svg.circle ~a:([R.Svg.a_cx (svgrfloat x);
                  R.Svg.a_cy (svgrfloat y);
                  Svg.a_r (float size, None);
                  R.Svg.a_stroke stroke_color;
                  R.Svg.a_fill color;
                  Svg.a_stroke_width (3., None);] @
                 (match onclick with
                  | None -> [] | Some x -> [Svg.a_onclick x]))
    []

let rec run_action st = function
  | NewCircle { id; x; y; size } ->
    assert (ITbl.find_opt st.circles id = None);
    let x = create_signal (float x) in
    let y = create_signal (float y) in
    let fill_color = create_signal (`Color ("white", None)) in
    let stroke_color, fade_thread = fade ~duration:0.3 `In Color.black in
    ITbl.add st.circles id {
      x; y; size = float size;
      fill = fill_color; stroke = stroke_color;
    };
    let onclick _ = st.queue_click id; true in
    let rel_x = SpaceAllocator.relx st.space x.s in
    let text_elt =
      Svg.text ~a:[R.Svg.a_x_list (React.S.map (fun x -> [x]) (svgrfloat rel_x));
                   R.Svg.a_y_list (React.S.map (fun x -> [x]) (svgrfloat y.s));
                   Svg.a_text_anchor `Middle;
                   Svg.a_dominant_baseline `Middle;
                   R.Svg.a_fill stroke_color.s;
                   Svg.a_class ["circle-text"];
                   Svg.a_onclick onclick;
                  ]
        [Svg.txt (string_of_int id)]
    in
    let circle_elt =
      node_circle ~x:rel_x ~y:y.s ~size ~stroke_color:stroke_color.s
        ~color:fill_color.s ~onclick () in
    RList.cons (text_elt, (Circle id)) st.elements;
    RList.cons (circle_elt, (Circle id)) st.elements;
    fade_thread
  | MoveCircleTo { id; x; y } ->
    let { x = xs; y = ys; _ } = circle st id in
    Lwt.join [
      signal_transition_to ~target:(float x) ~duration:0.3 xs;
      signal_transition_to ~target:(float y) ~duration:0.3 ys;
    ]
  | RemoveCircle id ->
    ITbl.remove st.circles id;
    remove_elements st (function
      | Circle k -> k = id
      | Arrow (i, j) -> i = id || j = id
    );
    (* No animation when removing stuff *)
    Lwt.return ()
  | NewArrow { circle1; circle2; style; curve } ->
    let { x = xs1; y = ys1; _ } = circle st circle1 in
    let { x = xs2; y = ys2; size = r2; _ } = circle st circle2 in
    let base_color = match style with
      | `Normal -> Color.black
      | `Bad -> Color.of_rgb 255 0 0
    in
    let line_style = match style with
      | `Normal -> []
      | `Bad -> [Svg.a_stroke_dasharray [(5., None); (5., None)]]
    in
    let color, fade_thread = fade ~duration:0.3 `In base_color in
    let curve = create_signal curve in
    IITbl.add st.arrows (circle1, circle2) { curve };
    let path =
      curved_arrow
        ~x1:(SpaceAllocator.relx st.space xs1.s) ~y1:ys1.s
        ~x2:(SpaceAllocator.relx st.space xs2.s) ~y2:ys2.s
        ~color ~line_style ~curve:curve.s
        ~head_dist:(r2 -. 3.) ()
    in
    RList.cons (path, (Arrow (circle1, circle2))) st.elements;
    fade_thread
  | RemoveArrow { circle1; circle2 } ->
    IITbl.remove st.arrows (circle1, circle2);
    remove_elements st (function
      | Arrow (i, j) -> i = circle1 && j = circle2
      | Circle _ -> false);
    Lwt.return ()
  | SetArrowCurve { circle1; circle2; curve } ->
    signal_transition_to ~target:curve ~duration:0.3
      (arrow st circle1 circle2).curve
  | Par actions ->
    run_actions_par st actions
  | Seq actions ->
    run_actions_seq st actions

and run_actions_par st (actions: action list) =
  List.map (run_action st) actions |> Lwt.join

and run_actions_seq st (actions: action list) =
  Lwt_list.iter_s (run_action st) actions

open! Utils
open! Lwt.Infix
open Js_of_ocaml
open Js_of_ocaml_lwt

type st = {
  ux : float signal;
  uy : float signal;

  elt : Dom_html.element Js.t;
  width : float signal;
  height : float signal;

  mutable max_virtual_x : int;
  mutable max_virtual_y : int;
}

let elt_width elt =
  Js.Optdef.get elt##getBoundingClientRect##.width
    (fun _ -> print_endline "oh no"; 1000.)

let elt_height elt =
  Js.Optdef.get elt##getBoundingClientRect##.height
    (fun _ -> print_endline "oh no"; 1000.)

let create_st (elt: Dom_html.element Js.t) =
  { ux = create_signal 1.;
    uy = create_signal 1.;
    elt;
    width = create_signal (elt_width elt);
    height = create_signal (elt_height elt);
    max_virtual_x = 1;
    max_virtual_y = 1; }

let relx st (x: float React.S.t) =
  React.S.l2 (fun x ux -> x *. ux) x st.ux.s

let recompute st =
  let avail_x = elt_width st.elt in
  st.ux.set (min 1. (avail_x /. float st.max_virtual_x))

let set_max_virtual_x st x =
  st.max_virtual_x <- max x 1;
  recompute st

let update_wh st =
  st.width.set (elt_width st.elt);
  st.height.set (elt_height st.elt)

let follow_resize st =
  Lwt_js_events.limited_onresizes ~elapsed_time:1. (fun _ _ ->
    update_wh st; recompute st; Lwt.return ()
  )

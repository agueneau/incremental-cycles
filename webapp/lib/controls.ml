open! Utils
open! Lwt.Infix
open Js_of_ocaml_tyxml
open Js_of_ocaml_lwt

type st = {
  manager: Manager.st;
  actions: Actions.action Lwt_stream.t;
  queue_action: Actions.action -> unit;
  mutable state: [`Top | `ClickingEdge of int];
  prompt: string signal;
}

let create_st () =
  let manager = Manager.create_st () in
  let actions, add_action = Lwt_stream.create () in
  let queue_action a =
    if Graph.G.cycle_edge manager.graph <> None
    && a <> Actions.Undo then ()
    else add_action (Some a)
  in
  { manager;
    actions; queue_action;
    state = `Top;
    prompt = create_signal "";
  }

let controls_elt =
  let open Tyxml_js in
  Html.div ~a:[Html.a_class ["text-controls"]] [
    Html.txt "Controls:  Undo: u   Redo: r   \
              Add vertex: v";
  ]

let legend_elt h =
  let open Tyxml_js in
  let arr x y1 y2 =
    Viewer.curved_arrow
      ~x1:(React.S.const x) ~y1:(React.S.const y1)
      ~x2:(React.S.const x) ~y2:(React.S.const y2)
      ~color:(create_signal (`Color ("black", None)))
      ~line_style:[] ~curve:(React.S.const 0.) ~head_dist:(-3.)
      ~head_base:6. ~head_height:10.
      ()
  in
  let txt s =
    Html.div ~a:[Html.a_class ["cap-label"; "cap"]] [Html.txt s] in
  let svgbox args =
    Html.svg ~a:[Svg.a_width (40., None);
                 Svg.a_height (30., None);
                 Svg.a_class ["cap"];
                ]
      args
  in
  let circle col =
    Viewer.node_circle
      ~x:(React.S.const 25.)
      ~y:(React.S.const 15.) ~size:12
      ~stroke_color:(React.S.const (`Color ("black", None)))
      ~color:(React.S.const (`Color (col, None))) ()
  in
  Html.div ~a:[Html.a_class ["text-controls"; "caption"]] [
    Html.div ~a:[Html.a_class ["cap"; "capcap"]] [Html.txt "Caption:"];
    svgbox [arr 30. 3. 27.; arr 30. 27. 3.;];
    txt "level";
    svgbox [circle Manager.node_red];
    txt "visited (at the same level)";
    svgbox [circle Manager.node_blue];
    txt "visited (levels update)";
    Html.div ~a:[Html.a_class ["permalink"; "cap"]] [
      Html.a ~a:[R.Html.a_href (Manager.permalink_url ~with_undo:true h)]
        [Html.txt "permalink"];
      Html.txt " ";
      Html.a ~a:[R.Html.a_href (Manager.permalink_url ~with_undo:false h)]
        [Html.txt "(without undo list)"];
    ];
  ]

let main_elt st =
  Tyxml_js.Html.div [
    controls_elt;
    legend_elt st.manager.history;
    Manager.history_elt st.manager.history;
    st.manager.view.svg_elt;
  ]

let set_circle_color st i ~on =
  let color = if on then "#dda500" else "black" in
  (try
     let Viewer.{ stroke; _ } = Viewer.circle st.manager.view i in
     stroke.set (`Color (color, None));
   with Failure _ -> ())

let handle_keydown st ev =
  let open Js_of_ocaml in
  let key = Js.Optdef.case ev##.key (fun () -> "ERR") Js.to_string in
  (* Printf.printf "key: %s %d\n" key ev##.keyCode; *)
  if key = " " then Dom.preventDefault ev;
  begin match st.state with
    | `Top ->
      begin match key, ev##.keyCode with
        | "u", _ ->
          st.prompt.set "u";
          st.queue_action Undo
        | "r", _ ->
          st.prompt.set "r";
          st.queue_action Redo
        | "v", _ ->
          st.prompt.set "v";
          st.queue_action AddVertex
        | _ -> ()
      end
    | `ClickingEdge i ->
      begin match ev##.keyCode with
        | 27 (* esc *) ->
          set_circle_color ~on:false st i;
          st.state <- `Top
        | _ ->
          ()
      end
  end

let handle_click st i =
  match st.state with
  | `Top ->
    st.prompt.set "";
    set_circle_color ~on:true st i;
    st.state <- `ClickingEdge i
  | `ClickingEdge k ->
    set_circle_color ~on:false st k;
    st.state <- `Top;
    if k <> i then
      st.queue_action (AddEdge (k, i))

let run st =
  let open Js_of_ocaml in
  Lwt.join [
    (* iter_p: risky? *)
    Lwt_stream.iter_p (Manager.run_action st.manager) st.actions;

    lwt_loop ~pause:0. (fun () ->
      let keyboard_event =
        Lwt_js_events.keydown ~use_capture:true Dom_html.document >>= fun ev ->
        handle_keydown st ev; Lwt.return ()
      in
      let click =
        Lwt_stream.next st.manager.view.clicks >>= fun i ->
        handle_click st i; Lwt.return ()
      in
      Lwt.pick [
        keyboard_event;
        click;
      ]
    ) ();
  ]

open Utils
open! Lwt.Infix
open Js_of_ocaml_tyxml
module RList = ReactiveData.RList
module G = Graph.G

type ('st, 'step) zip = {
  before_l : ('st * 'step) RList.t;
  before : ('st * 'step) RList.handle;
  after_l : ('step * 'st) RList.t;
  after : ('step * 'st) RList.handle;
}

type history = (G.snapshot, (Actions.action * G.Diff.t)) zip

type st = {
  view: Viewer.st;
  lview: EdgesLayout.st;
  graph: G.graph;
  history: history;
}

let create_st () =
  let before_l, before = RList.create [] in
  let after_l, after = RList.create [] in
  {
    view = Viewer.create_st ();
    lview = EdgesLayout.create_st ();
    graph = G.create ();
    history = { before_l; before; after_l; after };
  }

let record_history st ~action f =
  let g0 = G.snapshot st.graph in
  let res = f () in
  let gnow = G.snapshot st.graph in
  let diff = G.Diff.compute g0 gnow in
  RList.cons (g0, (action, diff)) st.history.before;
  RList.set st.history.after [];
  diff, res

(* ------------------- *)

let (%>) f g x = g (f x)

let history_elt (h: history) =
  let open Tyxml_js in
  let edges_timeline l =
    List.flatten l
    |> filter_map (function
      | G.Diff.NewNode _
      | G.Diff.ChangeNodeLevel _ -> None
      | G.Diff.NewEdge { node1; node2; style = _ } ->
        Some (Printf.sprintf "(%d, %d)" node1 node2)
    )
    |> reduce ~sep:" -- " ~unit:"" ~op:(^)
  in
  Html.div ~a:[Html.a_class ["text-history"]] [
    Html.div ~a:[Html.a_class ["text-past"]] [
      Html.txt "Inserted edges: ";
      R.Html.txt (
        React.S.map (fun l -> edges_timeline (List.rev_map (snd %> snd) l))
          (RList.signal h.before_l)
      );
    ];
    Html.div ~a:[Html.a_class ["text-future"]] [
      R.Html.txt (
        React.S.map (fun l ->
          let tl = edges_timeline (List.map (fst %> snd) l) in
          if tl = "" then "" else " -- " ^ tl
        ) (RList.signal h.after_l)
      )
    ];
  ]

let permalink_url ~with_undo (h: history) =
  let actions before after =
    if with_undo then
      "#" ^
      Actions.print (before @ after) ^
      String.make (List.length after) 'u'
    else
      "#" ^ Actions.print before
  in
  React.S.l2 (fun before after ->
    actions (List.rev_map (snd %> fst) before) (List.map (fst %> fst) after)
  ) (RList.signal h.before_l) (RList.signal h.after_l)

(* ------------------- *)

let margin_x = 100

let layout st (node: int) (level: int) =
  let max_height =
    int_of_float (React.S.value st.view.space.height.s) in
  let x = margin_x + (node - 1) * 150 in
  let y = (3 * max_height) / 4 - 100 * level in
  let size = 30 in
  (x, y, size)

let reserve_space st =
  let vs = G.vertices st.graph in
  let needed_x = 2 * margin_x + 150 * (max 0 (List.length vs - 1)) in
  SpaceAllocator.set_max_virtual_x st.view.space needed_x

let apply_diff_elt st (elt: G.Diff.elt): EdgesLayout.action =
  match elt with
  | NewNode { node; level } ->
    let x, y, size = layout st node level in
    NewCircle { id = node; x; y; size }
  | NewEdge { node1; node2; style } ->
    NewArrow { circle1 = node1; circle2 = node2; style }
  | ChangeNodeLevel { node; before = _; after } ->
    let x, y, _ = layout st node after in
    MoveCircleTo { id = node; x; y }

let undo_diff_elt st (elt: G.Diff.elt): EdgesLayout.action =
  match elt with
  | NewNode { node; level = _ } ->
    RemoveCircle node
  | NewEdge { node1; node2; style = _ } ->
    RemoveArrow { circle1 = node1; circle2 = node2 }
  | ChangeNodeLevel { node; before; after = _ } ->
    let x, y, _ = layout st node before in
    MoveCircleTo { id = node; x; y }

let apply_diff st (diff: G.Diff.t): Viewer.action =
  let view_actions =
    List.map (apply_diff_elt st) diff
    |> EdgesLayout.compile_actions st.lview
  in
  Par view_actions

let undo_diff st (diff: G.Diff.t): Viewer.action =
  let view_actions =
    List.map (undo_diff_elt st) diff
    |> EdgesLayout.compile_actions st.lview
  in
  Par view_actions

(* ----------------------- *)

let node_red = "#ff7373" (* red *)
let node_blue = "#737aff" (* blue *)

let color_marked (st: st) =
  let g = G.snapshot st.graph in
  let last_mark = G.next_mark g - 1 in
  let vf = G.visited_forward g |> ISet.of_list in
  IMap.iter (fun v m ->
    let Viewer.{ fill = fill_color; _ } = Viewer.circle st.view v in
    let color =
      if m = last_mark && m <> -1 then node_red
      else if ISet.mem v vf then node_blue
      else "white"
    in
    fill_color.set (`Color (color, None))
  ) (G.marks g)

let run_action (st: st) = function
  | Actions.AddVertex ->
    let diff, () = record_history st ~action:AddVertex (fun () ->
      let id = (G.vertices st.graph |> List.fold_left max 0) + 1 in
      Graph.IC.add_vertex st.graph id
    ) in
    reserve_space st;
    Viewer.run_action st.view (apply_diff st diff) >>= fun () ->
    color_marked st; Lwt.return ()

  | Actions.AddEdge (v, w) ->
    let vs = G.vertices st.graph in
    if not (List.mem v vs && List.mem w vs) then
      Lwt.return (Printf.eprintf "Invalid edge %d - %d\n" v w)
    else if List.mem w (G.outgoing st.graph v) then
      Lwt.return ()
    else begin
      let diff, () = record_history st ~action:(AddEdge (v, w)) (fun () ->
        let res = Graph.IC.add_edge_or_detect_cycle st.graph v w in
        match res with
        | Graph.IC.EdgeCreatesCycle _ ->
          print_endline "Cycle detected!";
          G.add_cycle_edge st.graph v w
        | _ -> ()
      ) in
      reserve_space st;
      color_marked st;
      Viewer.run_action st.view (apply_diff st diff)
    end

  | Actions.Undo ->
    begin match RList.value st.history.before_l with
      | [] -> Lwt.return ()
      | (g_before, (a, diff)) :: _ ->
        RList.remove 0 st.history.before;
        RList.cons ((a, diff), G.snapshot st.graph) st.history.after;
        G.rollback st.graph g_before;
        reserve_space st;
        Viewer.run_action st.view (undo_diff st diff) >>= fun () ->
        color_marked st; Lwt.return ()
    end

  | Actions.Redo ->
    begin match RList.value st.history.after_l with
      | [] -> Lwt.return ()
      | ((a, diff), g_after) :: _ ->
        RList.remove 0 st.history.after;
        RList.cons (G.snapshot st.graph, (a, diff)) st.history.before;
        G.rollback st.graph g_after;
        reserve_space st;
        Viewer.run_action st.view (apply_diff st diff) >>= fun () ->
        color_marked st; Lwt.return ()
    end

let incremental_cycles_ml_header = {|
include Incremental_cycles_intf

module Make (Raw_graph : Raw_graph) : S
  with type graph := Raw_graph.graph
   and type vertex := Raw_graph.vertex =
struct
|}

let incremental_cycles_ml_footer = {|
end
|}

let () =
  let implem_file = Sys.argv.(1) in
  let ic = open_in implem_file in
  let rec cat () =
    match input_line ic with
    | exception End_of_file -> close_in ic
    | line -> print_endline line; cat ()
  in
  print_endline incremental_cycles_ml_header;
  cat ();
  print_endline incremental_cycles_ml_footer
